<?php

namespace backend\controllers\dev;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\property;

class PropProfitController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $loadedProfit = new property\ProfitHistory();
        if ($loadedProfit->load(Yii::$app->request->post()) && $loadedProfit->validate()) {
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $loadedProfit->save();
                
                property\Profit::shareProfit($loadedProfit);
                
                $transaction->commit();
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => property\ProfitHistory::find()->orderBy('created_at desc')->limit(3),
            'pagination' => false,
        ]);
        
        $newProfit = new property\ProfitHistory();
        $propList = property\Info::find()->where(['status' => property\Status::OPERATING])
                                         ->select(['id', 'title'])->all();
        
        return $this->render('index', [
            'loadedProfit' => $loadedProfit,
            'dataProvider' => $dataProvider,
            'newProfit' => $newProfit,
            'propList' => ArrayHelper::map($propList, 'id', 'title'),
        ]);
    }
}
