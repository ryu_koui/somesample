<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\account\Creditcard */

$this->title = Yii::t('app', 'Create account_creditcard');
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'account_creditcard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditcard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
