<?php

use yii\helpers\Html;

$this->title = 'Dataform';
$this->params['breadcrumbs'][] = ['label' => 'Dataform'];
?>

<div class="data-browser-index">
    <h1>Data Form</h1>
    
    <div class="body-content" style="padding-left:100px">
        <h3>property</h3>
        <ul>
            <li><a href="/dev/dataform/property/info">prop_info</a></li>
            <li><a href="/dev/dataform/property/stocks">prop_stocks</a></li>
            <li><a href="/dev/dataform/property/profit-history">prop_profit_his</a></li>
        </ul>
        
        <h3>user</h3>
        <ul>
            <li><a href="/dev/dataform/user/info">user_info</a></li>
            <li><a href="/dev/dataform/user/point">user_point</a></li>
        </ul>
        
        <h3>account</h3>
        <ul>
            <li><a href="/dev/dataform/account/creditcard">account_creditcard</a></li>
        </ul>
        
        <h3>languages</h3>
        <ul>
            <li><a href="/dev/dataform/languages/prop-info-zhcn">prop_info_zh-CN</a></li>
        </ul>
        <ul>
            <li><a href="/dev/dataform/languages/campaign-info-zhcn">campaign_info_zh-CN</a></li>
        </ul>
    </div>
        
</div>