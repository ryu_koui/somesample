<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\languages\CampaignInfoZhcn */

$this->title = Yii::t('app', 'Create Campaign Info Zhcn');
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Campaign Info Zhcns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-info-zhcn-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
