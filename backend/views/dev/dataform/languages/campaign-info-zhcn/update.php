<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\languages\CampaignInfoZhcn */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Campaign Info Zhcn',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Campaign Info Zhcns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="campaign-info-zhcn-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
