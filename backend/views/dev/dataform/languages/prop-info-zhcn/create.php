<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\languages\PropInfoZhCN */

$this->title = Yii::t('app', 'Create Prop Info Zh Cn');
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prop Info Zh Cns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-info-zh-cn-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
