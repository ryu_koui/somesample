<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\languages\PropInfoZhCN */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Prop Info Zh Cn',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prop Info Zh Cns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="prop-info-zh-cn-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
