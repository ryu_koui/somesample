<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\property\Info */

$this->title = Yii::t('app', 'Create prop_info');
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'prop_info'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
