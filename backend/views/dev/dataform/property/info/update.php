<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\property\Info */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'prop_info',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'prop_info'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
