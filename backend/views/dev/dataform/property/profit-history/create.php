<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\property\ProfitHistory */

$this->title = Yii::t('app', 'Create Profit History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profit Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profit-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
