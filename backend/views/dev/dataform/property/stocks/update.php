<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\property\Stocks */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'prop_stocks',
]) . ' ' . $model->prop_id;
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'prop_stocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->prop_id, 'url' => ['view', 'prop_id' => $model->prop_id, 'user_id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="stocks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
