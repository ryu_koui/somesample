<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\user\Info */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'user_info',
]) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Dataform', 'url' => '/dev/dataform'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'user_info'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
