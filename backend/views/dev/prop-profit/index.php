<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\property\Profit;

$this->title = '房产收入输入器';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="prop-profit-editor">
    
    <h1><?= Html::encode($this->title) ?></h1>
    
<?php if ($loadedProfit && $loadedProfit->id):?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong><?= Yii::t('app-back', '完了！')?></strong> <?= Yii::t('app-back', '新収益情報の挿入と分配処理完了しました。')?>
    </div>
<?php endif?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'prop_id',
            'profit_type',
            'amount',
            'shared_at',
            'user_cnt',
            'shared_amount',
            'shared_fee',
            'shared_tax',
            'created_at',
        ],
        'summary' => Yii::t('app-back', '収益データ 最新{totalCount}件'),
        'rowOptions' => function($model, $key, $widget, $grid) use ($loadedProfit){
            if ($loadedProfit && $model->id === $loadedProfit->id) {
                return ['class' => 'success'];
            }
        },
    ]); ?>
    
    <hr/>
    
    <h3><?= Yii::t('app-back', '収益データを追加')?></h3>
    <div class="profit-editor-form">

        <?php $form = ActiveForm::begin(); ?>
    
        <?= $form->field($newProfit, 'prop_id')->dropDownList($propList, [
            'prompt' => Yii::t('app-back', '▼ 物件名を選択'),
        ]) ?>
    
        <?= $form->field($newProfit, 'profit_type')->dropDownList([
            Profit::TYPE_AIRBNB => 'airbnb',
            Profit::TYPE_RENTAL => Yii::t('app-back', '賃貸'),
        ]) ?>
        
        <?= $form->field($newProfit, 'amount')->textInput(['maxlength' => true]) ?>
        
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app-back', 'データを挿入し分配する'), ['class' => 'btn btn-success']) ?>
        </div>
    
        <?php ActiveForm::end(); ?>
    
    </div>
</div>