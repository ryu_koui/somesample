<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <h1><?= Yii::t('app-back', '開発ツールトップ')?></h1>

    <div class="body-content">

        <div class="row">
            <div class="col-md-4 index-panel">
                <h2><?= Yii::t('app-back', '物件収益入力フォーム')?></h2>
                <div>
                    <p><?= Yii::t('app-back', '物件収益情報を追加かつ収益をユーザに分配処理を行う')?></p>
                </div>
                <p><a class="btn btn-default" href="/dev/prop-profit">Property Profit Editor &raquo;</a></p>
            </div>
            <div class="col-md-4 index-panel">
                <h2><?= Yii::t('app-back', 'データ入力フォーム')?></h2>
                <div>
                    <p><?= Yii::t('app-back', 'DB内のデータを参照、更新する')?></p>
                </div>
                <p><a class="btn btn-default" href="/dev/dataform">Data Form &raquo;</a></p>
            </div>
            <div class="col-md-4 index-panel">
                <h2>Gii</h2>
                <div>
                    <p><?= Yii::t('app-back', 'コードジェネレータ')?></p>
                </div>
                <p><a class="btn btn-default" href="/gii">Gii &raquo;</a></p>
            </div>
            <div class="col-md-4">
                
            </div>
        </div>

    </div>
</div>
