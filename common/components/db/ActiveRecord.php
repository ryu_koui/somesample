<?php

namespace common\components\db;

use Yii;

class ActiveRecord extends \yii\db\ActiveRecord
{
    public function needTranslatAttributes()
    {
        return [];
    }
    
    public static function populateRecord($record, $row)
    {
        parent::populateRecord($record, $row);
        
        $attrs = $record->needTranslatAttributes();
        foreach ($attrs as $name => $column) {
            $category = 'db-' . $record->tableName() . '.' . $column;
            $message = $column . $record->id;
            $record->$name = Yii::t($category, $message);
        }
    }
}