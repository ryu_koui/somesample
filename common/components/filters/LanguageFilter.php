<?php

namespace common\components\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;

class LanguageFilter extends ActionFilter
{
    public function init()
    {
        parent::init();
    }
    
    public function beforeAction($action)
    {
        if ($lang = Yii::$app->session->get('lang')) {
            Yii::$app->language = $lang;
        }
        
        return true;
    }
}