<?php

namespace common\components\i18n;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DbMessageSource extends \yii\i18n\DbMessageSource
{
    public $categoryPrefix = '';
    
    public $messageTableSuffix = '';
    
    protected function loadMessagesFromDb($category, $language)
    {
        list($tableName, $columnName) = explode('.', substr($category, strlen($this->categoryPrefix)), 2);
        $tableName .= '_lang_' . $language;
        
        $mainQuery = new Query();
        $mainQuery->select(["concat('{$columnName}', id) as message", "{$columnName} as translation"])
                  ->from($tableName);
        
        $messages = $mainQuery->createCommand($this->db)->queryAll();
        
        return ArrayHelper::map($messages, 'message', 'translation');
    }
    
}