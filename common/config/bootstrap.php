<?php
Yii::setAlias('top', realpath(dirname(__DIR__) . '/../'));
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('consumer_front', dirname(dirname(__DIR__)) . '/consumer_front');
Yii::setAlias('supplier_front', dirname(dirname(__DIR__)) . '/supplier_front');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('rest', dirname(dirname(__DIR__)) . '/rest');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('resource', dirname(dirname(__DIR__)) . '/resource');

// BC Math default scale
bcscale(3);
