<?php
return [
    // 'language' => 'ja-JP',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'forceTranslation' => true,
                    // 'forceTranslation' => true,
                ],
                'db-*' => [
                    'class' => 'common\components\i18n\DbMessageSource',
                    'categoryPrefix' => 'db-',
                    'messageTableSuffix' => '_lang',
                    'forceTranslation' => true,
                    // 'enableCaching' => true,
                ],
                'creditcard' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    // 'forceTranslation' => true,
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'cache' => [
            'class' => 'yii\redis\Cache',
            'keyPrefix' => 'hlstartup_',
            'redis' => 'redis' // id of the connection application component
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd',
            'defaultTimeZone' => 'Asia/Tokyo',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
            // 'currencyCode' => 'JPY',
            // 'currencyCode' => 'CNY', // use local / language data
            'numberFormatterOptions' => [
                \NumberFormatter::MIN_FRACTION_DIGITS => 0,
                \NumberFormatter::MAX_FRACTION_DIGITS => 0,
            ],
       ],
    ],
];
