<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'systemUserId' => 1,
    'exciseTax' => 0.8,
    'defaultFeeId' => 1,
    
    'paypal' => [
        'config' => [
            'cache.enabled' => 'true',
            'cache.FileName' => '@runtime/paypal/auth.cache',
            'http.ConnectionTimeOut' => 30,
            'http.Retry' => 1,
        ]
    ],
];
