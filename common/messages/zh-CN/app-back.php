<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'DB内のデータを参照、更新する' => '可以参照或输入数据进数据库',
    '▼ 物件名を選択' => '▼ 选择房产名',
    'コードジェネレータ' => '自动代码生成器',
    'データを挿入し分配する' => '输入收入数据并分配',
    'データ入力フォーム' => '数据编辑器',
    '収益データ 最新{totalCount}件' => '收入数据 最新{totalCount}件',
    '収益データを追加' => '追加收入数据',
    '完了！' => '完成！',
    '新収益情報の挿入と分配処理完了しました。' => '新收入数据追加并分配完毕.',
    '物件収益入力フォーム' => '',
    '物件収益情報を追加かつ収益をユーザに分配処理を行う' => '可以手动输入房产的收入信息并令收入分配给用户',
    '賃貸' => '月租',
    '開発ツールトップ' => '开发工具主页',
];
