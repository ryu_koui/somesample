<?php
namespace common\models\account;

interface AccountServiceInterface
{
    public static function payoutUse();
    
    public static function paymentUse();
    
    public static function type();
    
    public static function acName();
    
    public static function getInst($user_id, $use = null);
    
    public function findOne($id);
    
    public function findAll();
    
    public function addOne($post);
}
