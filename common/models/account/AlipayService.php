<?php
namespace common\models\account;

use Yii;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;

class AlipayService extends BaseAccountService
{
    public static $payout_use = true;
    public static $payment_use = true;
    public static $label = 'Alipay';
    
    public static function acName()
    {
        return Alipay::className();
    }
}
