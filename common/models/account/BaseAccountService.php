<?php
namespace common\models\account;

use Yii;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;
use common\models\account\Creditcard;
use common\models\user;

abstract class BaseAccountService extends \yii\base\Model implements AccountServiceInterface
{
    public static $payout_use;
    public static $payment_use;
    public static $label;
    
    public $user_id;
    public $usage;
    public $ar_datas = [];
    
    public static function payoutUse()
    {
        return static::$payout_use;
    }
    
    public static function paymentUse()
    {
        return static::$payment_use;
    }
    
    public static function type()
    {
    }
    
    public static function acName()
    {
    }
    
    public static function getInst($user_id, $usage = null)
    {
        $className = static::className();
        $inst = new $className();
        $inst->user_id = $user_id;
        $inst->usage = $usage;
        return $inst;
    }
    
    public function findOne($id)
    {
        $acClassName = static::acName();
        $ar_data = $acClassName::findOne($id);
        return $ar_data;
    }
    
    public function findAll()
    {
        $acClassName = static::acName();
        $this->ar_datas = $acClassName::find()->where(['user_id' => $this->user_id])->all();
        return $this->ar_datas;
    }
    
    public function addOne($post)
    {
        $acClassName = static::acName();
        $model = new $acClassName();
        $model->user_id = $this->user_id;
        
        $this->ar_datas[] = $model;
        
        if (!$model->load($post)) return false;
        
        $point_model = user\Point::findOne($this->user_id);
        if (!$point_model) {
            throw new InvalidValueException('Faild to load user point info. (user_id: ' . $user_id . ')');
        }
        
        $save_payment = (static::paymentUse() && !$point_model->payment_account);
        $save_payout = (static::payoutUse() && !$point_model->payout_account);
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $result = $model->save();
            
            if ($save_payment) $point_model->payment_account = static::type();
            if ($save_payout) $point_model->payout_account = static::type();
            if ($save_payment || $save_payout) {
                $result = $result && $point_model->save();
            }
            
            if (!$result) {
                throw new \Exception('create account save failed.');
            }
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return true;
    }
}
