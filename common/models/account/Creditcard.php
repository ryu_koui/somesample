<?php

namespace common\models\account;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\web\JsExpression;
/**
 * This is the model class for table "account_creditcard".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property integer $number
 * @property integer $expire_month
 * @property integer $expire_year
 * @property string $name
 * @property integer $payout_default
 * @property integer $payment_default
 * @property string $created_at
 * @property string $updated_at
 */
class Creditcard extends \yii\db\ActiveRecord
{
    public $expire_date;
    public $cvc;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_creditcard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'number', 'expire_month', 'expire_year', 'name', 'expire_date', 'cvc'], 'required'],
            [['user_id', 'expire_month', 'expire_year', 'payout_default', 'payment_default'], 'integer'],
            [['number'], 'clientValidateNumber', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['expire_date'], 'clientValidateExpiry', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['cvc'], 'clientValidateCVC', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 32],
            [['number'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'number' => Yii::t('app', 'Number'),
            'expire_month' => Yii::t('app', 'Expire Month'),
            'expire_year' => Yii::t('app', 'Expire Year'),
            'expire_date' => Yii::t('app', 'Expiry'),
            'name' => Yii::t('app', 'Name'),
            'payout_default' => Yii::t('app', 'Payout Default'),
            'payment_default' => Yii::t('app', 'Payment Default'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public static function getCount($user_id)
    {
        return static::find()->where(['user_id' => $user_id])->count();
    }
    
    public function getHiddenNumber()
    {
        return '**** **** ' . substr($this->number, -4);
    }
    
    public function getExpiry()
    {
        return sprintf('%02d / %d', $this->expire_month, $this->expire_year);
    }
    
    public function charge($amount)
    {
        
    }
    
    public function clientValidateNumber($model, $attribute)
    {
        $message = json_encode(Yii::t('creditcard', 'Invalid card number.'), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return new JsExpression("
            if (!$.payment.validateCardNumber(value)) {
                messages.push($message);
            }
        ");
    }
    
    public function clientValidateExpiry($model, $attribute)
    {
        $message = json_encode(Yii::t('creditcard', 'Invalid expiry.'), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return new JsExpression("
            var expiry = $.payment.cardExpiryVal(value);
            if (!$.payment.validateCardExpiry(expiry.month, expiry.year)) {
                messages.push($message);
            }
        ");
    }
    
    public function clientValidateCVC($model, $attribute)
    {
        $message = json_encode(Yii::t('creditcard', 'Invalid security code.'), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return new JsExpression("
            if (!$.payment.validateCardCVC(value)) {
                messages.push($message);
            }
        ");
    }
}
