<?php
namespace common\models\account;

use Yii;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;

class CreditcardService extends BaseAccountService
{
    public static $payout_use = false;
    public static $payment_use = true;
    public static $label = 'Credit Card';
    
    public static function type()
    {
        return Manager::TYPE_CREDITCARD;
    }
    
    public static function acName()
    {
        return Creditcard::className();
    }
}
