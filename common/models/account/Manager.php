<?php
namespace common\models\account;

use Yii;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;
use common\models\user;

class Manager extends \yii\base\Model
{
    const USE_PAYMENT   = 'payment';
    const USE_PAYOUT    = 'payout';
    
    const TYPE_CREDITCARD = 'creditcard';
    const TYPE_ALIPAY     = 'alipay';
    const TYPE_PAYPAL     = 'paypal';
    
    private static $_service_list;
    
    public static function getList()
    {
        if (!self::$_service_list) {
            self::$_service_list = [
                self::TYPE_PAYPAL => PaypalService::className(),
                self::TYPE_CREDITCARD => CreditcardService::className(),
                // self::TYPE_ALIPAY => AlipayService::className(),
            ];
        }
        return self::$_service_list;
    }
    
    public static function getInfoList($usage = self::USE_PAYMENT)
    {
        $result = [];
        foreach (self::getList() as $id => $service_class) {
            if ($usage == self::USE_PAYMENT && $service_class::paymentUse() === true
                || $usage == self::USE_PAYOUT && $service_class::payoutUse() === true) {
                $result[$id] = $service_class;
            }
        }
        
        if (!$result) {
            throw new InvalidValueException('Invialid param value (account list): ' . $usage);
        }
        return $result;
    }
    
    public static function getAllList($user_id, $usage = null)
    {
        if (is_null($usage)) {
            $list = self::getList();
        } else {
            $list = self::getInfoList($usage);
        }
        
        $result = [];
        foreach ($list as $type => $service_class) {
            $records = $service_class::getInst($user_id, $usage)->findAll();
            if (!empty($records)) {
                $result[$type] = [
                    'service_class' => $service_class,
                    'data' => $records,
                ];
            }
        }
        
        return $result;
    }
    
    public static function getById($type, $id, $usage = null)
    {   
        $className = self::verifyAccount($type, $usage);
        $arName = $className::acName();
        return $arName::findOne($id);
    }
    
    public static function verifyAccount($type, $usage = null)
    {
        if (is_null($usage)) {
            $list = self::getList();
        } else {
            $list = self::getInfoList($usage);
        }
        
        if (!$list || !isset($list[$type])) {
            throw new InvalidParamException('Account type (' . $type . ') dose not exist.');
        }
        
        return $list[$type];
    }
    
    // public static function getAccount($type)
    // {
        // if (!Yii::$app->user->id) {
            // throw new InvalidParamException('Invialid param value (user_id)');
        // }
//         
        // $type_data = self::datas()['$type'];
        // $type_data['class_name']::findOne();
    // }
}
