<?php

namespace common\models\account;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "account_paypal".
 *
 * @property integer $user_id
 * @property string $email
 * @property string $phone
 * @property string $paypal_id
 * @property string $created_at
 * @property string $updated_at
 */
class Paypal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_paypal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['payout_default', 'payment_default'], 'integer'],
            [['email', 'phone', 'payer_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'payer_id' => Yii::t('app', 'Payer ID'),
            'payout_default' => Yii::t('app', 'Payout Default'),
            'payment_default' => Yii::t('app', 'Payment Default'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
}
