<?php
namespace common\models\account;

use Yii;
use PayPal\Api;
use PayPal\Auth;
use PayPal\Rest;

class PaypalService extends BaseAccountService
{
    public static $payout_use = true;
    public static $payment_use = true;
    public static $label = 'Paypal';
    
    public static function type()
    {
        return Manager::TYPE_PAYPAL;
    }
    
    public static function acName()
    {
        return Paypal::className();
    }
    
    public static function createPayment($data)
    {
        $apiContext = self::getApiContext();
        
        $item_list = [];
        foreach ($data['required_stock_list'] as $stock_data) {
            $item = new Api\Item();
            $item->setName($data['title'])
                 ->setCurrency('JPY')
                 ->setQuantity($stock_data['num'])
                 ->setPrice(floor($stock_data['unit_price']));
            $item_list[] = $item;
        }
        
        $itemList = new Api\ItemList();
        $itemList->setItems($item_list);
        
        $details = new Api\Details();
        $details->setTax(floor($data['tax']))
                ->setHandlingFee(floor($data['fee']))
                ->setSubtotal(floor($data['required_price']));
        
        $amount = new Api\Amount();
        $amount->setCurrency("JPY")
               ->setTotal(floor($data['charge']))
               ->setDetails($details);
        
        $desc = isset($data['description']) ? $data['description'] : '';
        $transaction = new Api\Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription($desc)
                    ->setInvoiceNumber(uniqid());
                    
        $redirectUrls = new Api\RedirectUrls();
        $redirectUrls->setReturnUrl($data['return_url'])
                     ->setCancelUrl($data['cancel_url']);
        
        $payer = new Api\Payer();
        $payer->setPaymentMethod("paypal");
        
        $payment = new Api\Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));
        
        try {
            $payment->create($apiContext);
        } catch (Exception $e) {
            Yii::error($e->__toString());
            Yii::error($payment->toJSON(128));
            throw $e;
        }
        
        if ($payment->getState() == 'created' && $payment->getApprovalLink()) {
            return $payment->getApprovalLink();
        }
        
        return false;
    }
    
    public static function executPayment($payment_data)
    {
        $apiContext = self::getApiContext();
        
        $payment = Api\Payment::get($payment_data['paymentId'], $apiContext);

        $execution = new Api\PaymentExecution();
        $execution->setPayerId($payment_data['PayerID']);
        
        try {
            $result = $payment->execute($execution, $apiContext);
            $payment = Api\Payment::get($payment_data['paymentId'], $apiContext);
            
            if ($payment->payer->payer_info->email) {
                $paypal = Paypal::findOne($payment_data['user_id']);
                if (!$paypal) {
                    $paypal = new Paypal();
                    $paypal->user_id = $payment_data['user_id'];
                    $paypal->email = $payment->payer->payer_info->email;
                    $paypal->phone = $payment->payer->payer_info->phone;
                    $paypal->payer_id = $payment->payer->payer_info->payer_id;
                    $paypal->save();
                }
                return $paypal;
            }
        } catch (Exception $e) {
            Yii::error($e->__toString());
            Yii::error($payment->toJSON(128));
            throw $e;
        }
        
        return false;
    }
    
    public static function payout($payout_data)
    {
        $apiContext = self::getApiContext();
        
        $senderBatchHeader = new Api\PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid());
        if (isset($payout_data['email_subject']) && $payout_data['email_subject']) {
            $senderBatchHeader->setEmailSubject($payout_data['email_subject']);
        }
        
        $currency = new Api\Currency();
        $currency->setCurrency('JPY')->setValue($payout_data['amount']);
        
        $senderItem = new Api\PayoutItem();
        $senderItem->setRecipientType('EMAIL')
                   ->setReceiver($payout_data['email'])
                   ->setAmount($currency);
        
        $payouts = new Api\Payout();                            
        $payouts->setSenderBatchHeader($senderBatchHeader)
                ->addItem($senderItem);
                
        try {
            $output = $payouts->createSynchronous($apiContext);
            Yii::trace($payouts->toJSON(128));
        } catch (Exception $e) {
            Yii::error($e->__toString());
            Yii::error($payouts->toJSON(128));
            throw $e;
        }
        
        return false;
    }
    
    protected static function getApiContext()
    {
        $apiContext = new Rest\ApiContext(
            new Auth\OAuthTokenCredential(
                Yii::$app->params['paypal']['clientId'],
                Yii::$app->params['paypal']['secret']
            )
        );
        
        if (isset(Yii::$app->params['paypal']['config']['cache.FileName'])) {
            Yii::$app->params['paypal']['config']['cache.FileName'] = Yii::getAlias(Yii::$app->params['paypal']['config']['cache.FileName']);
        }
        
        $apiContext->setConfig(Yii::$app->params['paypal']['config']);
        
        return $apiContext;
    }
}