<?php
namespace common\models\campaign;

interface ActionsInterface
{
    public function buyStock();
    
    public function shareProfitPerStockDetail();
}