<?php
namespace common\models\campaign;

use Yii;

abstract class BaseActions extends \yii\base\Component implements ActionsInterface
{
    public $info;
    
    public function buyStock() {}
    
    public function shareProfitPerStockDetail() {}
}
