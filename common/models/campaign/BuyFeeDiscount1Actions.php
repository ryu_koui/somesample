<?php
namespace common\models\campaign;

use Yii;
use common\models\fee;

class BuyFeeDiscount1Actions extends BaseActions
{
    public function buyStock()
    {
        fee\Current::load([$this->info->fee_id]);
    }
}
