<?php

namespace common\models\campaign;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\components;

/**
 * This is the model class for table "campaign_info".
 *
 * @property integer $id
 * @property string $class
 * @property string $label
 * @property string $description
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Info extends components\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_info';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'label'], 'required'],
            [['description'], 'string'],
            [['fee_id'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['class'], 'string', 'max' => 32],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'class' => Yii::t('app', 'Class'),
            'label' => Yii::t('app', 'Label'),
            'description' => Yii::t('app', 'Description'),
            'fee_id' => Yii::t('app', 'Fee ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public function needTranslatAttributes()
    {
        return [
            'label' => 'label',
            'description' => 'description',
        ];
    }
}
