<?php
namespace common\models\campaign;

use Yii;

class Service
{
    const ACT_BUY_STOCK = 'buyStock';
    const ACT_SHARE_PROFIT_PER_STOCK_DETAIL = 'shareProfitPerStockDetail';
    
    public static function apply($action, $info_list)
    {
        $args = array_slice(func_get_args(), 2);
        
        foreach ($info_list as $info) {
            $class_name = __NAMESPACE__ . '\\' . $info->class . 'Actions';
            
            $service = new $class_name();
            $service->info = $info;
            
            call_user_func_array([$service, $action], $args);
        }
    }
}