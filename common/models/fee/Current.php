<?php
namespace common\models\fee;

use yii;
use yii\base\InvalidParamException;

class Current
{
    public static $default_fee = null;
    
    public static $stock_deposit_per;
    public static $buy_stock_per;
    public static $sell_stock_per;
    public static $user_earnings_per;
    public static $point_exchange_per;
    
    private static $loaded_fee_list = [];
    
    public static function loadDefault()
    {
        self::load([], true);
    }
    
    public static function load($fee_ids = [], $refresh = false)
    {
        $defaultLoaded = false;
        if (is_null(self::$default_fee)) {
            $default_fee_id = Yii::$app->params['defaultFeeId'];
            self::$default_fee = Info::getDb()->cache(function($db) use ($default_fee_id) {
                return Info::findOne($default_fee_id);
            });
            if (!self::$default_fee) {
                throw new InvalidParamException("Invalid param value (defaultFeeId: {$default_fee_id})");
            }
            
            self::setParams(self::$default_fee);
            $defaultLoaded = true;
        }
        
        if (!$defaultLoaded && $refresh) {
            self::setParams(self::$default_fee);
        }
        
        if ($fee_ids) {
            
            $target_ids = [];
            foreach ($fee_ids as $fee_id) {
                if (isset(self::$loaded_fee_list[$fee_id]) && self::$loaded_fee_list[$fee_id]) {
                    self::setParams(self::$loaded_fee_list[$fee_id]);
                } else {
                    $target_ids[] = $fee_id;
                }
            }
            
            if ($target_ids) {
                $fee_models = Info::getDb()->cache(function($db) use ($target_ids) {
                    return Info::findAll($target_ids);
                });
                if (count($fee_models) != count($target_ids)) {
                    $ids_str = implode(',', $target_ids);
                    throw new InvalidParamException("Invalid param value (fee_ids: {$ids_str})");
                }
                foreach ($fee_models as $model) {
                    self::$loaded_fee_list[$model->id] = $model;
                    self::setParams($model);
                }
            }
        }
    }
    
    public static function getList()
    {
        return [
            'stock_deposit_per' => self::$stock_deposit_per,
            'buy_stock_per' => self::$buy_stock_per,
            'sell_stock_per' => self::$sell_stock_per,
            'user_earnings_per' => self::$user_earnings_per,
            'point_exchange_per' => self::$point_exchange_per,
        ];
    }
    
    private static function setParams($fee_model)
    {
        if (!is_null($fee_model->stock_deposit_per)) {
            self::$stock_deposit_per = $fee_model->stock_deposit_per;
        }
        if (!is_null($fee_model->buy_stock_per)) {
            self::$buy_stock_per = $fee_model->buy_stock_per;
        }
        if (!is_null($fee_model->sell_stock_per)) {
            self::$sell_stock_per = $fee_model->sell_stock_per;
        }
        if (!is_null($fee_model->user_earnings_per)) {
            self::$user_earnings_per = $fee_model->user_earnings_per;
        }
        if (!is_null($fee_model->point_exchange_per)) {
            self::$point_exchange_per = $fee_model->point_exchange_per;
        }
    }
}
