<?php

namespace common\models\fee;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fee_info".
 *
 * @property integer $id
 * @property string $stock_deposit_per
 * @property string $buy_stock_per
 * @property string $sell_stock_per
 * @property string $user_earnings_per
 * @property string $point_exchange_per
 * @property string $created_at
 * @property string $updated_at
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fee_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_deposit_per', 'buy_stock_per', 'sell_stock_per', 'user_earnings_per', 'point_exchange_per'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_deposit_per' => Yii::t('app', 'Stock Deposit Per'),
            'buy_stock_per' => Yii::t('app', 'Buy Stock Per'),
            'sell_stock_per' => Yii::t('app', 'Sell Stock Per'),
            'user_earnings_per' => Yii::t('app', 'User Earnings Per'),
            'point_exchange_per' => Yii::t('app', 'Point Exchange Per'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
