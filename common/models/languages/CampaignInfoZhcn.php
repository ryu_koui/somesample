<?php

namespace common\models\languages;

use Yii;

/**
 * This is the model class for table "campaign_info_lang_zh_CN".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 */
class CampaignInfoZhcn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_info_lang_zh-CN';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['description'], 'string'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
