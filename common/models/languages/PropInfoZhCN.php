<?php

namespace common\models\languages;

use Yii;

/**
 * This is the model class for table "prop_info_lang_zh_CN".
 *
 * @property integer $id
 * @property string $title
 * @property string $summary
 * @property string $description
 */
class PropInfoZhcn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_info_lang_zh-CN';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title', 'summary'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'summary' => Yii::t('app', 'Summary'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
