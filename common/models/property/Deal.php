<?php
namespace common\models\property;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;
use yii\db\Transaction;
use common\models\account;
use common\models\campaign;
use common\models\fee;
use common\models\user;

class Deal extends \yii\base\Model
{
    const SCENARIO_RESERVE = 'reserve';
    const SCENARIO_TRADE = 'trade';
    const FLOW_DATA_KEY_PREFIX = 'deal_flow_';
    const FLOW_DATA_EXPIRE = 900;
    
    public $prop_id;
    public $prop_info;
    public $buyable_stock_list;
    public $fixed_stocks;
    // public $reserved_stocks;
    public $free_stocks;
    public $free_price;
    public $buyer_cnt;
    public $account;
    public $flow_data;
    
    public $required_stock_list;
    public $required_price;
    public $required_num;
    
    public function scenarios()
    {
        return [
            self::SCENARIO_RESERVE => ['required_stock_list', 'required_price', 'required_num'],
            self::SCENARIO_TRADE => ['required_stock_list', 'required_price', 'required_num'],
        ];
    }
    
    public function rules()
    {
        return [
            ['required_stock_list', 'required', 'on' => self::SCENARIO_RESERVE],
        ];
    }
    
    public static function getInst($prop_id, $scenario = null)
    {
        $deal = new Deal();
        $deal->prop_id = $prop_id;
        if ($scenario) {
            $deal->scenario = $scenario;
        }
        return $deal;
    }
    
    public function attributeLabels()
    {
        return [
            'required_stock_list' => Yii::t('app', 'Selected Stocks'),
        ];
    }
    
    public function getStockDetail($user_id = null)
    {
        $this->prop_info = Info::getDb()->cache(function($db){
            return Info::findOne($this->prop_id);
        });
        if (!$this->prop_info) {
            throw new InvalidValueException('The property data does not exist: ' . $this->prop_id);
        }
        
        $this->buyer_cnt = $this->prop_info->stockState->buyer_cnt;
        $this->fixed_stocks = $this->prop_info->stockState->fixed_stocks;
        $onsale_list = $this->prop_info->stockState->onsale_list;
        $reserved_list = ReservedStocks::find($this->prop_id)->getReservedList($user_id);
        $this->free_stocks = 0;
        $this->free_price = 0;
        foreach ($onsale_list as $stock) {
            $data = [];
            if ($stock->user_id == $user_id) continue;
            if (isset($reserved_list[$stock->id])) {
                if ($stock->onsale_cnt <= $reserved_list[$stock->id]) continue;
                $data['onsale_cnt'] = $stock->onsale_cnt - $reserved_list[$stock->id];
            } else {
                $data['onsale_cnt'] = $stock->onsale_cnt;
            }
            $data['unit_price'] = $stock->unit_price;
            $data['username'] = $stock->userinfo->username;
            $data['stock_id'] = $stock->id;
            $this->buyable_stock_list[] = $data;
            
            $this->free_stocks += $data['onsale_cnt'];
            $this->free_price += $data['unit_price'] * $data['onsale_cnt'];
        }
        
        return $this;
    }
    
    public function reserveStock($user_id)
    {
        $this->prop_info = Info::getDb()->cache(function($db){
            return Info::findOne($this->prop_id);
        });
        if (!$this->prop_info) {
            throw new InvalidValueException('The property data does not exist: ' . $this->prop_id);
        }
        
        $onsale_list = $this->prop_info->stockState->onsale_list;
        $reserved_list = ReservedStocks::find($this->prop_id)->getReservedList($user_id);
        $counter = 0;
        
        foreach($onsale_list as $stock) {
            if (empty($this->required_stock_list[$stock->id])) continue;
            $required = $this->required_stock_list[$stock->id]['num'];
            $reserved = isset($reserved_list[$stock->id]) ? $reserved_list[$stock->id] : 0;
            if ($required > $stock->onsale_cnt - $reserved) {
                throw new InvalidParamException('Invalid param value (required_stock_list)');
            }
            $counter ++;
        }
        
        if ($counter != count($this->required_stock_list)) {
            throw new InvalidParamException('Invalid param value (required_stock_list)');
        }
        
        return (new ReservedStocks($this->prop_id))->save($this->required_stock_list);
    }
    
    public function saveFlowData($user_id)
    {
        if (!$this->prop_info) {
            $this->getStockDetail($user_id);
        }
        
        fee\Current::loadDefault();
        campaign\Service::apply(campaign\Service::ACT_BUY_STOCK, $this->prop_info->campaigns);
        
        $fee = ceil($this->required_price * fee\Current::$buy_stock_per / 100);
        $tax = ceil(($this->required_price + $fee) * Yii::$app->params['exciseTax'] / 100);
        $charge = ceil($this->required_price + $fee + $tax);
        
        $this->flow_data = [
            'user_id' => $user_id,
            'prop_id' => $this->prop_id,
            'required_stock_list' => $this->required_stock_list,
            'required_price' => $this->required_price,
            'required_num' => $this->required_num,
            'fee' => $fee,
            'tax' => $tax,
            'charge' => $charge,
        ];
        
        $key = self::FLOW_DATA_KEY_PREFIX . Yii::$app->security->generateRandomString();
        $result = Yii::$app->cache->set($key, $this->flow_data, self::FLOW_DATA_EXPIRE);
        
        if (!$result) {
            throw new InvalidValueException('The deal temp data save failed');
        }
        
        return $key;
    }

    public static function deleteFlowData($token)
    {
        return Yii::$app->cache->delete($token);
    }
    
    public static function virefyFlowData($user_id, $datas)
    {
        $flow_data = Yii::$app->cache->get($datas['token']);
        
        if (!$flow_data || $flow_data['user_id'] != $user_id) {
            throw new InvalidValueException('Flow data timeout.');
        }
        
        $model = self::getInst($flow_data['prop_id']);
        $model->prop_info = Info::findOne($flow_data['prop_id']);
        $onsale_list = Stocks::getOnsaleList($flow_data['prop_id']);
        $required_list = $flow_data['required_stock_list'];
        
        $counter = 0;
        $required_num = 0;
        $required_price = 0;
        foreach($onsale_list as $stock) {
            if (empty($required_list[$stock->id])) continue;
            
            if ($required_list[$stock->id]['num'] > $stock->onsale_cnt
                || $required_list[$stock->id]['unit_price'] != $stock->unit_price) {
                // TODO: エラーページ、価格の変更、トレード設定変更等の原因で手続き継続不可能
                throw new InvalidParamException('Some thing change after flow started');
            }
            $required_num += $required_list[$stock->id]['num'];
            $required_price += $required_list[$stock->id]['num'] * $required_list[$stock->id]['unit_price'];
            $counter ++;
        }
        if ($counter != count($required_list) || $required_num != $flow_data['required_num']
            || $required_price != $flow_data['required_price']) {
            // TODO: エラーページ、価格の変更、トレード設定変更等の原因で手続き継続不可能
            throw new InvalidParamException('Some thing change after flow started');
        }
        
        // アカウント確認、現在はいらない
        // $account = account\Manager::getById($datas['account_type'], $datas['account_id'], account\Manager::USE_PAYMENT);
        // if (!$account || $account->user_id != $user_id) {
            // throw new InvalidValueException('Invalid account.');
        // }
        // $model->account = $account;
        
        $flow_data['token'] = $datas['token'];
        $model->flow_data = $flow_data;
        
        return $model;
    }
    
    public function buy($user_id, $payment_data)
    {
        // TODO: select ... for update と READ_COMMITTED で更新対象をロック
        // $transaction = Yii::$app->db->beginTransaction(Transaction::READ_COMMITTED);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            
            $required_list = $this->flow_data['required_stock_list'];
            $target = Stocks::findAll(array_keys($required_list));
            
            if (count($target) != count($required_list)) {
                throw new InvalidValueException('Flow data is not safe.');
            }
            
            $total_num = 0;
            $total_price = 0;
            $price_list = [];
            $delete_stock_ids = [];
            $delete_detail_ids = [];
            foreach ($target as $stock) {
                if ($required_list[$stock->id]['num'] > $stock->onsale_cnt
                    || $stock->onsale_cnt > $stock->stock_cnt
                    || $required_list[$stock->id]['unit_price'] != $stock->unit_price) {
                    throw new InvalidValueException('Flow data is not safe.');
                }
                
                // 販売元の株減算or削除処理
                if ($required_list[$stock->id]['num'] == $stock->stock_cnt) {
                    // 特定ユーザの株買いきりの場合、元を削除
                    $delete_stock_ids[] = $stock->id;
                } else {
                    // 株詳細から買い取り分の株数を減算
                    $decrement = $required_list[$stock->id]['num'] * -1;
                    $stock->updateCounters(['stock_cnt' => $decrement, 'onsale_cnt' => $decrement]);
                    $stockDetails = $stock->getDetail()->andWhere('onsale_cnt > 0')->orderBy('buy_price DESC')->all();
                    foreach ($stockDetails as $detail) {
                        if ($decrement >= 0) break;
                        if ($detail->onsale_cnt > abs($decrement)) {
                            $detail->updateCounters(['stock_cnt' => $decrement, 'onsale_cnt' => $decrement]);
                            break;
                        } else {
                            $detail_dec = $detail->onsale_cnt * -1;
                            if ($detail->onsale_cnt == $detail->stock_cnt) {
                                $delete_detail_ids[] = $detail->id;
                            } else if ($detail->onsale_cnt < $detail->stock_cnt) {
                                $detail->updateCounters(['stock_cnt' => $detail_dec, 'onsale_cnt' => $detail_dec]);
                            } else {
                                throw new InvalidValueException('Stock detail data is not safe.' . "(ID: {$detail->id})");
                            }
                            $decrement -= $detail_dec;
                        }
                    }
                }
                
                $total_num += $required_list[$stock->id]['num'];
                $total_price += $required_list[$stock->id]['unit_price'] * $required_list[$stock->id]['num'];
                
                if (isset($price_list[$required_list[$stock->id]['unit_price']])) {
                    $price_list[$required_list[$stock->id]['unit_price']] += $required_list[$stock->id]['num'];
                } else {
                    $price_list[$required_list[$stock->id]['unit_price']] = $required_list[$stock->id]['num'];
                }
                
                // 売却元にポイント加算
                fee\Current::loadDefault();
                $fee = $total_price * fee\Current::$sell_stock_per / 100;
                $charge = $total_price - $fee;
                user\Point::sellStock($stock->user_id, $charge);
                
                // TODO: stock売却履歴挿入
                // TODO: 売りユーザに通知
                
            }
            
            // Stock系売却済みデータ一括削除
            if (count($delete_stock_ids) > 0) {
                Stocks::deleteAll(['id' => $delete_stock_ids]);
                StockDetail::deleteAll(['stock_id' => $delete_stock_ids]);
                StockCampaign::deleteAll(['stock_id' => $delete_stock_ids]);
            }
            if (count($delete_detail_ids) > 0) {
                StockDetail::deleteAll(['id' => $delete_detail_ids]);
                StockCampaign::deleteAll(['detail_id' => $delete_detail_ids]);
            }
            
            if ($total_price != $this->flow_data['required_price']) {
                throw new InvalidValueException('Flow data is not safe.');
            }
            
            $stocks_model = Stocks::find()->where([
                'prop_id' => $this->prop_id,
                'user_id' => $user_id,
            ])->one();
            
            if ($stocks_model) {
                $stocks_model->updateCounters(['stock_cnt' => $total_num]);
            } else {
                $stocks_model = new Stocks();
                $stocks_model->prop_id = $this->prop_id;
                $stocks_model->user_id = $user_id;
                $stocks_model->stock_cnt = $total_num;
                $stocks_model->onsale_cnt = 0;
                $stocks_model->unit_price = 0;
                $stocks_model->save();
            }
            
            $campaign_ids = ArrayHelper::getColumn($this->prop_info->campaigns, 'id');
            foreach ($price_list as $price => $num) {
                $detail_model = new StockDetail();
                $detail_model->stock_id = $stocks_model->id;
                $detail_model->stock_cnt = $num;
                $detail_model->buy_price = $price;
                $detail_model->save();
                
                foreach ($this->prop_info->campaigns as $campaign) {
                    $stock_camp = new StockCampaign();
                    $stock_camp->stock_id = $stocks_model->id;
                    $stock_camp->detail_id = $detail_model->id;
                    $stock_camp->camp_id = $campaign->id;
                    $stock_camp->save();
                }
            }
            
            // TODO: 支払い方法別制御
            
            // 購入分ポイント減算
            user\Point::expense($user_id, $this->flow_data['charge'], 0, account\PaypalService::type());
            
            // TODO: stock購入履歴挿入 (Paypal請求書ID等)
            
            // Paypal決済実行処理
            account\PaypalService::executPayment($payment_data);
            
            $this->prop_info->stockState->refreshData();
            
            (new ReservedStocks($this->prop_id))->delete($user_id);
            
            // self::deleteFlowData($this->flow_data['token']);
            $this->flow_data['complete'] = 1;
            Yii::$app->cache->set($this->flow_data['token'], $this->flow_data);
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return $stocks_model;
    }

}