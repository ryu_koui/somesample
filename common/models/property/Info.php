<?php

namespace common\models\property;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\components;
use common\models\campaign;
// use common\models\property\Campaign;

/**
 * This is the model class for table "prop_info".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property string $price
 * @property integer $stock_num
 * @property string $earning_at
 * @property string $expire
 * @property integer $region
 * @property string $city
 * @property integer $postal_code
 * @property string $address
 * @property integer $type
 * @property string $layout
 * @property string $summary
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Info extends components\db\ActiveRecord
{
    const SCENARIO_BASE  = 'default';
    const SCENARIO_INFO  = 'info';
    const SCENARIO_ALL   = 'all';
    
    public static $selectType = self::SCENARIO_BASE;
    
    public $lowest_price;
    public $onsale_total;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_info';
    }
    
    public function init()
    {
        $this->setScenario(self::$selectType);
        parent::init();
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'price', 'stock_num', 'region', 'type'], 'required'],
            [['status', 'stock_num', 'region', 'postal_code', 'type'], 'integer'],
            [['price', 'profit_rate'], 'number'],
            [['earning_at', 'expire'], 'safe'],
            [['description'], 'string'],
            [['title', 'address', 'summary'], 'string', 'max' => 255],
            [['city', 'layout'], 'string', 'max' => 32],
            ['status', 'in' , 'range' => Status::getValues()],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'status' => Yii::t('app', 'Status'),
            'earning_flg' => Yii::t('app', 'Earning Flag'),
            'trading_flg' => Yii::t('app', 'Trading Flag'),
            'price' => Yii::t('app', 'Price'),
            'stock_num' => Yii::t('app', 'Stock Num'),
            'earning_at' => Yii::t('app', 'Earning At'),
            'profit_type' => Yii::t('app', 'Profit Type'),
            'profit_id' => Yii::t('app', 'Profit ID'),
            'profit_rate' => Yii::t('app', 'Profit Rate'),
            'expire' => Yii::t('app', 'Expire'),
            'region' => Yii::t('app', 'Region'),
            'city' => Yii::t('app', 'City'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'address' => Yii::t('app', 'Address'),
            'type' => Yii::t('app', 'Type'),
            'layout' => Yii::t('app', 'Layout'),
            'summary' => Yii::t('app', 'Summary'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function needTranslatAttributes()
    {
        switch(self::$selectType) {    
            case self::SCENARIO_ALL:
                return [
                    'title' => 'title',
                    'summary' => 'summary',
                    'description' => 'description',
                ];
            case self::SCENARIO_INFO:
            case self::SCENARIO_BASE:
            default:
                return [
                    'title' => 'title',
                    'summary' => 'summary',
                ];
        }
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public function scenarios()
    {
        return [
            self::SCENARIO_BASE => self::getSelectColumns(self::SCENARIO_BASE),
            self::SCENARIO_INFO => self::getSelectColumns(self::SCENARIO_INFO),
            self::SCENARIO_ALL => $this->attributes(),
        ];
    }
    
    public static function getSelectColumns()
    {
        switch(self::$selectType) {
            case self::SCENARIO_INFO:
                return [
                    'id','title','status', 'price', 'stock_num', 'earning_at', 'profit_rate', 'expire',
                    'summary', 'region', 'city', 'postal_code', 'address', 'type', 'layout'
                ];
            case self::SCENARIO_ALL:
                return [];
            case self::SCENARIO_BASE:
            default:
                return [
                    'id','title','status', 'earning_flg', 'trading_flg', 'price', 'stock_num',
                    'earning_at', 'profit_type', 'profit_id', 'profit_rate', 'expire', 'summary',
                ];
        }
    }
    
    public function getStocks()
    {
        return $this->hasMany(Stocks::className(), ['prop_id' => 'id']);
    }
    
    public function getStockState()
    {
        return $this->hasOne(StockState::className(), ['prop_id' => 'id']);
    }
    
    public function getProfitHis()
    {
        return $this->hasMany(ProfitHistory::className(), ['prop_id' => 'id']);
    }
    
    public function getCampaigns()
    {
        return $this->hasMany(campaign\Info::className(), ['id' => 'camp_id'])
                    ->andOnCondition(['status' => campaign\Info::STATUS_ACTIVE])
                    ->viaTable(PropCampaign::tableName(), ['prop_id' => 'id']);
    }
    
    public function getNewProfitHis($fromDate = 'today 6 months ago')
    {
        $fromDate = (new \DateTime($fromDate))->format('Y-m-d H:i:s');
        return $this->hasMany(ProfitHistory::className(), ['prop_id' => 'id'])
                    ->where('created_at > :fromDate', ['fromDate' => $fromDate])
                    ->orderBy('created_at DESC');
    }
    
    public static function findForPanel($scenario = self::SCENARIO_INFO)
    {
        Info::$selectType = $scenario;
        return self::find()->with('campaigns')
                           ->select([self::tableName().'.*', 'MIN(unit_price) as lowest_price', 'SUM(onsale_cnt) as onsale_total'])
                           ->joinWith(['stocks' => function($query) {
                               $query->onCondition('onsale_cnt > :onsale_cnt', ['onsale_cnt' => 0]);
                           }])
                           ->groupBy('prop_id');
    }
    
    public static function findOneForPanel($id, $scenario = self::SCENARIO_INFO)
    {
        Info::$selectType = $scenario;
        return self::findForPanel($scenario)->andWhere([self::tableName().'.id' => $id])
                                            ->one();
    }
    
    public static function getRaisingList()
    {
        return self::findForPanel()->andWhere(['status' => Status::RAISING])->limit(3)->all();
    }
    
    public static function getOperatingList()
    {
        return self::findForPanel()->andWhere(['status' => Status::OPERATING])->limit(3)->all();
    }
    
    /**
     * @inheritdoc
     * @return InfoQuery the active query used by this AR class.
     */
    // public static function find()
    // {
        // $query = new InfoQuery(get_called_class());
        // $query->select(self::getSelectColumns());
        // return $query;
    // }
}
