<?php
namespace common\models\property;

use Yii;
use yii\base\InvalidValueException;
use yii\db\Expression;
use common\models\user;
use common\models\property;
use common\helpers\Utils;
use common\models\fee;
use common\models\campaign;

class Profit extends \yii\base\Model
{
    const TYPE_RENTAL = 1;
    const TYPE_AIRBNB = 2;
    
    public static function shareProfit($profitHis)
    {
        // echo "\n++++ For debug ++++++++++++++++++++++++++\n";
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            
            $stocks = property\Stocks::find()
                       ->with('detail')
                       ->where('prop_id = :prop_id and user_id <> :user_id',
                               ['prop_id' => $profitHis->prop_id, 'user_id' => Yii::$app->params['systemUserId']])
                       ->orderBy('prop_id, user_id')
                       ->all();
                       
            $user_cnt = 0;
            $total_shared_amount = 0;
            $total_shared_fee = 0;
            $total_shared_tax = 0;
            $user_earning_id_list = [];
            foreach ($stocks as $stock) {
                // echo "\n:: stock\n";
                // echo var_export($stock->toArray(), true) . "\n";
                
                $earnings = new user\EarningsHistory();
                $earnings->user_id = $stock->user_id;
                $earnings->prop_id = $stock->prop_id;
                $earnings->amount = 0;
                $earnings->fee = 0;
                $earnings->tax = 0;
                $earnings->stock_cnt = 0;
                $earnings->buy_price = 0;
                foreach ($stock->detail as $detail) {
                    $earnings->stock_cnt += $detail->stock_cnt;
                    $earnings->buy_price += ($detail->buy_price * $detail->stock_cnt);
                    
                    fee\Current::loadDefault();
                    campaign\Service::apply(campaign\Service::ACT_SHARE_PROFIT_PER_STOCK_DETAIL, $detail->campaigns);
                    
                    $target_amount = $detail->stock_cnt / $profitHis->propInfo->stock_num * $profitHis->amount;
                    $fee = $target_amount * fee\Current::$user_earnings_per / 100;
                    $earnings->fee += $fee;
                    
                    $earnings->amount += $target_amount;
                }
                // TODO: 取得税が必要なら税計算
                $earnings->tax = 0;
                $earnings->amount -= ($earnings->fee - $earnings->tax);
                
                if ($earnings->stock_cnt != $stock->stock_cnt) {
                    throw new InvalidValueException("prop_id: ({$stock->prop_id}) stock_id: ({$stock->id}) のstock_cnt数が不整合");
                }
                
                // echo "\n:: earnings_his\n";
                // echo var_export($earnings->toArray(), true) . "\n";
                
                $earnings->save();
                $earningsID = Yii::$app->db->getLastInsertID();
                $user_earning_id_list[] = $earningsID;
                
                user\Point::earning($earnings->user_id, $earnings->amount);
                
                $total_shared_amount += $earnings->amount;
                $total_shared_fee += $earnings->fee;
                $total_shared_tax += $earnings->tax;
                $user_cnt ++;
            }
            
            $profitHis->shared_amount = $total_shared_amount;
            $profitHis->shared_fee = $total_shared_fee;
            $profitHis->shared_tax = $total_shared_tax;
            $profitHis->user_cnt = $user_cnt;
            $profitHis->shared_at = new Expression('NOW()');
            $profitHis->save();
            
            user\EarningsHistory::updateAll(['profit_his_id' => $profitHis->id], ['id' => $user_earning_id_list]);
            
            // echo "\n:: profit_his\n";
            // echo var_export($profitHis->toArray(), true) . "\n";
            // echo "-----------------------------------------\n";
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
    }
    
}