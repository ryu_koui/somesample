<?php

namespace common\models\property;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "prop_profit_his".
 *
 * @property integer $id
 * @property integer $prop_id
 * @property integer $profit_type
 * @property integer $receipt_id
 * @property string $amount
 * @property integer $share_his_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProfitHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_profit_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prop_id', 'profit_type', 'amount'], 'required'],
            [['prop_id', 'profit_type', 'receipt_id', 'user_cnt'], 'integer'],
            [['amount', 'shared_amount', 'shared_fee', 'shared_tax'], 'number'],
            [['created_at', 'updated_at', 'shared_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prop_id' => Yii::t('app', 'Prop ID'),
            'profit_type' => Yii::t('app', 'Profit Type'),
            'receipt_id' => Yii::t('app', 'Receipt ID'),
            'amount' => Yii::t('app', 'Amount'),
            'shared_at' => Yii::t('app', 'Shared At'),
            'user_cnt' => Yii::t('app', 'User Cnt'),
            'shared_amount' => Yii::t('app', 'Shared Amount'),
            'shared_fee' => Yii::t('app', 'Shared fee'),
            'shared_tax' => Yii::t('app', 'Shared tax'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getPropInfo()
    {
        return $this->hasOne(Info::className(), ['id' => 'prop_id']);
    }
}
