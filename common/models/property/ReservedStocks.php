<?php

namespace common\models\property;

use Yii;
use yii\base\InvalidParamException;

/**
 * This is the model class for redis key "prop_reserved_stocks".
 *
 */
class ReservedStocks extends \yii\base\Model
{
    const EXPIRE_SEC = 900;
    const KEY_PREFIX = 'prop_reserved_stock_';
    
    private $_key;
    private $_current_time;
    private $_expired_until;
    
    public $prop_id;
    public $list;
    
    public static function find($prop_id)
    {
        return new ReservedStocks($prop_id);
    }
    
    public static function getDb()
    {
        return \Yii::$app->get('redis');
    }
    
    public function __construct($prop_id, $config = [])
    {
        $this->_key = self::KEY_PREFIX . $prop_id;
        $this->_current_time = time();
        $this->_expired_until = $this->_current_time - self::EXPIRE_SEC;
        $this->prop_id = $prop_id;
        parent::__construct($config);
    }
    
    public function save($stock_list)
    {
        if (!$this->prop_id || !Yii::$app->user->id) {
            throw new InvalidParamException('Invialid param value (prop_id or user_id)');
        }
        $this->getList();
        
        self::getDb()->MULTI();
        
        foreach($this->list as $row) {
            list($uid, $stock_id, $num) = explode('_', $row);
            if ($uid == Yii::$app->user->id) {
                self::getDb()->ZREM($this->_key, $row);
            }
        }
        
        foreach($stock_list as $stock_id => $stock) {
            $member = Yii::$app->user->id . '_' . $stock_id . '_' . $stock['num'];
            self::getDb()->ZADD($this->_key, $this->_current_time, $member);
        }
        
        self::getDb()->EXPIRE($this->_key, self::EXPIRE_SEC);
        
        $result = self::getDb()->EXEC();
        
        return (isset($result[0]) && $result[0]) ? true : false;
    }
    
    public function delete($user_id)
    {
        $this->getList();
        self::getDb()->MULTI();
        foreach($this->list as $row) {
            list($uid, $stock_id, $num) = explode('_', $row);
            if ($uid == $user_id) {
                self::getDb()->ZREM($this->_key, $row);
            }
        }
        self::getDb()->EXEC();
    }
    
    public function getlist()
    {
        if (!is_array($this->list)) {
            if (self::getDb()->EXISTS($this->_key)) {
                self::getDb()->MULTI();
                self::getDb()->ZREMRANGEBYSCORE($this->_key, '-inf', $this->_expired_until);
                self::getDb()->ZRANGEBYSCORE($this->_key, '-inf', '+inf');
                $result = self::getDb()->EXEC();
                $this->list = (isset($result[1]) && is_array($result[1])) ? $result[1] : [];
            } else {
                $this->list = [];
            }
        }
        return $this->list;
    }

    public function getReservedList($user_id = null)
    {
        $this->getList();
        
        $result = [];
        foreach ($this->list as $row) {
            list($uid, $stock_id, $num) = explode('_', $row);
            if ($user_id && $uid != $user_id) {
                if (!isset($result[$stock_id])) $result[$stock_id] = 0;
                $result[$stock_id] += $num;
            }
        }
        
        return $result;
    }

}
