<?php
namespace common\models\property;

use Yii;
use yii\base\InvalidValueException;
use yii\base\InvalidParamException;

class Sell extends \yii\base\Model
{
    public $num;
    public $unit_price;

    public $prop_id;
    public $user_id;
    
    public $userStock;
    public $numMaxValue = 0;
    
    public function rules()
    {
        return [
            [['num', 'unit_price'], 'required'],
            [['num'], 'integer', 'min' => 1, 'max' => $this->numMaxValue],
            [['unit_price'], 'integer', 'min' => 1],
        ];
    }
    
    public static function getInst($prop_id, $user_id)
    {
        $sell = new Sell();
        $sell->prop_id = $prop_id;
        $sell->user_id = $user_id;
        
        $sell->userStock = Stocks::find()
                            ->where(['prop_id' => $prop_id, 'user_id' => $user_id])
                            ->one();
                            
        if ($sell->userStock) {
            $sell->numMaxValue = $sell->userStock->stock_cnt;
        }
        
        return $sell;
    }
    
    public function sellStock()
    {
        if (!$this->userStock || !$this->validate() || $this->num > $this->userStock->stock_cnt) {
            return false;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->userStock->onsale_cnt = $this->num;
            $this->userStock->unit_price = $this->unit_price;
            $this->userStock->save();
            
            $sell_num = $this->num;
            $stockDetail = $this->userStock->getDetail()->orderBy('buy_price DESC')->all();
            foreach ($stockDetail as $detail) {
                if ($sell_num > 0) {
                    if ($detail->stock_cnt < $sell_num) {
                        $detail->onsale_cnt = $detail->stock_cnt;
                        $sell_num -= $detail->stock_cnt;
                    } else {
                        $detail->onsale_cnt = $sell_num;
                        $sell_num = 0;
                    }
                    $detail->save();
                } else {
                    if ($detail->onsale_cnt > 0) {
                        $detail->onsale_cnt = 0;
                        $detail->save();
                    }
                }
            }
            
            $stockState = new StockState();
            $stockState->prop_id = $this->prop_id;
            $stockState->refreshData();
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return true;
    }
}