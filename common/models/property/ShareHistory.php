<?php

namespace common\models\property;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "prop_share_his".
 *
 * @property integer $id
 * @property integer $prop_id
 * @property string $amount
 * @property integer $profit_cnt
 * @property integer $user_cnt
 * @property string $created_at
 * @property string $updated_at
 */
class ShareHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_share_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prop_id', 'amount', 'profit_cnt', 'user_cnt'], 'required'],
            [['prop_id', 'profit_cnt', 'user_cnt'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prop_id' => Yii::t('app', 'Prop ID'),
            'amount' => Yii::t('app', 'Amount'),
            'profit_cnt' => Yii::t('app', 'Profit Cnt'),
            'user_cnt' => Yii::t('app', 'User Cnt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
