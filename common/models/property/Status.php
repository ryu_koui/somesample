<?php
namespace common\models\property;

Class Status
{
    const DELETED   = -1;
    const DISABLED  = 0;
    const RAISING   = 10;
    const OPERATING  = 20;
    
    public static function getList()
    {
        return [
            self::DELETED   => 'DELETED',
            self::DISABLED  => 'DISABLED',
            self::RAISING   => 'RAISING',
            self::OPERATING => 'OPERATING',
        ];
    }
    
    public static function getValues()
    {
        return [self::DELETED, self::DISABLED, self::RAISING, self::OPERATING];
    }
}
