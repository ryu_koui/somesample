<?php

namespace common\models\property;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "prop_stock_campaign".
 *
 * @property integer $id
 * @property integer $stock_id
 * @property integer $detail_id
 * @property integer $camp_id
 * @property string $created_at
 * @property string $updated_at
 */
class StockCampaign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_stock_campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'detail_id', 'camp_id'], 'required'],
            [['stock_id', 'detail_id', 'camp_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_id' => Yii::t('app', 'Stock ID'),
            'detail_id' => Yii::t('app', 'Detail ID'),
            'camp_id' => Yii::t('app', 'Camp ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
}
