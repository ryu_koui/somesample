<?php

namespace common\models\property;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\models\campaign;

/**
 * This is the model class for table "prop_stock_detail".
 *
 * @property integer $id
 * @property integer $stock_id
 * @property integer $stock_cnt
 * @property string $buy_price
 * @property integer $onsale_cnt
 * @property string $created_at
 * @property string $updated_at
 */
class StockDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_stock_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'stock_cnt', 'buy_price'], 'required'],
            [['stock_id', 'stock_cnt', 'onsale_cnt'], 'integer'],
            [['buy_price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_id' => Yii::t('app', 'Stock ID'),
            'stock_cnt' => Yii::t('app', 'Stock Cnt'),
            'buy_price' => Yii::t('app', 'Buy Price'),
            'onsale_cnt' => Yii::t('app', 'Onsale Cnt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getCampaigns()
    {
        return $this->hasMany(campaign\Info::className(), ['id' => 'camp_id'])
                    ->viaTable(StockCampaign::tableName(), ['detail_id' => 'id']);
    }
}
