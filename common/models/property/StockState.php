<?php

namespace common\models\property;

use Yii;

/**
 * This is the model class for redis key "prop_stock_state".
 *
 * @property integer $prop_id
 * @property integer $fixed_stocks
 * @property integer $buyer_cnt
 */
class StockState extends \yii\redis\ActiveRecord
{
    public $onsale_list = [];
    
    public static function primaryKey()
    {
        return ['prop_id'];
    }
    
    public function rules()
    {
        return [
            [['prop_id', 'fixed_stocks', 'buyer_cnt', 'onsale_str'], 'required'],
        ];
    }
    
    public function attributes()
    {
        return ['prop_id', 'fixed_stocks', 'buyer_cnt', 'onsale_str'];
    }
    
    public static function find()
    {
        return new StockStateQuery(get_called_class());
    }
    
    public function refreshData()
    {
        $count = Stocks::calculatState($this->prop_id);
        $this->buyer_cnt = $count['buyer_cnt'];
        $this->fixed_stocks = $count['fixed_stocks'];
        $this->onsale_list = Stocks::getOnsaleList($this->prop_id);
        $this->onsale_str = serialize($this->onsale_list);
        $this->save();
    }
}
