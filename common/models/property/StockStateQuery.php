<?php
namespace common\models\property;

class StockStateQuery extends \yii\redis\ActiveQuery
{
    public function one($db = null)
    {
        $model = parent::one();
        
        if (is_null($model) && $this->primaryModel) {
            $model = new StockState();
            $model->prop_id = $this->primaryModel->primaryKey;
            $model->refreshData();
        } else {
            if ($list = unserialize($model->onsale_str)) {
                $model->onsale_list = $list;
            }
        }
        
        return $model;
    }
}
