<?php

namespace common\models\property;

use Yii;
use common\models;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\models\user;
use common\models\campaign;

/**
 * This is the model class for table "prop_stocks".
 *
 * @property integer $prop_id
 * @property integer $user_id
 * @property integer $stock_cnt
 * @property string $created_at
 * @property string $updated_at
 */
class Stocks extends \yii\db\ActiveRecord
{
    public $lastEarning;
    
    // public $lastEarning = null;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prop_stocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prop_id', 'user_id', 'stock_cnt', 'onsale_cnt'], 'required'],
            [['prop_id', 'user_id', 'stock_cnt', 'onsale_cnt'], 'integer'],
            [['unit_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prop_id' => Yii::t('app', 'Prop ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'stock_cnt' => Yii::t('app', 'Stock Cnt'),
            'onsale_cnt' => Yii::t('app', 'On Sale Cnt'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public function getPropinfo()
    {
        return $this->hasOne(Info::className(), ['id' => 'prop_id']);
    }
    
    public function getUserinfo()
    {
        return $this->hasOne(user\Info::className(), ['id' => 'user_id']);
    }
    
    public function getDetail()
    {
        return $this->hasMany(StockDetail::className(), ['stock_id' => 'id']);
    }
    
    public function getUserEarnings()
    {
        return $this->hasMany(user\EarningsHistory::className(), ['user_id' => 'user_id', 'prop_id' => 'prop_id']);
    }
    
    public function getDetailCampaigns()
    {
        return $this->hasMany(campaign\Info::className(), ['id' => 'camp_id'])
                    ->viaTable(StockCampaign::tableName(), ['stock_id' => 'id']);
    }
    
    public function getLastEarning()
    {
        if (is_null($this->lastEarning)) {
            $last = user\EarningsHistory::getLastByUser($this->user_id, $this->prop_id);
            if (!$last) $last = (new user\EarningsHistory())->loadDefaultValues();
            $this->lastEarning = $last;
        }
        
        return $this->lastEarning;
    }
    
    public function getPurchaseTotal()
    {
        $detail_tn = StockDetail::tableName();
        return self::find()->joinWith('detail')
                           ->where(['prop_id' => $this->prop_id, 'user_id' => $this->user_id])
                           ->select("SUM({$detail_tn}.stock_cnt * {$detail_tn}.buy_price) as purchase_total")
                           ->scalar();
    }
    
    public static function getOnsaleList($prop_id)
    {
        return self::find()->where('prop_id = :prop_id and onsale_cnt > :onsale_cnt',
                                   ['prop_id' => $prop_id, 'onsale_cnt' => 0])
                           ->with('userinfo')
                           ->orderBy('unit_price')
                           ->all();
    }
    
    public static function getUserPropList($user_id, $limit = 10)
    {
        return self::find()->joinWith('propinfo')
                           ->where('user_id = :user_id and status > :status',
                                   ['user_id' => $user_id, 'status' => Status::DISABLED])
                           ->orderBy(self::tableName().'.updated_at DESC')
                           ->limit($limit)
                           ->all();
    }
    
    public static function calculatState($prop_id)
    {
        $standerd = self::find()->select(['count(distinct user_id) as buyer_cnt', 'ifnull(sum(stock_cnt - onsale_cnt),0) as fixed_stocks'])
                                ->where('prop_id = :prop_id and user_id <> :user_id',
                                        ['prop_id' => $prop_id, 'user_id' => Yii::$app->params['systemUserId']])
                                ->asArray()->one();
                                
        $buyable = self::find()->select(['ifnull(sum(onsale_cnt),0) as buyable_cnt',
                                         'sum(unit_price * onsale_cnt) as buyable_price'])
                               ->where('prop_id = :prop_id and onsale_cnt > :onsale_cnt',
                                       ['prop_id' => $prop_id, 'onsale_cnt' => 0])
                               ->asArray()->one();
        
        return array_merge(['prop_id' => $prop_id], $standerd, $buyable);
    }
    
}
