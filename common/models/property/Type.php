<?php
namespace common\models\property;

Class Type
{
    private static $_datas = [
        1 => 'マンション',
        2 => '一戸建て',
    ];
    
    public static function getAll()
    {
        return self::$_datas;
    }
    
    public static function get($id)
    {
        return isset(self::$_datas[$id]) ? self::$_datas[$id] : false;
    }
}
