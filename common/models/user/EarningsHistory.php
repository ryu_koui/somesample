<?php

namespace common\models\user;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "user_earnings_his".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $prop_id
 * @property string $amount
 * @property string $fee
 * @property integer $stock_cnt
 * @property string $buy_price
 * @property string $created_at
 * @property string $updated_at
 */
class EarningsHistory extends \yii\db\ActiveRecord
{
    public $total_amount = 0;
    public $per = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_earnings_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'prop_id', 'amount', 'fee', 'stock_cnt', 'buy_price'], 'required'],
            [['user_id', 'prop_id', 'stock_cnt', 'profit_his_id'], 'integer'],
            [['amount', 'fee', 'tax', 'buy_price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'prop_id' => Yii::t('app', 'Prop ID'),
            'amount' => Yii::t('app', 'Amount'),
            'fee' => Yii::t('app', 'Fee'),
            'tax' => Yii::t('app', 'Tax'),
            'stock_cnt' => Yii::t('app', 'Stock Cnt'),
            'buy_price' => Yii::t('app', 'Buy Price'),
            'profit_his_id' => Yii::t('app', 'Profit History ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public function getPER()
    {
        if (!$this->buy_price || $this->buy_price == 0 || !$this->amount || $this->amount == 0) return 0;
        return $this->amount * 12 / $this->buy_price * 100;
    }
    
    public static function getListByUser($user_id, $prop_id = null, $options = [])
    {
        $query = self::find()->where(['user_id' => $user_id])
                             ->orderBy('created_at ASC');
        if ($prop_id) {
            $query->andWhere(['prop_id' => $prop_id]);
        }
        if (!empty($options['fromDate'])) {
            $fromDate = (new \DateTime($options['fromDate']))->format('Y-m-d H:i:s');
            $query->andWhere('created_at > :fromDate', ['fromDate' => $fromDate]);
        }
        return $query;
    }
    
    public static function getLastByUser($user_id, $prop_id, $fromDate = 'today 1 month ago')
    {
        $fromDate = (new \DateTime($fromDate))->format('Y-m-d H:i:s');
        return self::find()->select(['*, SUM(amount) as total_amount', '(SUM(amount)*12 / buy_price * 100) as per'])
                           ->where('user_id = :user_id and prop_id = :prop_id and created_at > :fromDate',
                                   ['user_id' => $user_id, 'prop_id' => $prop_id, 'fromDate' => $fromDate])
                           ->groupBy('prop_id')->orderBy('created_at DESC')->one();
    }
}
