<?php

namespace common\models\user;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\base\InvalidValueException;

/**
 * This is the model class for table "user_point".
 *
 * @property integer $user_id
 * @property integer $points
 * @property integer $payout_account
 * @property integer $payment_account
 * @property string $created_at
 * @property string $updated_at
 */
class Point extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['points', 'total_earnings', 'total_expenses'], 'number'],
            [['payout_account', 'payment_account'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'points' => Yii::t('app', 'Points'),
            'payout_account' => Yii::t('app', 'Payout Account'),
            'payment_account' => Yii::t('app', 'Payment Account'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public static function buy($user_id, $amount)
    {
        return self::income($user_id, $amount, PointHistory::ACT_BUY_POINTS, false);
    }
    
    public static function earning($user_id, $amount)
    {
        return self::income($user_id, $amount, PointHistory::ACT_PROP_EARINGS);
    }
    
    public static function sellStock($user_id, $amount)
    {
        return self::income($user_id, $amount, PointHistory::ACT_SELL_STOCK);
    }
    
    public static function expense($user_id, $amount, $use_points = 0, $account_type = '')
    {
        // TODO: 通貨の種類によって処理が変わる
        $total_points = $amount;
        
        $use_points = $total_points < $use_points ? $total_points : $use_points;
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $point_model = self::findOne($user_id);
            
            if ($point_model->points < $use_points) {
                throw new InvalidValueException('Not enough points.');
            }
            
            if ($total_points > $use_points) {
                // 支払いに不足分のポイントの購入履歴追加
                $point_his = new PointHistory();
                $point_his->user_id = $point_model->user_id;
                $point_his->action = PointHistory::ACT_BUY_POINTS;
                $point_his->change = $total_points - $use_points;
                $point_his->points = $new_points = $point_model->points + $point_his->change;
                $point_his->total_earnings = $point_model->total_earnings;
                $point_his->total_expenses = $point_model->total_expenses;
                $point_his->account_type = $account_type;
                $point_his->save();
            }
            
            // 支払い分のポイント消費履歴を追加
            $point_his = new PointHistory();
            $point_his->user_id = $point_model->user_id;
            $point_his->action = PointHistory::ACT_BUY_STOCK;
            $point_his->change = $total_points * -1;
            $point_his->points = $new_points - $total_points;
            $point_his->total_earnings = $point_model->total_earnings;
            $point_his->total_expenses = $point_model->total_expenses + $amount;
            $point_his->account_type = $account_type;
            $point_his->save();
            
            if ($use_points > 0) {
                $point_model->updateCounters(['total_expenses' => $amount, 'points' => $use_points * -1]);
            } else {
                $point_model->updateCounters(['total_expenses' => $amount]);
            }
            $point_model->updated_at = new Expression('NOW()');
            $point_model->save();
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return $point_model;
    }

    public static function income($user_id, $amount, $his_action, $increase_earnings = true)
    {
        // TODO: 通貨の種類によって処理が変わる
        $points = $amount;
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $point_model = self::findOne($user_id);
            $counter_cond = ['points' => $points];
            if ($increase_earnings) {
                $counter_cond['total_earnings'] = $amount;
            }
            $point_model->updateCounters($counter_cond);
            $point_model->updated_at = new Expression('NOW()');
            $point_model->save();
            
            $point_his = new PointHistory();
            $point_his->user_id = $point_model->user_id;
            $point_his->action = $his_action;
            $point_his->change = $points;
            $point_his->points = $point_model->points;
            $point_his->total_earnings = $point_model->total_earnings;
            $point_his->total_expenses = $point_model->total_expenses;
            $point_his->save();
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return $point_model;
    }
    
    public static function outgo($user_id, $amount, $his_action, $increase_earnings = false)
    {
        $points = $amount;
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $point_model = self::findOne($user_id);
            if ($point_model->points < $amount) {
                throw new InvalidValueException('Not enough points.');
            }
            
            $counter_cond = ['points' => $points * -1];
            if ($increase_earnings) {
                $counter_cond['total_expenses'] = $amount;
            }
            $point_model->updateCounters($counter_cond);
            $point_model->updated_at = new Expression('NOW()');
            $point_model->save();
            
            $point_his = new PointHistory();
            $point_his->user_id = $point_model->user_id;
            $point_his->action = $his_action;
            $point_his->change = $points * -1;
            $point_his->points = $point_model->points;
            $point_his->total_earnings = $point_model->total_earnings;
            $point_his->total_expenses = $point_model->total_expenses;
            $point_his->save();
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        return $point_model;
    }
}
