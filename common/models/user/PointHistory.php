<?php

namespace common\models\user;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_point_his".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $action
 * @property integer $change
 * @property integer $points
 * @property string $total_earnings
 * @property string $total_expenses
 * @property string $account_type
 * @property string $created_at
 * @property string $updated_at
 */
class PointHistory extends \yii\db\ActiveRecord
{
    const ACT_EXCHANGE_POINTS = 1;
    const ACT_BUY_STOCK = 2;
    const ACT_RESERVE_STOCK = 3;
    
    const ACT_BUY_POINTS = 101;
    const ACT_SELL_STOCK = 102;
    const ACT_PROP_EARINGS = 103;
    const ACT_CANCEL_RESERVE = 104;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_point_his';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'action', 'change', 'points', 'total_earnings', 'total_expenses'], 'required'],
            [['user_id', 'action'], 'integer'],
            [['change', 'points', 'total_earnings', 'total_expenses'], 'number'],
            [['account_type'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'action' => Yii::t('app', 'Action'),
            'change' => Yii::t('app', 'Change'),
            'points' => Yii::t('app', 'Points'),
            'total_earnings' => Yii::t('app', 'Total Earnings'),
            'total_expenses' => Yii::t('app', 'Total Expenses'),
            'account_type' => Yii::t('app', 'Account Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    public static function actionLabels()
    {
        return [
            self::ACT_EXCHANGE_POINTS => Yii::t('app', 'ポイント換金'),
            self::ACT_BUY_STOCK => Yii::t('app', '物件株購入'),
            self::ACT_RESERVE_STOCK => Yii::t('app', '物件株予約'),
            
            self::ACT_BUY_POINTS => Yii::t('app', 'ポイント購入'),
            self::ACT_SELL_STOCK => Yii::t('app', '物件下部売却'),
            self::ACT_PROP_EARINGS => Yii::t('app', '物件収益'),
            self::ACT_CANCEL_RESERVE => Yii::t('app', '物件株予約キャンセル'),
        ];
    }

    public static function getActionLabel($act_id)
    {
        $labels = self::actionLabels();
        return $labels[$act_id];
    }
}
