<?php

namespace common\widgets;
 
use Yii;
use yii\base\Component;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\web\Cookie;
 
class languageSwitcher extends Widget
{
    public $languages = [
        'ja-JP' => '日本語',
        'zh-CN' => '中文',
    ];
    
    public function init()
    {
        if(php_sapi_name() === 'cli') {
            return true;
        }
 
        parent::init();
 
        if ($new = Yii::$app->request->get('lang')) {
            if (isset($this->languages[$new])) {
                Yii::$app->request->set('lang', $new);
            }
        }
        
        Yii::$app->language = Yii::$app->request->get('lang');
    }
}