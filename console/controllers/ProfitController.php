<?php
namespace console\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use common\models\property;
use common\models\user;

class ProfitController extends \yii\console\Controller
{
    public $defaultAction = 'share';
    
    /**
     * 物件収益の分配処理.
     *
     * [prop_profit_his]のデータから各ユーザへの配当金を計算し、分配処理を行う
     * 実行対象は未分配データ全部
     *
     * 以下はコマンド例：
     * 
     * ~~~
     * yii profit/share
     * yii profit/share 10
     * ~~~
     *
     * @param string $limit 実行件数制限
     * デフォルトは未分配データ全部、数値の場合古い順から数値分を実行
     */
    public function actionShare($limit = 'all')
    {
        $limit = is_numeric($limit) && $limit > 0 ? $limit : false;
        $executed_cnt = 0;
        
        $phTN = property\ProfitHistory::tableName();
        $infoTN = property\Info::tableName();
        $profitHisQuery = property\ProfitHistory::find()
                        ->with('propInfo')
                        ->where(['shared_at' => null])
                        ->orderBy("created_at ASC");
                        
        if ($limit) $profitHisQuery->limit($limit);
        
        $profitHisBatch = $profitHisQuery->each(100);
        foreach ($profitHisBatch as $profitHis) {
            $this->stdout("\n++++ For debug ++++++++++++++++++++++++++\n", Console::BOLD);
            $this->stdout(var_export($profitHis->toArray(), true) . "\n");
            // $this->stdout("-----------------------------------------\n", Console::BOLD);
            
            try {
                property\Profit::shareProfit($profitHis);
            } catch(\Exception $e) {
                $message = "分配処理失敗";
                Yii::error($message);
                Yii::error($e->__toString());
                $this->stdout("\n{$message}\n", Console::FG_RED);
                $this->stdout($e->__toString() . "\n\n", Console::FG_YELLOW);
                continue;
            }
            
            $this->stdout("\n処理物件： {$profitHis->propInfo->title} (ID: {$profitHis->prop_id})\n");
            $executed_cnt ++;
        }
        
        $this->stdout("\n処理完了 ($executed_cnt)\n", Console::FG_GREEN);
        return 0;
    }
    
}
