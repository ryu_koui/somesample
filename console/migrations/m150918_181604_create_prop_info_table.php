<?php

use yii\db\Migration;

class m150918_181604_create_prop_info_table extends Migration
{
    
    public function up()
    {
        $this->createTable('prop_info', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'status' => 'tinyint(4) NOT NULL',
            'price' => $this->money(15, 3)->notNull(),
            'stock_num' => $this->integer()->notNull(),
            'earning_at' => $this->datetime(),
            'expire' => $this->datetime(),
            'region' => $this->smallInteger()->notNull(),
            'city' => $this->string(32),
            'postal_code' => $this->smallInteger(),
            'address' => $this->string(255),
            'type' => 'tinyint NOT NULL',
            'layout' => $this->string(32),
            'summary' => $this->string(255),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_info', true) !== null) {
            $this->dropTable('prop_info');
        }
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
