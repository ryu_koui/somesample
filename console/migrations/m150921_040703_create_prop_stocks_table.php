<?php

use yii\db\Migration;

class m150921_040703_create_prop_stocks_table extends Migration
{
    // public function up()
    // {
    // }
// 
    // public function down()
    // {
    // }

    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('prop_stocks', [
            'prop_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'stock_cnt' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->addPrimaryKey('prop_stocks_pk', 'prop_stocks', ['prop_id', 'user_id']);
    }

    public function safeDown()
    {
        if ($this->db->schema->getTableSchema('prop_stocks', true) !== null) {
            $this->dropTable('prop_stocks');
        }
    }
    
}
