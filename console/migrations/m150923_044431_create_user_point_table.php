<?php

use yii\db\Migration;

class m150923_044431_create_user_point_table extends Migration
{
    /*
    public function up()
    {
    }

    public function down()
    {
    }
    */

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('user_point', [
            'user_id' => $this->primaryKey(),
            'points' => $this->money(15, 3)->notNull()->defaultValue(0),
            'total_earnings' => $this->money(15, 3)->notNull()->defaultValue(0),
            'total_expenses' => $this->money(15, 3)->notNull()->defaultValue(0),
            'payout_account' => $this->string(32),
            'payment_account' => $this->string(32),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        
        $this->createTable('account_creditcard', [
            'id' => $this->primaryKey(),
            'user_id' => $this->smallInteger()->notNull(),
            'type' => $this->string(32)->notNull(),
            'number' => $this->string(20)->notNull(),
            'expire_month' => $this->smallInteger()->notNull(),
            'expire_year' => $this->smallInteger()->notNull(),
            'expire_year' => $this->smallInteger()->notNull(),
            'name' => $this->string(255)->notNull(),
            'payout_default' => $this->smallInteger()->notNull()->defaultValue(0),
            'payment_default' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('user_id', 'account_creditcard', 'user_id');
    }

    public function safeDown()
    {
        if ($this->db->schema->getTableSchema('user_point', true) !== null) {
            $this->dropTable('user_point');
        }

        if ($this->db->schema->getTableSchema('account_creditcard', true) !== null) {
            $this->dropTable('account_creditcard');
        }
    }
    
}
