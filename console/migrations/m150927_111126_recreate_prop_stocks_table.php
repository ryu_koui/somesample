<?php

use yii\db\Migration;

class m150927_111126_recreate_prop_stocks_table extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('prop_stocks', true) !== null) {
            $this->dropTable('prop_stocks');
        }
        
        $this->createTable('prop_stocks', [
            'id' => $this->primaryKey(),
            'prop_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'stock_cnt' => $this->integer()->notNull(),
            'onsale_cnt' => $this->integer()->notNull()->defaultValue(0),
            'unit_price' => $this->money(15, 3)->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('prop_id', 'prop_stocks', 'prop_id');
        $this->createIndex('user_id', 'prop_stocks', 'user_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_stocks', true) !== null) {
            $this->dropTable('prop_stocks');
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
