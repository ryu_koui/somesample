<?php

use yii\db\Migration;

class m151104_135449_change_prop_info_status_column extends Migration
{
    public function up()
    {
        if ($propInfoSchema = $this->db->schema->getTableSchema('prop_info', true)) {
            if (is_null($propInfoSchema->getColumn('trading_flg'))) {
                $this->addColumn('prop_info', 'trading_flg', 'tinyint NOT NULL DEFAULT 0 AFTER status');
            }
            if (is_null($propInfoSchema->getColumn('earning_flg'))) {
                $this->addColumn('prop_info', 'earning_flg', 'tinyint NOT NULL DEFAULT 0 AFTER status');
            }
        }
    }

    public function down()
    {
        if ($propInfoSchema = $this->db->schema->getTableSchema('prop_info', true)) {
            if (!is_null($propInfoSchema->getColumn('trading_flg'))) {
                $this->dropColumn('prop_info', 'trading_flg');
            }
            if (!is_null($propInfoSchema->getColumn('earning_flg'))) {
                $this->dropColumn('prop_info', 'earning_flg');
            }
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
