<?php

use yii\db\Migration;

class m151106_223947_create_prop_profit extends Migration
{
    // public function up()
    // {
// 
    // }
// 
    // public function down()
    // {
        // echo "m151106_223947_create_prop_profit cannot be reverted.\n";
// 
        // return false;
    // }

    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        if ($this->db->schema->getTableSchema('prop_profit_his', true) !== null) {
            $this->dropTable('prop_profit_his');
        }
        
        $this->createTable('prop_profit_his', [
            'id' => $this->primaryKey(),
            'prop_id' => $this->integer()->notNull(),
            'profit_type' => $this->smallInteger()->notNull(),
            'receipt_id' => $this->integer()->notNull()->defaultValue(0),
            'amount' => $this->money(15, 3)->notNull(),
            'created_at' => $this->dateTime()->defaultValue(null),
            'user_cnt' => $this->integer()->notNull()->defaultValue(0),
            'shared_amount' => $this->money(15, 3)->notNull()->defaultValue(0),
            'shared_fee' => $this->money(15, 3)->notNull()->defaultValue(0),
            'shared_tax' => $this->money(15, 3)->notNull()->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('prop_id', 'prop_profit_his', 'prop_id');
        
        if ($propInfoSchema = $this->db->schema->getTableSchema('prop_info', true)) {
            if (is_null($propInfoSchema->getColumn('profit_rate'))) {
                $this->addColumn('prop_info', 'profit_rate', 'DECIMAL(3,1) NOT NULL DEFAULT 0 AFTER earning_at');
            }
            if (is_null($propInfoSchema->getColumn('profit_id'))) {
                $this->addColumn('prop_info', 'profit_id', 'int NOT NULL DEFAULT 0 AFTER earning_at');
            }
            if (is_null($propInfoSchema->getColumn('profit_type'))) {
                $this->addColumn('prop_info', 'profit_type', 'tinyint NOT NULL DEFAULT 0 AFTER earning_at');
            }
        }
        
    }

    public function safeDown()
    {
        if ($this->db->schema->getTableSchema('prop_profit_his', true) !== null) {
            $this->dropTable('prop_profit_his');
        }
        
        if ($propInfoSchema = $this->db->schema->getTableSchema('prop_info', true)) {
            if (is_null($propInfoSchema->getColumn('profit_rate'))) {
                $this->dropColumn('prop_info', 'profit_rate');
            }
            if (!is_null($propInfoSchema->getColumn('profit_id'))) {
                $this->dropColumn('prop_info', 'profit_id');
            }
            if (!is_null($propInfoSchema->getColumn('profit_type'))) {
                $this->dropColumn('prop_info', 'profit_type');
            }
        }
    }
    
}
