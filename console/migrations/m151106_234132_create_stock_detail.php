<?php

use yii\db\Migration;

class m151106_234132_create_stock_detail extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('prop_stock_detail', true) !== null) {
            $this->dropTable('prop_stock_detail');
        }
        
        $this->createTable('prop_stock_detail', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer()->notNull(),
            'stock_cnt' => $this->integer()->notNull(),
            'buy_price' => $this->money(15, 3)->notNull(),
            'onsale_cnt' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_stock_detail', true) !== null) {
            $this->dropTable('prop_stock_detail');
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
