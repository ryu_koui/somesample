<?php

use yii\db\Migration;

class m151107_045502_create_prop_share_his extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('prop_share_his', true) !== null) {
            $this->dropTable('prop_share_his');
        }
        
        $this->createTable('prop_share_his', [
            'id' => $this->primaryKey(),
            'prop_id' => $this->integer()->notNull(),
            'amount' => $this->money(15, 3)->notNull(),
            'profit_cnt' => $this->integer()->notNull(),
            'user_cnt' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('prop_id', 'prop_share_his', 'prop_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_share_his', true) !== null) {
            $this->dropTable('prop_share_his');
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
