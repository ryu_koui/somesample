<?php

use yii\db\Migration;

class m151107_045538_create_user_earnings_his extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('user_earnings_his', true) !== null) {
            $this->dropTable('user_earnings_his');
        }
        
        $this->createTable('user_earnings_his', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'prop_id' => $this->integer()->notNull(),
            'amount' => $this->money(15, 3)->notNull()->defaultValue(0),
            'fee' => $this->money(15, 3)->notNull()->defaultValue(0),
            'tax' => $this->money(15, 3)->notNull()->defaultValue(0),
            'stock_cnt' => $this->integer()->notNull()->defaultValue(0),
            'buy_price' => $this->money(15, 3)->notNull()->defaultValue(0),
            'profit_his_id' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('user_id', 'user_earnings_his', 'user_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('user_earnings_his', true) !== null) {
            $this->dropTable('user_earnings_his');
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
