<?php

use yii\db\Migration;

class m151216_093041_create_user_point_his extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('user_point_his', true) !== null) {
            $this->dropTable('user_point_his');
        }
        
        $this->createTable('user_point_his', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'action' => $this->smallInteger()->notNull(),
            'change' => $this->money(15, 3)->notNull(),
            'points' => $this->money(15, 3)->notNull(),
            'total_earnings' => $this->money(15, 3)->notNull(),
            'total_expenses' => $this->money(15, 3)->notNull(),
            'account_type' => $this->string(32),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('user_id', 'user_point_his', 'user_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('user_point_his', true) !== null) {
            $this->dropTable('user_point_his');
        }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
