<?php

use yii\db\Migration;

class m151217_070512_create_account_paypal extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('account_paypal', true) !== null) {
            $this->dropTable('account_paypal');
        }
        
        $this->createTable('account_paypal', [
            'user_id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'phone' => $this->string(255)->notNull()->defaultValue(''),
            'payer_id' => $this->string(255)->notNull()->defaultValue(''),
            'payout_default' => $this->smallInteger()->notNull()->defaultValue(0),
            'payment_default' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('account_paypal', true) !== null) {
            $this->dropTable('account_paypal');
        }
    }
}
