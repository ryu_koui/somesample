<?php

use yii\db\Migration;

class m151217_200947_create_campaign_info extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('campaign_info', true) !== null) {
            $this->dropTable('campaign_info');
        }
        
        $this->createTable('campaign_info', [
            'id' => $this->primaryKey(),
            'class' => $this->string(32)->notNull(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'fee_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('campaign_info', true) !== null) {
            $this->dropTable('campaign_info');
        }
    }
}
