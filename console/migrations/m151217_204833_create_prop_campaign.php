<?php

use yii\db\Migration;

class m151217_204833_create_prop_campaign extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('prop_campaign', true) !== null) {
            $this->dropTable('prop_campaign');
        }
        
        $this->createTable('prop_campaign', [
            'id' => $this->primaryKey(),
            'prop_id' => $this->integer()->notNull(),
            'camp_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('prop_id', 'prop_campaign', 'prop_id');
        $this->createIndex('camp_id', 'prop_campaign', 'camp_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_campaign', true) !== null) {
            $this->dropTable('prop_campaign');
        }
    }
}
