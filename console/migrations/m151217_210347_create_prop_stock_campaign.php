<?php

use yii\db\Migration;

class m151217_210347_create_prop_stock_campaign extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('prop_stock_campaign', true) !== null) {
            $this->dropTable('prop_stock_campaign');
        }
        
        $this->createTable('prop_stock_campaign', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer()->notNull(),
            'detail_id' => $this->integer()->notNull(),
            'camp_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
        $this->createIndex('stock_id', 'prop_stock_campaign', 'stock_id');
        $this->createIndex('detail_id', 'prop_stock_campaign', 'detail_id');
        $this->createIndex('camp_id', 'prop_stock_campaign', 'camp_id');
    }

    public function down()
    {
        if ($this->db->schema->getTableSchema('prop_stock_campaign', true) !== null) {
            $this->dropTable('prop_stock_campaign');
        }
    }
}
