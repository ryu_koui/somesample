<?php

use yii\db\Migration;

class m151218_000401_create_fee_info extends Migration
{
    public function up()
    {
        if ($this->db->schema->getTableSchema('fee_info', true) !== null) {
            $this->dropTable('fee_info');
        }
        
        $this->createTable('fee_info', [
            'id' => $this->primaryKey(),
            'stock_deposit_per' => $this->decimal(4, 2),
            'buy_stock_per' => $this->decimal(4, 2),
            'sell_stock_per' => $this->decimal(4, 2),
            'user_earnings_per' => $this->decimal(4, 2),
            'point_exchange_per' => $this->decimal(4, 2),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('fee_info');
    }
}
