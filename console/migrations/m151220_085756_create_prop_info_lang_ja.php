<?php

use yii\db\Migration;

class m151220_085756_create_prop_info_lang_ja extends Migration
{
    public function up()
    {
        $this->createTable('prop_info_lang_ja-JP', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'summary' => $this->string(255),
            'description' => $this->text(),
        ]);
        
        $this->createTable('prop_info_lang_zh-CN', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'summary' => $this->string(255),
            'description' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('prop_info_lang_ja-JP');
        $this->dropTable('prop_info_lang_zh-CN');
    }
}
