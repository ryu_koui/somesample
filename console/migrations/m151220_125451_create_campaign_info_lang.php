<?php

use yii\db\Migration;

class m151220_125451_create_campaign_info_lang extends Migration
{
    public function up()
    {
        $this->createTable('campaign_info_lang_ja-JP', [
            'id' => $this->primaryKey(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text(),
        ]);
        
        $this->createTable('campaign_info_lang_zh-CN', [
            'id' => $this->primaryKey(),
            'label' => $this->string(255)->notNull(),
            'description' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('campaign_info_lang_ja-JP');
        $this->dropTable('campaign_info_lang_zh-CN');
    }
}
