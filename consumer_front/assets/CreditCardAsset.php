<?php

namespace consumer_front\assets;

use Yii;
use yii\web\AssetBundle;

class CreditCardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
         'css/creditcard.css',
    ];

    public $js = [
        'js/creditcard.js',
    ];

    public $depends = [
       'yii\web\YiiAsset',
       'yii\bootstrap\BootstrapAsset',
    ];
}