<?php
namespace consumer_front\assets;

use yii\web\AssetBundle;

class PaymentAsset extends AssetBundle 
{
    public $sourcePath = '@bower/jquery.payment/lib'; 
    public $js = [ 
        'jquery.payment.js',
    ];
    public $depends = [
       'yii\web\YiiAsset',
       'yii\bootstrap\BootstrapAsset',
    ];
}