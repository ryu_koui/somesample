<?php
namespace consumer_front\assets;

use yii\web\AssetBundle;

class PropStockSelectorAsset extends AssetBundle 
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [ 
        'js/prop_stock_selector.js',
    ];
    public $depends = [
       'consumer_front\assets\AppAsset',
    ];
}