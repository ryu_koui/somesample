<?php
namespace consumer_front\assets;

use yii\web\AssetBundle;

class SellStockSelectorAsset extends AssetBundle 
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [ 
        'js/sell_stock_selector.js',
    ];
    public $depends = [
       'consumer_front\assets\AppAsset',
    ];
}