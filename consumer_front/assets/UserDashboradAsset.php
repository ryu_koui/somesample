<?php
namespace consumer_front\assets;

use yii\web\View;
use yii\web\AssetBundle;

class UserDashboradAsset extends AssetBundle 
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [ 
        'js/user_dashborad.js',
    ];
    public $jsOption = [
        'position' => View::POS_BEGIN,
    ];
    public $depends = [
       'consumer_front\assets\AppAsset',
    ];
}