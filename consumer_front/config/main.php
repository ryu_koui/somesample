<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-consumer_front',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'consumer_front\controllers',
    // 'language' => 'en-US',
    // 'language' => 'ja-JP',
    'language' => 'zh-CN',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\user\Info',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '/var/log/hl/consumer_front_log',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'session' => [
            'class' => 'yii\redis\Session',
            // 'redis' => 'redis' // id of the connection application component
        ],
        'urlManager' => [
            'rules' => [
                'prop/<prop_id:\d+>' => 'prop/index',
                '<controller:(propdeal|propsell)>/<prop_id:\d+>/<action:(stocks|accselect|acctype|accinfo|confirm|result)>' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
