<?php
namespace consumer_front\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\account;

class AccountController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
        ];
    }
    
    public function actionSelect()
    {
        return $this->render('select', [
            'list' => account\Manager::getAllList(Yii::$app->user->id),
        ]);
    }
    
    public function actionAdd()
    {
        return $this->render('add', [
            'list' => account\Manager::getInfoList(),
        ]);
    }
    
    public function actionCreate()
    {
        $type = Yii::$app->request->get('at');
        return $this->runAction('create-' . $type);
    }
    
    public function actionCreateCreditcard()
    {
        // $exist_cnt = account\Creditcard::getCount(Yii::$app->user->id);
        
        $cc_service = account\CreditcardService::getInst(Yii::$app->user->id);
        $result = $cc_service->addOne(Yii::$app->request->post());
        
        if ($result && isset($cc_service->ar_datas[0])) {
            if (Yii::$app->request->get('ref') === 'deal') {
                $this->redirect(array_merge(
                    ['propdeal/confirm'],
                    Yii::$app->request->get(),
                    ['act' => account\CreditcardService::type(),'acid' => $cc_service->ar_datas[0]->id]
                ));
            } else {
                $this->redirect('account/add');
            }
        }
        
        return $this->render('creditcard/create', [
            'model' => $cc_service->ar_datas[0],
            // 'force_default' => ($exist_cnt > 0 ? true : false),
        ]);
    }
}