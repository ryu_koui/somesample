<?php
namespace consumer_front\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\filters\LanguageFilter;
use common\models\property\Info;
use common\models\property\Stocks;

class PropController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'language' => [
                'class' => LanguageFilter::className(),
            ],
        ];
    }
    
    public function actionIndex($prop_id)
    {
        $prop = Info::findOneForPanel($prop_id, Info::SCENARIO_ALL);
        if ($prop === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $userStock = null;
        if (Yii::$app->user->id && $prop->earning_flg) {
            $userStock = Stocks::findOne(['prop_id'=>$prop_id, 'user_id'=>Yii::$app->user->id]);
        }
        
        $earnings = new ActiveDataProvider([
            'query' => $prop->getNewProfitHis(),
            'pagination' => false,
        ]);
        
        return $this->render('index', [
            'prop' => $prop,
            'userStock' => $userStock,
            'earnings' => $earnings,
        ]);
    }
    
    protected function findModel($id)
    {
        $model = Info::getDb()->cache(function($db) use(&$id){
            return Info::findOne($id);
        });
        
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
