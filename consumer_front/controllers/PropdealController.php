<?php
namespace consumer_front\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\base\InvalidValueException;
use common\components\filters\LanguageFilter;
use common\models\property;
use common\models\account\PaypalService;

class PropdealController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
            'language' => [
                'class' => LanguageFilter::className(),
            ],
        ];
    }
    
    public function actionStocks($prop_id)
    {
        $model = property\Deal::getInst($prop_id, property\Deal::SCENARIO_RESERVE);
        $detail = $model->getStockDetail(Yii::$app->user->id);
        
        if ($model->load(Yii::$app->request->post()) && $model->reserveStock(Yii::$app->user->id)) {
            $flow_token = $model->saveFlowData(Yii::$app->user->id);
            Yii::$app->session->set('buy_stock_flow_token', $flow_token);
            
            // if (Yii::$app->user->identity->point->payment_account) {
                // $action = 'select';
            // } else {
                // $action = 'add';
            // }
            // return $this->redirect(Url::to(['account/'.$action, 'ref' => 'deal']));
            return $this->redirect(Url::to(['confirm', 'act' => 'paypal', 'acid' => Yii::$app->user->id]));
        } else {
            return $this->render('stocks', [
                'model' => $detail,
            ]);
        }
    }
    
    public function actionConfirm()
    {
        $params = [
            'token' => Yii::$app->session->get('buy_stock_flow_token'),
            'account_type' => Yii::$app->request->get('act'),
            'account_id' => Yii::$app->request->get('acid'),
        ];
        $model = property\Deal::virefyFlowData(Yii::$app->user->id, $params);
        
        return $this->render('confirm', [
            'model' => $model,
            'action' => 'confirm',
        ]);
    }
    
    public function actionPayment()
    {
        $post = Yii::$app->request->post();
        $post['Deal']['token'] = Yii::$app->session->get('buy_stock_flow_token');
        $model = property\Deal::virefyFlowData(Yii::$app->user->id, $post['Deal']);
        
        $data = $model->flow_data;
        $data['title'] = $model->prop_info->title . 'の株';
        $data['desc'] = $model->prop_info->title . 'の株の購入';
        $data['return_url'] = Url::toRoute(['buy', 't' => $data['token']], true);
        $data['cancel_url'] = Url::toRoute(['payment', 'failed' => '1'], true);
        
        if (!Yii::$app->request->get('failed')) {
            $return_url = PaypalService::createPayment($data);
            if ($return_url) {
                return $this->redirect($return_url);
            }
        }
        
        return $this->render('confirm', [
            'model' => $model,
            'action' => 'payment',
            'payment_failed' => 'paypal',
        ]);
    }
    
    public function actionBuy()
    {
        $token = Yii::$app->request->get('t');
        if (!$token || $token !== Yii::$app->session->get('buy_stock_flow_token')) {
            throw new InvalidValueException('Invalid flow token data.');
        }
        
        $payment_data = [
            'user_id' => Yii::$app->user->id,
            'paymentId' => Yii::$app->request->get('paymentId'),
            'PayerID' => Yii::$app->request->get('PayerID'),
        ];
        $model = property\Deal::virefyFlowData(Yii::$app->user->id, ['token' => $token]);
        $stocks_model = $model->buy(Yii::$app->user->id, $payment_data);
        return $this->redirect(Url::to(['result']));
    }
    
    public function actionResult()
    {
        $token = Yii::$app->session->get('buy_stock_flow_token');
        $flow_data = Yii::$app->cache->get($token);
        if (!$flow_data || $flow_data['user_id'] != Yii::$app->user->id || !isset($flow_data['complete'])) {
            throw new InvalidValueException('Invalid flow token data.');
        }
        
        $prop_info = property\Info::findOne($flow_data['prop_id']);
        
        // property\Deal::deleteFlowData($token);
        
        return $this->render('result', [
            'flow_data' => $flow_data,
            'prop_info' => $prop_info,
        ]);
    }
    
}
