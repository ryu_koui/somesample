<?php
namespace consumer_front\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use common\components\filters\LanguageFilter;
use common\models\property;

class PropsellController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
            'language' => [
                'class' => LanguageFilter::className(),
            ],
        ];
    }
    
    public function actionStocks($prop_id)
    {
        $model = property\Sell::getInst($prop_id, Yii::$app->user->id);
        
        if ($model->load(Yii::$app->request->post()) && $model->sellStock()) {
            $this->redirect(Url::to(["propsell/$prop_id/result"]));
        } else {
            $propStocks = property\Stocks::getOnsaleList($prop_id);
            
            return $this->render('stocks', [
                'model' => $model,
                'stock' => $model->userStock,
                'propStocks' => $propStocks,
            ]);
        }
    }
    
    public function actionResult()
    {
        return $this->render('result', []);
    }
}