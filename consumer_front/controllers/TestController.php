<?php
namespace consumer_front\controllers;

use Yii;
use yii\helpers\Url;
use PayPal\Api;
use PayPal\Auth;
use PayPal\Rest;
use common\models\property;
use common\models\fee;
use common\models\campaign;

class TestController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $result = '';
        
        $result = campaign\Info::find()->all();
        foreach ($result as $test) {
            Yii::trace($test->toArray());
        }
        
        // fee\Current::loadDefault();
        // Yii::trace(fee\Current::getList());
//         
        // $campaigns = campaign\Info::find()->all();
        // campaign\Service::apply(campaign\Service::ACT_BUY_STOCK, $campaigns);
        // Yii::trace(fee\Current::getList());
        
        return $this->render('index', [
            'title' => 'Test Page',
            // 'result' => var_export($result, true),
        ]);
    }
    
    public function actionPayment()
    {
        $apiContext = self::getApiContext();
        
        $payer = new Api\Payer();
        $payer->setPaymentMethod("paypal");

        $amount = new Api\Amount();
        $amount->setCurrency("JPY")
               ->setTotal(50000);
        
        $transaction = new Api\Transaction();
        $transaction->setAmount($amount)
                    ->setDescription("Payment description")
                    ->setInvoiceNumber(uniqid());
        
        $redirectUrls = new Api\RedirectUrls();
        $redirectUrls->setReturnUrl(Url::toRoute('callback', true))
                     ->setCancelUrl(Url::toRoute('/test', true));
        
        $payment = new Api\Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));
        
        $error = null;
        try {
            $payment->create($apiContext);
        } catch (Exception $e) {
            $error = $e->__toString();
        }
        
        if ($payment->getState() == 'created' && $payment->getApprovalLink()) {
            return $this->redirect($payment->getApprovalLink());
        }
        
        return $this->render('index', [
            'title' => 'Test Payment',
            'error' => $error,
            'result' => $payment->toJSON(128),
        ]);
    }

    public function actionCallback()
    {
        $apiContext = self::getApiContext();
        
        $paymentId = Yii::$app->request->get('paymentId');
        $payment = Api\Payment::get($paymentId, $apiContext);

        $execution = new Api\PaymentExecution();
        $execution->setPayerId(Yii::$app->request->get('PayerID'));
        
        $error = null;
        try {
            $result = $payment->execute($execution, $apiContext);
            try {
                $payment = Api\Payment::get($paymentId, $apiContext);
            } catch (Exception $e) {
                $error = $e->__toString();
            }
        } catch (Exception $e) {
            $error = $e->__toString();
        }
        
        return $this->render('index', [
            'title' => 'Test Payment',
            'error' => $error,
            'result' => $payment->toJSON(128),
        ]);
    }
    
    public function actionConsent()
    {
        $apiContext = self::getApiContext();
        
        $redirectUrl = Api\OpenIdSession::getAuthorizationUrl(
            Url::toRoute('consent-callback', true),
            ['email'
                // 'openid', 'email', 'phone',
                // 'https://uri.paypal.com/services/expresscheckout'
            ],
            null,
            null,
            null,
            $apiContext
        );
        
        return $this->redirect($redirectUrl);
    }
    
    public function actionConsentCallback()
    {
        $apiContext = self::getApiContext();
        $code = Yii::$app->request->get('code');
        
        $error = null;
        try {
            $accessToken = Api\OpenIdTokeninfo::createFromAuthorizationCode(array('code' => $code), null, null, $apiContext);
        } catch (Exception $e) {
            $error = $e->__toString();
        }
        
        return $this->render('index', [
            'title' => 'Test User Consent',
            'error' => $error,
            'result' => "Code : $code \n\n" . $accessToken->toJSON(128),
        ]);
    }
    
    public function actionPayout()
    {
        $apiContext = self::getApiContext();
        
        $senderBatchHeader = new Api\PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
                          ->setEmailSubject("You have a Payout!");
        
        $senderItem = new Api\PayoutItem();
        $senderItem->setRecipientType('EMAIL')
                   ->setNote('Thanks for your patronage!')
                   ->setReceiver('ryukoui-buyer@gmail.com')
                   ->setSenderItemId("2014031400023")
                   ->setAmount(new Api\Currency('{
                                        "value":"90000",
                                        "currency":"JPY"
                                    }'));
        
        $payouts = new Api\Payout();                            
        $payouts->setSenderBatchHeader($senderBatchHeader)
                ->addItem($senderItem);
        
        $error = null;
        try {
            $output = $payouts->createSynchronous($apiContext);
        } catch (Exception $e) {
            $error = $e->__toString();
        }
        
        return $this->render('index', [
            'title' => 'Test Payment',
            'error' => $error,
            'result' => $output->toJSON(128),
        ]);
    }
    
    private static function getApiContext()
    {
        $apiContext = new Rest\ApiContext(
            new Auth\OAuthTokenCredential(
                Yii::$app->params['paypal']['clientId'],
                Yii::$app->params['paypal']['secret']
            )
        );
        
        if (isset(Yii::$app->params['paypal']['config']['cache.FileName'])) {
            Yii::$app->params['paypal']['config']['cache.FileName'] = Yii::getAlias(Yii::$app->params['paypal']['config']['cache.FileName']);
        }
        
        $apiContext->setConfig(Yii::$app->params['paypal']['config']);
        
        return $apiContext;
    }
}
