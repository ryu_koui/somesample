<?php
namespace consumer_front\controllers;

use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\base\InvalidValueException;
use common\components\filters\LanguageFilter;
use common\models\account;
use common\models\user;
use common\models\property;
use common\models\fee;

class UserController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
            'language' => [
                'class' => LanguageFilter::className(),
            ],
        ];
    }
    
    public function actionDashboard()
    {
        $user_id = Yii::$app->user->id;
        $point = user\Point::findOne($user_id);
        $stockProps = property\Stocks::getUserPropList($user_id);
        $recommendProp = property\Info::findOneForPanel(7);
        $earnings = new ActiveDataProvider([
            'query' => user\EarningsHistory::getListByUser($user_id, null, ['fromDate' => 'first day of today 6 months ago']),
            'pagination' => false,
        ]);
        
        return $this->render('index', [
            'point' => $point,
            'stockProps' => $stockProps,
            'earnings' => $earnings,
            'recommendProp' => $recommendProp,
        ]);
    }
    
    public function actionPoint()
    {
        $user_id = Yii::$app->user->id;
        $point = user\Point::findOne($user_id);
        $recommendProp = property\Info::findOneForPanel(7);
        
        $point_his = new ActiveDataProvider([
            'query' => user\PointHistory::find()->where(['user_id' => Yii::$app->user->id])->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        return $this->render('point', [
            'point' => $point,
            'point_his' => $point_his,
            'recommendProp' => $recommendProp,
        ]);
    }
    
    public function actionPointExchange()
    {
        $user_id = Yii::$app->user->id;
        $point = user\Point::findOne($user_id);
        
        if ($point->points <= 0) {
            throw new InvalidValueException('Not enough points.');
        }
        
        $paypal = account\Paypal::findOne($user_id);
        if (!$paypal) {
            throw new InvalidValueException('No paypal data.');
        }
        
        $model = new DynamicModel(['amount' => null]);
        $model->addRule(['amount'], 'number', ['max' => floor($point->points)])
              ->addRule(['amount'], 'required');
        
        return $this->render('point-exchange', [
            'point' => $point,
            'model' => $model,
            'paypal' => $paypal,
        ]);
    }
    
    public function actionPointExchangeConfirm()
    {
        $user_id = Yii::$app->user->id;
        $point = user\Point::findOne($user_id);
        
        $paypal = account\Paypal::findOne($user_id);
        if (!$paypal) {
            throw new InvalidValueException('No paypal data.');
        }
        
        $model = new DynamicModel(['amount' => null, 'fee' => null]);
        $model->addRule(['amount'], 'number', ['max' => floor($point->points)])
              ->addRule(['amount'], 'required');
        
        if (!$model->load(Yii::$app->request->post()) || $point->points < $model->amount) {
            throw new InvalidValueException('Invalid flow token data.');
        }
        
        if (Yii::$app->request->post('exchange')) {
            try {
                account\PaypalService::payout([
                    'amount' => $model->amount,
                    'email' => $paypal->email,
                    'email_subject' => Yii::t('app', 'ポイント換金処理のお知らせ'),
                ]);
                user\Point::outgo($user_id, $model->amount, user\PointHistory::ACT_EXCHANGE_POINTS);
            } catch (Exception $e) {
                throw new InvalidValueException('Paypal payout request faild.');
            }
            
            $this->redirect('point-exchange-result');
        }
        
        return $this->render('point-exchange-confirm', [
            'point' => $point,
            'model' => $model,
            'paypal' => $paypal,
        ]);
    }
    
    public function actionPointExchangeResult()
    {
        return $this->render('point-exchange-result');
    }
    
    public function actionAccount()
    {
        return $this->render('account');
    }
    
    public function actionPropprofit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $prop_id = Yii::$app->request->post('prop_id');
        if (!$prop_id) {
            return ['error' => 'no prop_id'];
        }
        
        $profits = new ActiveDataProvider([
            'query' => user\EarningsHistory::getListByUser(Yii::$app->user->id, $prop_id),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $detail_html = $this->renderPartial('_prop_profit_detail', ['profits' => $profits]);
        return ['html' => $detail_html];
    }
}