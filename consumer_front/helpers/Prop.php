<?php
namespace consumer_front\helpers;

use Yii;
use yii\helpers\Html;

class Prop
{
    public static function unitPrice($prop_info)
    {
        $unit_price = $prop_info->price / $prop_info->stock_num;
        return Yii::$app->formatter->asCurrency($unit_price);
    }
    
    public static function balance($prop_info)
    {
        $unit_price = $prop_info->price / $prop_info->stock_num;
        $balance = ceil($unit_price * ($prop_info->stock_num - $prop_info->stockState->fixed_stocks));
        return Yii::$app->formatter->asCurrency($balance);
    }
    
    public static function compratio($prop_info, $format = true)
    {
        $ratio = $prop_info->stockState->fixed_stocks / $prop_info->stock_num;
        
        return $format ? Yii::$app->formatter->format($ratio, ['percent', 1]) : $ratio * 100;
    }
    
    public static function balanceStocks($prop_info)
    {
        return Yii::$app->formatter->asInteger($prop_info->stock_num - $prop_info->stockState->fixed_stocks);
    }
    
    public static function fullAddress($prop_info)
    {
        // TODO: add region
        return Html::encode($prop_info->city . ' ' . $prop_info->address);
    }
}
