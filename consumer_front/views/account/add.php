<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\account;
?>
<div class="info-view">
    
    <div class="body-content container">
        <h1>Add Account</h1>
        
        <div class="row account-select">
<?php foreach($list as $id => $className):?>
            <a href="<?= Url::to(array_merge(['account/create'], Yii::$app->request->get(), ['at' => $id]))?>">
                <div class="col-md-6 account-tile">
                    <div class="account-tile-inner">
                        <h5 class="account-tile-title"><?= $className::$label?></h5>
                    </div>
                </div>
            </a>
<?php endforeach?>
        </div>
    </div>
    
</div>