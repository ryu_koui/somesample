<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

    <h3><?= $group['service_class']::$label?></h3>
    <div class="row account-list">
<?php foreach($group['data'] as $data):?>
        <div class="col-md-10 col-md-offset-1 account-list">
            <div class="account-list-inner">
                <div class="row">
                    <div class="col-sm-2 attr_label"><?= $data->getAttributeLabel('type')?>：</div>
                    <div class="col-sm-2"><i class="fa fa-cc-<?= $data->type?>"></i> <?= $data->type?></div>
                    <div class="col-sm-2 attr_label"><?= $data->getAttributeLabel('name')?>：</div>
                    <div class="col-sm-2"><?= $data->name?></div>
                </div>
                <div class="row">
                    <div class="col-sm-2 attr_label"><?= $data->getAttributeLabel('number')?>：</div>
                    <div class="col-sm-2"><?= $data->getHiddenNumber()?></div>
                    <div class="col-sm-2 attr_label"><?= $data->getAttributeLabel('expire_date')?>：</div>
                    <div class="col-sm-2"><?= $data->getExpiry()?></div>
                    <?php $url = Url::to(array_merge(['propdeal/confirm'], Yii::$app->request->get(), ['act' => $id, 'acid' => $data->id]))?>
                    <div class="col-sm-3 col-sm-offset-1">
                        <?= Html::a(Yii::t('app', 'このアカウントを選択'), [$url], ['class' => 'btn btn-success btn-xs']) ?>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach?>
    </div>