<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use consumer_front\widgets\CreditCard;

?>
<div class="info-create">
    <div class="body-content container">
        <h1>Add Credit Card</h1>
        <?php $creditCard = new CreditCard(['model' => $model]) ?>
        <?php $form = ActiveForm::begin(['id' => 'creditcard_form']) ?>
        <div class="container">
            <?php $creditCard->setForm($form) ?>
            <div class="row"><div class="col-xs-12"><?= $creditCard->number() ?></div></div>
            <div class="row"><div class="col-xs-12"><?= $creditCard->expiry() ?></div></div>
            <div class="row"><div class="col-xs-12"><?= $creditCard->name() ?></div></div>
            <div class="row"><div class="col-xs-12"><?= $creditCard->cvc() ?></div></div>
            <div class="form-group">
                <?= $creditCard->hidden('type') ?>
                <?= $creditCard->hidden('expire_month') ?>
                <?= $creditCard->hidden('expire_year') ?>
                <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

</div>
