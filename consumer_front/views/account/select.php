<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="info-view">
    
    <div class="body-content container">
        <h1>Select Account</h1>
        
<?php foreach($list as $id => $group):?>
        <div class="container">
            <?= $this->render($id.'/_list_element', [
                'id' => $id,
                'group' => $group,
            ]) ?>
        </div>
<?php endforeach?>
    </div>
    
</div>