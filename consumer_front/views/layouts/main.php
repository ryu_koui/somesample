<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use consumer_front\assets\AppAsset;
use consumer_front\assets\FontAwesomeAsset;
use consumer_front\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
    <?php if (Yii::$app->language == 'zh-CN'):?>
        html, body {
            font-family: 'Microsoft YaHei', tahoma, arial, 'Hiragino Sans GB', 宋体;
        }
    <?php else:?>
    <?php endif?>
    </style> 
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Vadek',
                'brandUrl' => Yii::$app->homeUrl,
                'renderInnerContainer' => false,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            
            $menuItems = [];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => '<i class="fa fa-user"></i> ' . Yii::$app->user->identity->username,
                    'items' => [
                        ['label' => '<i class="fa fa-sign-out"></i> Logout', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
                    ],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'site-header'],
                'items' => $menuItems,
                'encodeLabels' => false,
            ]);
            
            // 言語用プールダウン
            $menuItems = [];
            $menuItems[] = [
                'label' => '<i class="fa fa-language"></i> ' . Yii::t('app', '言語'),
                'items' => [
                    ['label' => Yii::t('app', '日本語'), 'url' => ['/site/language?lang=ja-JP']],
                    ['label' => Yii::t('app', '中文'), 'url' => ['/site/language?lang=zh-CN']],
                ],
            ];
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'site-header'],
                'items' => $menuItems,
                'encodeLabels' => false,
            ]);
            
            if (!Yii::$app->user->isGuest) {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'site-header'],
                    'items' => [[
                        'label' => '<i class="fa fa-tachometer"></i> ' . Yii::t('app', 'Dashboard'),
                        'url' => ['/user/dashboard'],
                    ]],
                    'encodeLabels' => false,
                ]);
            }
            
            NavBar::end();
        ?>

        <div class="top-container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Vadek <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>
    <?php $this->registerJsFile('js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
