<?php
use yii\helpers\Html;
use yii\helpers\Url;
use consumer_front\helpers\Prop;
use common\models\property\Status;
?>

<?php foreach($props as $prop):?>
    <?php if($add_link):?><a href="/prop/<?= $prop->id?>"><?php endif?>
            <div class="<?php if (isset($widthClass)):?><?= $widthClass?><?php else:?>col-md-4<?php endif?> property-tile">
                <div class="property-tile-inner <?php if ($prop->status == Status::RAISING):?>raising<?php endif?>">
                    <div class="property-tile-thumbnail">
                        <div class="img-block">
                            <img src="/res/img/test/prop_<?= $prop->id?>.jpg" width="340px"/>
                        </div>
                        <div class="campaign-list">
                        <?php foreach($prop->campaigns as $campaign):?>
                            <div class="campaign-label campaign-label-<?= $campaign->class?>" data-toggle="popover" data-placement="top" data-trigger="hover" title="<?= \Yii::t('app', 'キャンペーン詳細');?>" data-content="<?= Html::encode($campaign->description)?>"><?= Html::encode($campaign->label)?></div>
                        <?php endforeach?>
                        </div>
                        <div class="earning-rate"><?= $prop->profit_rate?><span class="unit">%</span></div>
                    </div>
                    <div class="property-contents">
                        <h5 class="property-title"><?= Html::encode($prop->title)?></h5>
                        <p class="property-author"><?= Html::encode($prop->city)?></p>
                        <p class="property-description"><?= Html::encode($prop->summary)?></p>
                        <div class="property-footer">
                            <div class="property-footer-contents">
                            <?php if ($prop->status == Status::RAISING):?>
                                <p><?= Yii::$app->formatter->asCurrency($prop->price)?></p>
                                <div class="property-progress-bar">
                                    <div class="property-percent-pledged" style="width: <?= Prop::compratio($prop)?>"></div>
                                </div>
                            <?php endif?>
                                <ul>
                                    <li>
                                        <p class="property-footer-contents-subtile">
                                            <?php if ($prop->status == Status::RAISING):?>
                                                <?= \Yii::t('app', '総株数');?>
                                            <?php else:?>
                                                <?= \Yii::t('app', '販売中株数');?>
                                            <?php endif?>
                                        </p>
                                        <p class="property-footer-contents-params">
                                            <?php if ($prop->status == Status::RAISING):?>
                                                <?= Yii::$app->formatter->asInteger($prop->stock_num)?>
                                            <?php else:?>
                                                <?= Yii::$app->formatter->asInteger($prop->onsale_total)?>
                                            <?php endif?>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="property-footer-contents-subtile">
                                            <?php if ($prop->status == Status::RAISING):?>
                                                <?= \Yii::t('app', '単価');?>
                                            <?php else:?>
                                                <?= \Yii::t('app', '最低単価');?>
                                            <?php endif?>
                                        </p>
                                        <p class="property-footer-contents-params"><?= Yii::$app->formatter->asCurrency($prop->lowest_price)?></p>
                                    </li>
                                    <li>
                                        <p class="property-footer-contents-subtile"><?= \Yii::t('app', '参加人数');?></p>
                                        <p class="property-footer-contents-params"><?= Yii::$app->formatter->asInteger($prop->stockState->buyer_cnt)?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php if($add_link):?></a><?php endif?>
<?php endforeach?>