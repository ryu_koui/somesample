<?php
use yii\helpers\Html;
use consumer_front\helpers\Prop;
use common\models\property\Status;
use sjaakp\gcharts\ColumnChart;
use yii\grid\GridView;

$this->title = $prop->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="prop-detail">
    <h1 class="h2 title"><?= Html::encode($this->title) ?></h1>
    <div class="body-content container">
        <div class="prop-detail-info">
            <div class="row">
                <div class="col-md-8">
                    <div class="prop-detail-top-media">
                        <img src="/res/img/test/prop_<?= $prop->id?>.jpg" height="450px"/>
                        <div class="campaign-list">
                        <?php foreach($prop->campaigns as $campaign):?>
                            <div class="campaign-label campaign-label-<?= $campaign->class?>" data-toggle="popover" data-placement="top" data-trigger="hover" title="<?= \Yii::t('app', 'キャンペーン詳細');?>" data-content="<?= Html::encode($campaign->description)?>"><?= Html::encode($campaign->label)?></div>
                        <?php endforeach?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="prop-detail-state">
                        <div class="state-item bold">
                            <div class="h1 per"><?= $prop->profit_rate?><span class="unit">%</span></div>
                            <span class="h6"><?= \Yii::t('app', '利益率');?></span>
                        </div>
                        <!-- <div class="state-item bold">
                            <div class="h1"><?= Yii::$app->formatter->asInteger($prop->stockState['buyer_cnt'])?></div>
                            <span class="h6">参加者</span>
                        </div> -->
                        <div class="state-item bold">
                            <div class="h1"><?= Yii::$app->formatter->asCurrency($prop->lowest_price)?></div>
                    <?php if($prop->status == Status::RAISING):?>
                            <span class="h6"><?= \Yii::t('app', '単価');?></span>
                    <?php else:?>
                            <span class="h6"><?= \Yii::t('app', '最低単価');?></span>
                    <?php endif?>
                        </div>
                        <div class="state-item bold">
                            <div class="h1"><?= $prop->onsale_total?></div>
                    <?php if($prop->status == Status::RAISING):?>
                            <span class="h6"><?= \Yii::t('app', '残株数');?></span>
                    <?php else:?>
                            <span class="h6"><?= \Yii::t('app', '販売中株数');?></span>
                    <?php endif?>
                        </div>
                    <?php if($prop->status == Status::RAISING):?>
                        <div class="pickup-progress-bar">
                            <div class="pickup-percent-pledged" style="width: <?= Prop::compratio($prop)?>"></div>
                        </div>
                    <?php endif?>
                        <div class="engage">
                            <?= Html::a(Yii::t('app', 'この物件に参加'), ['/propdeal/' . $prop->id . '/stocks'], ['class' => 'btn btn-success btn-lg']) ?>
                        </div>
                        <!-- <div class="h6 text-muted">
                            この物件は 2022年2月22日 まで掲載されます
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row prop-detail-data">
            <div class="col-md-8">
                <div class="summary"><?= Html::encode($prop->summary)?></div>
                <div class="address"><i class="fa fa-map-marker"></i> &nbsp; <?= Prop::fullAddress($prop)?></div>
            </div>
            
            <div class="col-md-4">
            <?php if($userStock):?>
                <div class="user-prop-state">
                    <ul>
                        <li>
                            <p class="contents-subtile"><?= Yii::t('app', '所持株数')?></p>
                            <p class="contents-params"><?= $userStock->stock_cnt?></p>
                        </li>
                        <li>
                            <p class="contents-subtile"><?= Yii::t('app', '購入総額')?></p>
                            <p class="contents-params"><?= Yii::$app->formatter->asCurrency($userStock->getLastEarning()->buy_price)?></p>
                        </li>
                    </ul>
                </div>
            <?php else:?>
                <dl class="dl-horizontal row">
                  <dt><i class="fa fa-question-circle"></i> <?= Yii::t('app', '面積')?></dt>
                  <dd>102.45 m<sup>2</sup></dd>
                  <dt><i class="fa fa-question-circle"></i> <?= Yii::t('app', '間取り')?></dt>
                  <dd><?= Html::encode($prop->layout)?></dd>
                  <dt><i class="fa fa-question-circle"></i> <?= Yii::t('app', '分類')?></dt>
                  <dd>マンション</dd>
                </dl>
            <?php endif?>
            </div>
        </div>
        
        <div class="row prop-detail-body">
            
            <div class="col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                <?php if ($prop->earning_flg):?>
                    <li role="presentation" class="active"><a href="#profitinfo" role="tab" id="profitinfo-tab" data-toggle="tab" aria-controls="profitinfo" aria-expanded="true"><?= Yii::t('app', '収益情報')?></a></li>
                    <li role="presentation" class=""><a href="#description" id="home-tab" role="tab" data-toggle="tab" aria-controls="description" aria-expanded="false"><?= Yii::t('app', '物件詳細')?></a></li>
                <?php else:?>
                    <li role="presentation" class="active"><a href="#description" id="home-tab" role="tab" data-toggle="tab" aria-controls="description" aria-expanded="true"><?= Yii::t('app', '物件詳細')?></a></li>
                    <li role="presentation" class="disabled"><a><?= Yii::t('app', '収益情報')?></a></li>
                <?php endif?>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade <?php if ($prop->earning_flg):?>active in<?php endif?>" id="profitinfo" aria-labelledby="profitinfo-tab">
                    <?php if ($prop->earning_flg):?>
                        <?= ColumnChart::widget([
                            'height' => '200px',
                            'dataProvider' => $earnings,
                            'columns' => [
                                [
                                    'attribute' => 'created_at',
                                    'label' => Yii::t('app', '日付'),
                                    'type' => 'string',
                                    'value' => function($model, $a, $i, $w) {
                                        return date('m/d', strtotime($model->created_at));
                                    },
                                ],
                                [
                                    'attribute' => 'amount',
                                    'label' => Yii::t('app', '利益額'),
                                ],
                                [
                                    'attribute' => 'amount',
                                    'role' => 'annotation',
                                    'type' => 'string',
                                    'value' => function($model, $a, $i, $w) {
                                        return Yii::$app->formatter->asCurrency($model->amount);
                                    }
                                ],
                            ],
                            'options' => [
                                'title' => Yii::t('app', '過去半年の収益グラフ'),
                                'backgroundColor' => '#f5f5f5',
                            ],
                        ]) ?>
                        
                        <?= GridView::widget([
                            'dataProvider' => $earnings,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'created_at',
                                ['value' => function($data) {
                                    return Yii::$app->formatter->asCurrency($data->amount);
                                }],
                            ]
                        ]); ?>
                        
                    <?php endif?>
                    </div>
                    <div role="tabpanel" class="tab-pane fade <?php if (!$prop->earning_flg):?>active in<?php endif?>" id="description" aria-labelledby="description-tab">
                        <div class="description"><?= nl2br(Html::encode($prop->description))?></div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                
                <div class="onsale-stock-panel">
                    <h3 class="h4"><?= Yii::t('app', '販売中株')?></h3>
                <?php foreach($prop->stockState->onsale_list as $stock):?>
                    <div class="prop-detail-stocks">
                        <div class="header">
                            <div class="unit-price"><?= Yii::t('app', '単価')?> <?= Yii::$app->formatter->asCurrency($stock->unit_price)?></div>
                            <div class="balance-stocks"><?= Yii::$app->formatter->asInteger($stock->stock_cnt)?> (<?= Yii::t('app', '株')?>)</div>
                            <div style="clear: both;"></div>
                        </div>
                        <div class="sub">
                            <div class="owner">by <?= $stock->userinfo->username?></div>
                            <!-- <div class="expire">期限 2015/12/12</div> -->
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                <?php endforeach?>
                </div>
                
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
                <div class="buy-myself">
                    <div><?= Html::a(Yii::t('app', 'この物件を単独で購入'), ['#'], ['class' => 'btn btn-primary btn-lg']) ?></div>
                    <p><i class="fa fa-info-circle"></i> <?= Yii::t('app', '単独で購入する場合、契約名義もお客様となります')?></p>
                </div>
            </div>
        </div>
        
    </div>
    
</div>