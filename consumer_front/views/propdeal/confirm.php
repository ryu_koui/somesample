<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $model->prop_info->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-stocks">
    <h1 class="h2 title"><?= Html::encode($this->title) ?></h1>
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-4">
                <li><?= Yii::t('app', '購入株の選択')?></li>
                <li <?php if($action == 'confirm'):?>class="active"<?php endif?>><?= Yii::t('app', '購入情報確認')?></li>
                <li <?php if($action == 'payment'):?>class="active"<?php endif?>><?= Yii::t('app', '支払い認証')?></li>
                <li><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"></div>
        </div>
        
        
        <div class="row">
            <div class="col-md-8 prop-stock-confirm-body">
                
                <div class="buy-stock-confirm">
                <?php if (isset($payment_failed)):?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> <?= Yii::t('app', 'PayPalの支払い処理に失敗しました。お手数ですが再度試してください。')?>
                    </div>
                <?php endif?>
                    
                    <table class="table">
                        <caption><?= Yii::t('app', '購入詳細情報')?></caption>
                        <tbody>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '物件名')?></th>
                                <td><?= Html::encode($model->prop_info->title)?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '購入株数')?></th>
                                <td><?= $model->flow_data['required_num']?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '購入株内訳')?></th>
                                <td class="buy-stocks-detail">
                                    <table class="table table-condensed"><tbody>
                                        <tr>
                                            <th scope="row"><?= Yii::t('app', '株数')?></th>
                                            <th scope="row"><?= Yii::t('app', '株単価')?></th>
                                        </tr>
                                    <?php foreach($model->flow_data['required_stock_list'] as $stock_data):?>
                                        <tr>
                                            <td><?= $stock_data['num']?></td>
                                            <td><?= Yii::$app->formatter->asCurrency($stock_data['unit_price'])?></td>
                                        </tr>
                                    <?php endforeach?>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '購入株総額')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($model->flow_data['required_price'])?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '手数料')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($model->flow_data['fee'])?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '消費税')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($model->flow_data['tax'])?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '適用キャンペーン')?></th>
                                <td>
                            <?php foreach($model->prop_info->campaigns as $campaign):?>
                                    <span class="campaign-label campaign-label-<?= $campaign->class?>"><?= $campaign->label?></span>
                            <?php endforeach?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '支払総額')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($model->flow_data['charge'])?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '支払方法')?></th>
                                <td>
                                    <div class="pay-type-paypal success">
                                        <p class="header"><i class="fa fa-paypal"></i> <?= Yii::t('app', 'PayPal')?></p>
                                        <p><?= Yii::t('app', '決済サービスPayPalを利用して支払いを行います。')?></p>
                                        <p><?= Yii::t('app', 'これから購入した株で得た収入もPayPalアカウントを通して支払われますので、PayPalアカウントを所持していない場合はアカウントを作成して決済を行ってください。')?></p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="buy-stock-confirm-btn">
                        <?php $form = ActiveForm::begin(['action' => 'payment']); ?>
                            <?= Html::activeHiddenInput($model, 'prop_id')?>
                            <?php true//Html::activeHiddenInput($model, 'account_type', ['value' => $account_type])?>
                            <?php true//Html::activeHiddenInput($model, 'account_id', ['value' => $account_id])?>
                            <div class="form-group">
                                <?= Html::submitButton('<b><i class="fa fa-paypal"></i> '.Yii::t('app', '支払いに進む').'</b>', ['class' => 'btn btn-success btn-lg']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                        
                        <!-- <a href="<?= Url::toRoute(['buy', 't' => $model->flow_data['token']])?>"><?= Html::submitButton('<b>'.Yii::t('app', '支払いに進む (FOR TEST)').'</b>', ['class' => 'btn btn-success btn-lg']) ?></a> -->
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-md-4 prop-stock-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 予定金額完了まで契約は成立しません
                    </div>
                    <p>
                        予定金額100%到達まで売買契約が成立しません
                    </p>
                    <p>
                        ですが、予約金として手続きが完了次第、予約した金額の10%の支払いが発生します。予約キャンセル時は払い戻し致します。最終契約成立時は残りの90%の支払いが発生します。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>

</div>
