<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $prop_info->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-stocks">
    <h1 class="h2 title"><?= Html::encode($this->title) ?></h1>
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-4">
                <li><?= Yii::t('app', '購入株の選択')?></li>
                <li><?= Yii::t('app', '購入情報確認')?></li>
                <li><?= Yii::t('app', '支払い認証')?></li>
                <li class="active"><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"></div>
        </div>
        
        
        <div class="row">
            
            <div class="col-md-8 prop-stock-confirm-body">
                
                <div class="buy-stock-confirm">
                    <h2 class="buy-stock-result-title"><?= Yii::t('app', '購入手続完了致しました！')?></h2>
                    
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '物件名')?></th>
                                <td><?= Html::encode($prop_info->title)?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '購入株数')?></th>
                                <td><?= $flow_data['required_num']?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '支払総額')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($flow_data['charge'])?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', 'PayPal請求書ID')?></th>
                                <td><?= Html::encode('')?></td>
                            </tr>
                            <tr><td></td><td></td></tr>
                        </tbody>
                    </table>
                    
                    <div class="buy-stock-confirm-btn">
                        <?= Html::a(Yii::t('app', 'ダッシュボード'), '/user/dashboard', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 prop-stock-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 予定金額完了まで契約は成立しません
                    </div>
                    <p>
                        予定金額100%到達まで売買契約が成立しません
                    </p>
                    <p>
                        ですが、予約金として手続きが完了次第、予約した金額の10%の支払いが発生します。予約キャンセル時は払い戻し致します。最終契約成立時は残りの90%の支払いが発生します。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>

</div>
