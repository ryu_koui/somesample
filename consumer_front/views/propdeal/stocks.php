<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use consumer_front\assets\PropStockSelectorAsset;

PropStockSelectorAsset::register($this);

$this->title = $model->prop_info->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prop-stocks">
    <h1 class="h2 title"><?= Html::encode($this->title) ?></h1>
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-4">
                <li class="active"><?= Yii::t('app', '購入株の選択')?></li>
                <li><?= Yii::t('app', '購入情報確認')?></li>
                <li><?= Yii::t('app', '支払い認証')?></li>
                <li><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"><i class="fa fa-info-circle"></i> <?= Yii::t('app', '株数、あるいは予算で購入株を選択して次へお進みください。')?></div>
        </div>
        
        <div class="row">
            <div class="col-md-8 prop-stock-body">
                
                <div id="stock-selector" class="stock-selector">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="selector-tab active"><a href="#stock-selector-by-num" id="home-tab" role="tab" data-toggle="tab" aria-controls="stock-selector-by-num" aria-expanded="true"><?= Yii::t('app', '株数で選択')?></a></li>
                        <li role="presentation" class="selector-tab"><a href="#stock-selector-by-price" role="tab" id="profitinfo-tab" data-toggle="tab" aria-controls="stock-selector-by-price" aria-expanded="false"><?= Yii::t('app', '総額で選択')?></a></li>
                    </ul>
                    <div class="stock-selector-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="stock-selector-by-num" aria-labelledby="description-tab">
                                <div class="stock-selector-input">
                                    <label for="value-input-num"><?= Yii::t('app', '選択可能株数')?> <?= Yii::$app->formatter->asInteger($model->free_stocks)?></label>
                                    <div style="clear: both;"></div>
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <div class="input-group input-group-sm">
                                                <input type="text" id="value-input-num" class="form-control value-input" name="value-input-num" aria-describedby="value-input-num-addon" placeholder="<?= Yii::t('app', '選択したい株数を入力')?>"/>
                                                <span class="input-group-addon" id="value-input-num-addon"><?= Yii::t('app', '株')?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                                                <button type="button" id="select-by-num-btn" class="btn btn-default select-btn"><?= Yii::t('app', '選択して確認')?></button>
                                            </div>
                                        </li>
                                    </ul>
                                    <div style="clear: both;"></div>
                                    <div class="hint-block"><i class="fa fa-info-circle"></i> <?= Yii::t('app', '購入可能株から安い順に選択されます。選択後も手動変更できます。')?></div>
                                    <div class="help-block text-danger"></div>
                                </div> 
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="stock-selector-by-price" aria-labelledby="profitinfo-tab">
                                <div class="stock-selector-input">
                                    <label for="value-input-num"><?= Yii::t('app', '選択可能額')?> <?= Yii::$app->formatter->asCurrency($model->free_price)?></label>
                                    <div style="clear: both;"></div>
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <div class="input-group input-group-sm">
                                                <input type="text" id="value-input-price" class="form-control value-input" name="Deal[required_stocks]" aria-describedby="value-input-price-addon" placeholder="<?= Yii::t('app', '予算を入力')?>"/>
                                                <span class="input-group-addon" id="value-input-price-addon"><?= Yii::t('app', '円')?> <?= Yii::t('app', '以下')?></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                                                <button type="button" id="select-by-price-btn" class="btn btn-default select-btn"><?= Yii::t('app', '選択して確認')?></button>
                                            </div>
                                        </li>
                                    </ul>
                                    <div style="clear: both;"></div>
                                    <div class="hint-block"><i class="fa fa-info-circle"></i> <?= Yii::t('app', '予算に収まるよう購入可能株から安い順に選択されます。選択後も手動変更できます。')?></div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="selected-info">
                            <ul>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '選択中株数')?></p>
                                    <p class="contents-params" id="seleted_stock_num">0</p>
                                </li>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '選択中総額')?></p>
                                    <p class="contents-params" id="seleted_stock_price"><?= Yii::$app->formatter->asCurrency(0)?></p>
                                </li>
                            </ul>
                            <!-- <button type="submit" class="btn btn-lg btn-success pull-right" disabled="disabled">確定して次へ</button> -->
                            <?php $form = ActiveForm::begin(['id' => 'stock_select_form']); ?>
                                <?= Html::submitButton(Yii::t('app', '確定して次へ'), ['class' => 'btn btn-lg btn-success pull-right', 'disabled' => "disabled"]) ?>
                            <?php ActiveForm::end(); ?>
                            <div style="clear: both;"></div>
                        </div>
                        
                        
                    </div>
                </div>
                
                <div id="stock_select_list" class="stock-select-list">
                    <?php foreach($model->buyable_stock_list as $stock):?>
                        <div class="stocks-select-panel" id="stock-select-<?= $stock['stock_id']?>">
                            <div class="info-block">
                                <div class="info-main">
                                    <div class="balance-stocks"><?= Yii::$app->formatter->asInteger($stock['onsale_cnt'])?> <span class="badge"><?= Yii::t('app', '株')?></span></div>
                                    <div class="sub">
                                        <div class="owner">by <?= $stock['username']?></div>
                                        <!-- <div class="expire">期限 2015/12/12</div> -->
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="info-unit-price">
                                    <?= Yii::t('app', '単価')?> <span class="unit-price"><?= Yii::$app->formatter->asCurrency($stock['unit_price'])?></span>
                                    <div style="clear: both;"></div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="control-block">
                                <div class="help-block"></div>
                                <div class="control-panel">
                                    <input type="text" class="form-control value-input" name="value-input-num" placeholder="<?= Yii::t('app', '株数を入力')?>"/>
                                    <button class="btn btn-xs btn-primary"><?= Yii::t('app', '全選択')?></button>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    <?php endforeach?>
                </div>
                <script type="text/javascript">
                    var stock_list = <?= json_encode($model->buyable_stock_list)?>;
                    var max_stock_num = <?= $model->free_stocks?>;
                    var max_stock_price = <?= $model->free_price?>;
                    var form_name = 'Deal';
                </script>
                
            </div>
            
            
            <div class="col-md-4 prop-stock-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 予定金額完了まで契約は成立しません
                    </div>
                    <p>
                        予定金額100%到達まで売買契約が成立しません
                    </p>
                    <p>
                        ですが、予約金として手続きが完了次第、予約した金額の10%の支払いが発生します。予約キャンセル時は払い戻し致します。最終契約成立時は残りの90%の支払いが発生します。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
                <div class="buy-myself">
                    <div><?= Html::a(Yii::t('app', 'この物件を単独で購入'), ['#'], ['class' => 'btn btn-primary btn-lg']) ?></div>
                    <p><i class="fa fa-info-circle"></i> <?= Yii::t('app', '単独で購入する場合、契約名義もお客様となります')?></p>
                </div>
            </div>
        </div>
    </div>
    
</div>