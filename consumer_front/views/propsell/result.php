<?php
use yii\helpers\Html;
?>
<div class="info-create">
    <div class="body-content container">
        <h1>Result</h1>

        <div class="container">
            <?= Html::a(Yii::t('app', 'ホーム'), '/', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'ダッシュボード'), '/user/dashborad', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

</div>
