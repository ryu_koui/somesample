<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use consumer_front\assets\SellStockSelectorAsset;

SellStockSelectorAsset::register($this);

$this->title = $stock->propinfo->title;
?>
<div class="prop-stocks">
    <h1 class="h2 title"><?= Html::encode($this->title) ?></h1>
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-3">
                <li class="active"><?= Yii::t('app', '売却株の選択')?></li>
                <li><?= Yii::t('app', '売却情報確認')?></li>
                <li><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"><i class="fa fa-info-circle"></i> <?= Yii::t('app', '売却株数と単価を入力して次へお進みください。')?></div>
        </div>
        
        <div class="row">
            <div class="col-md-8 prop-stock-body">
                
                <div id="" class="stock-selector">
                    <div class="stock-selector-body">
                        <?php $form = ActiveForm::begin(['id' => 'stock_select_form']); ?>
                        <div id="stock-selector">
                            <div class="stock-selector-input">
                                <label for="value-input-num"><?= Yii::t('app', '売却可能株数')?> <?= Yii::$app->formatter->asInteger($stock->stock_cnt)?></label>
                                <div style="clear: both;"></div>
                                <ul class="nav navbar-nav">
                                    <li>
                                        <div class="input-group input-group-sm sell-num-input">
                                            <?= Html::activeInput('text', $model, 'num', ['id'=>"value-input-num", 'class'=>"form-control value-input", 'aria-describedby'=>"num-addon", 'placeholder'=>Yii::t('app', '売却株数を入力')])?>
                                            <span class="input-group-addon" id="num-addon"><?= Yii::t('app', '株')?></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="input-group input-group-sm sell-price-input">
                                            <span class="input-group-addon" id="unit_price-addon">￥</span>
                                            <?= Html::activeInput('text', $model, 'unit_price', ['id'=>"value-input-price", 'class'=>"form-control value-input", 'aria-describedby'=>"unit_price-addon", 'placeholder'=>Yii::t('app', '売却株の株単価を入力')])?>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                                            <button type="button" id="select-btn" class="btn btn-default select-btn"><?= Yii::t('app', '確認')?></button>
                                        </div>
                                    </li>
                                </ul>
                                <div style="clear: both;"></div>
                                <div class="hint-block"><i class="fa fa-info-circle"></i> <?= Yii::t('app', '現在売出し中の株がある場合はこちらの設定で上書きされます')?>。</div>
                                <div class="help-block text-danger"></div>
                            </div> 
                        </div>
                        
                        <div class="selected-info">
                            <ul>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '入力株数')?></p>
                                    <p class="contents-params" id="seleted_stock_num">0</p>
                                </li>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '売却総額')?></p>
                                    <p class="contents-params" id="seleted_stock_price"><?= Yii::$app->formatter->asCurrency(0)?></p>
                                </li>
                            </ul>
                            
                            <?= Html::submitButton(Yii::t('app', '確定して次へ'), ['id' => 'next-step-btn', 'class' => 'btn btn-lg btn-success pull-right', 'disabled' => "disabled"]) ?>                            
                            <div style="clear: both;"></div>
                        </div>
                        <?php ActiveForm::end(); ?>
                        
                    </div>
                </div>
                
                <div id="" class="stock-select-list onsale-stocks">
                <?php if ($stock->onsale_cnt > 0):?>
                    <h4 class="bold"><?= Yii::t('app', '売却設定中の自己所持株情報')?></h4>
                    <div class="stocks-select-panel">
                        <div class="info-block">
                            <div class="info-main">
                                <div class="balance-stocks">
                                    <span class="unit-price"><?= Yii::$app->formatter->asInteger($stock->onsale_cnt)?> <span class="badge"><?= Yii::t('app', '株')?></span></span>
                                </div>
                                
                            </div>
                            <div class="info-unit-price">
                                <?= Yii::t('app', '単価')?> <span class="unit-price"><?= Yii::$app->formatter->asCurrency($stock->unit_price)?></span>
                                <div style="clear: both;"></div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        
                        <div style="clear: both;"></div>
                    </div>
                <?php endif?>
                </div>
                
                <div class="stock-select-list onsale-stocks">
                    <h4 class="bold"><?= Yii::t('app', '売却設定中の他株情報')?></h4>
                <?php foreach($propStocks as $propStock):?>
                    <?php if($propStock->user_id == Yii::$app->user->id){continue;}?>
                    <div class="stocks-select-panel">
                        <div class="info-block">
                            <div class="info-main">
                                <div class="balance-stocks"><?= Yii::$app->formatter->asInteger($propStock->onsale_cnt)?> <span class="badge"><?= Yii::t('app', '株')?></span></div>
                                <div class="sub">
                                    <div class="owner">by <?php if($propStock->user_id == Yii::$app->params['systemUserId']):?>offical<?php else:?>other user<?php endif?></div>
                                    <!-- <div class="expire">期限 2015/12/12</div> -->
                                    <div style="clear: both;"></div>
                                </div>
                            </div>
                            <div class="info-unit-price">
                                <?= Yii::t('app', '単価')?> <span class="unit-price"><?= Yii::$app->formatter->asCurrency($propStock->unit_price)?></span>
                                <div style="clear: both;"></div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        
                        <div style="clear: both;"></div>
                    </div>
                <?php endforeach?>
                </div>
                
                <script type="text/javascript">
                    var max_num = <?= $stock->stock_cnt?>;
                </script>
                
            </div>
            
            <div class="col-md-4 prop-stock-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 株の売却は買い取りされるまで成立しません
                    </div>
                    <p>
                        売却手続きは該当の株の売却宣言のみで、<br/>
                        実際にすぐには売却されません。
                    </p>
                    <p>
                        買い取り手が出て始めて実際に売却され、売却総額相当のポイントがユーザに還元されます。<br/>
                        現金化するにはダッシュボードのポイント管理から行ってください。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
            
        </div>
        
    </div>
    
</div>