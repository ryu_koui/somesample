<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<div class="site-index">

    <div class="herounit">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
            <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <span class="hero-img" style="background-image: url('/res/img/test/hero_1.jpg');"></span>
              <div class="carousel-caption" id="hero-caption-1">
                <h1 class="text-jumbo"><?= \Yii::t('app', '日本で不動産に投資しよう');?></h1>
                <h4 class="text-normal"><?= \Yii::t('app', '少ない資金で不動産に投資、運用するプラットフォームです');?></h4>
                <p><a class="btn btn-success btn-lg" href="*"><?= \Yii::t('app', 'ご利用方法');?></a></p>
              </div>
            </div>
            <div class="item">
              <span class="hero-img" style="background-image: url('/res/img/test/hero_2.jpg'); background-position: center top 65%;"></span>
            </div>
            <div class="item">
              <span class="hero-img" style="background-image: url('/res/img/test/hero_3.jpg');"></span>
            </div>
          </div>
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>

    <div class="body-content container">
        
        <div class="row" id="pickup">
            <div class="col-md-9 pickup-body">
                <div class="pickup-header row">
                    <h3 class="col-xs-6"><?= \Yii::t('app', '注目物件');?></h3>
                    <p class="col-xs-6 text-right show-all"><a class="btn btn-default"><?= \Yii::t('app', 'すべての注目物件を見る');?></a></p>
                </div>
                <?= $this->render('../prop/_big_tile', [
                    'props' => [$raising_props[2]],
                    'add_link' => true,
                ])?>
            </div>
            <!-- <div class="col-md-2 pickup-category">
                <ul>
                    <li class="current">東京</li>
                    <li><a href="#">京都</a></li>
                    <li><a href="#">大阪</a></li>
                    <li><a href="#">北海道</a></li>
                    <li><a href="#">沖縄</a></li>
                    <li><a href="#">一軒家</a></li>
                    <li><a href="#">タワーマンション</a></li>
                    <li><a href="#">ペントハウス</a></li>
                </ul>
            </div> -->
            <div class="col-md-3 contact-us">
                <h4><?= \Yii::t('app', 'お問い合わせ');?></h4>
                <div class="account-qr"><img src="/res/img/test/line_qr.gif"/></div>
                <div class="social-icons">
                    <i class="fa fa-facebook-official"></i>
                    <i class="fa fa-twitter"></i>
                    <i class="fa fa-google-plus"></i>
                    <i class="fa fa-qq"></i>
                    <i class="fa fa-tencent-weibo"></i>
                </div>
                <div class="phone"><i class="fa fa-phone"></i>&nbsp;&nbsp; 0120 555 555</div>
            </div>
        </div>
        
        <div class="top-spliter"></div>
        
        <h3 class=""><?= \Yii::t('app', '予約物件');?></h3>
        <div class="row property-group">
            <?= $this->render('../prop/_normal_tile', [
                'props' => $raising_props,
                'add_link' => true,
            ])?>
        </div>
        
        <div class="top-spliter"></div>
        
        <h3 class=""><?= \Yii::t('app', '運用中物件');?></h3>
        <div class="row property-group">
            <?= $this->render('../prop/_normal_tile', [
                'props' => $trading_props,
                'add_link' => true,
            ])?>
        </div>
        
    </div>
</div>
