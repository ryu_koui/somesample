<?php
use yii\helpers\Html;

$this->title = $title;
?>

<div class="prop-detail">
    <h1 class="h2 title"><?= Html::encode(Yii::t('app', $this->title)) ?></h1>
    <div class="body-content container">
        
        <?= Yii::$app->language?>
        
    <?php if(isset($error) && $error):?>
        <h3>Error</h3>
        <figure class="highlight"><pre>
            <?= $error?>
        </pre></figure>
    <?php endif?>
    
    <?php if(isset($result) && $result):?>
        <h3>Result</h3>
        <figure class="highlight"><pre>
            <?= $result?>
        </pre></figure>
    <?php endif?>
    
        <div class="container">
            <?= Html::a('Test Payment', '/test/payment', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>