<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
?>

<div>
    <h5><b><?= Yii::t('app', '収益履歴')?></b></h5>
    <?= GridView::widget([
        'dataProvider' => $profits,
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'created_at',
            ['value' => function($data) {
                return Yii::$app->formatter->asCurrency($data->amount);
            }],
        ]
    ]); ?>
</div>