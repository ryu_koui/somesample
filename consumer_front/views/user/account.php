<?php
use yii\helpers\Html;
use common\models\property\Status;
use sjaakp\gcharts\ColumnChart;

$this->title = Yii::t('app', 'アカウント管理');
?>

<div class="user-dashboard">

    <div class="herounit">
        
        <div class="hero-content">
            
        </div>
        
        <div class="user-main-tab container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="/user/dashboard" role="tab">ダッシュボード</a></li>
                <li role="presentation" class=""><a href="/user/point" role="tab">ポイント詳細</a></li>
                <li role="presentation" class="active"><a href="#" role="tab" data-toggle="tab">アカウント管理</a></li>
            </ul>
        </div>
        
    </div>
    
    <div class="body-content container">
        point
    </div>
    
</div>