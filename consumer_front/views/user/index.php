<?php
use yii\helpers\Html;
use common\models\property\Status;
use consumer_front\helpers\Prop;
use consumer_front\assets\UserDashboradAsset;
use sjaakp\gcharts\ColumnChart;

UserDashboradAsset::register($this);
$this->title = Yii::t('app', 'ダッシュボード');
?>
<div class="user-dashboard">
    
    <div class="herounit">
        <div class="hero-content">
            <ul class="dash-info">
                <li>
                    <p class="contents-subtile"><?= Yii::t('app', '収入総額')?></p>
                    <p class="contents-params total-earnings"><?= Yii::$app->formatter->asCurrency($point->total_earnings)?></p>
                </li>
                <li>
                    <p class="contents-subtile"><?= Yii::t('app', '総利益率')?></p>
                    <p class="contents-params">
                    <?php if ($point->total_expenses == 0):?>
                        0
                    <?php else:?>    
                        <?= number_format($point->total_earnings / $point->total_expenses * 100, 1)?>
                    <?php endif?>
                        <span class="unit">%</span>
                    </p>
                </li>
            </ul>
        </div>
        
        <div class="user-main-tab container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#" role="tab" data-toggle="tab"><?= Yii::t('app', 'ダッシュボード')?></a></li>
                <li role="presentation" class=""><a href="/user/point" role="tab"><?= Yii::t('app', 'ポイント詳細')?></a></li>
                <li role="presentation" class=""><a href="/user/account" role="tab"><?= Yii::t('app', 'アカウント管理')?></a></li>
            </ul>
        </div>
    </div>
    
    <div class="body-content container">
        
        <div class="row">
            
            <div class="col-md-9 user-dashboard-body">
                
                <div class="user-props">
                    
                    <div class="user-total-earnings-graph">
                        <?= ColumnChart::widget([
                            'height' => '200px',
                            'dataProvider' => $earnings,
                            'columns' => [
                                [
                                    'attribute' => 'created_at',
                                    'label' => '月',
                                    'type' => 'string',
                                    'value' => function($model, $a, $i, $w) {
                                        return date('m-d', strtotime($model->created_at));
                                    },
                                ],
                                [
                                    'attribute' => 'amount',
                                    'label' => Yii::t('app', '利益額'),
                                ],
                                [
                                    'attribute' => 'amount',
                                    'role' => 'annotation',
                                    'type' => 'string',
                                    'value' => function($model, $a, $i, $w) {
                                        return Yii::$app->formatter->asCurrency($model->amount);
                                    }
                                ],
                            ],
                            'options' => [
                                'title' => Yii::t('app', '過去半年の収益グラフ'),
                                'backgroundColor' => '#f5f5f5',
                            ],
                        ]) ?>
                    </div>
                    
                    <?php foreach($stockProps as $stockProp):?>
                        <div class="user-prop-tile">
                            <div class="user-prop-base">
                                <div class="user-prop-tile-thumbnail">
                                    <a href="/prop/<?= $stockProp->prop_id?>"><img src="/res/img/test/prop_<?= $stockProp->prop_id?>.jpg" width="340px"/></a>
                                </div>
                                <div class="user-prop-info-body">
                                    <div class="user-prop-info-base">
                                        <h5><?= Html::encode($stockProp->propinfo->title)?></h5>
                                        <ul>
                                            <li>
                                                <p class="contents-subtile">
                                            <?php if($stockProp->propinfo->status == Status::RAISING):?>
                                                    <?= Yii::t('app', '予約株数')?>
                                            <?php else:?>
                                                    <?= Yii::t('app', '所持株数')?>
                                            <?php endif?>
                                                </p>
                                                <p class="contents-params"><?= Yii::$app->formatter->asInteger($stockProp->stock_cnt)?></p>
                                            </li>
                                        <?php if($stockProp->onsale_cnt > 0):?>
                                            <li>
                                                <p class="contents-subtile"><?= Yii::t('app', '売出中株数')?></p>
                                                <p class="contents-params"><?= $stockProp->onsale_cnt?></p>
                                            </li>
                                        <?php endif?>
                                            <li>
                                                <p class="contents-subtile">
                                            <?php if($stockProp->propinfo->status == Status::RAISING):?>
                                                    <?= Yii::t('app', '支払い予定額')?>
                                            <?php else:?>
                                                    <?= Yii::t('app', '購入総額')?>
                                            <?php endif?>
                                                </p>
                                                <p class="contents-params">
                                            <?php if($stockProp->propinfo->status == Status::RAISING):?>
                                                <?= Yii::$app->formatter->asCurrency($stockProp->propinfo->price / $stockProp->propinfo->stock_num * $stockProp->stock_cnt)?>
                                            <?php else:?>
                                                <?= Yii::$app->formatter->asCurrency($stockProp->purchaseTotal)?>
                                            <?php endif?>
                                                </p>
                                            </li>
                                        
                                        <?php if($stockProp->propinfo->status == Status::RAISING):?>
                                            <div><span class="label label-danger slim"><i class="fa fa-money"></i> <?= Yii::t('app', '予約キャンセル')?></span></div>
                                        <?php else:?>
                                            <div><a href="/propsell/<?= $stockProp->prop_id?>/stocks" style="text-decoration: none;"><span class="label label-danger slim"><i class="fa fa-money"></i> <?= Yii::t('app', '所持株売却')?></span></a></div>
                                        <?php endif?>                                        
                                        </ul>
                                    </div>
                                <?php if($stockProp->propinfo->status == Status::RAISING):?>
                                    <div class="user-prop-info-penal reserve">
                                        <h6><?= Yii::t('app', '予約進行状況')?></h6>
                                        <div class="user-prop-info-per">
                                            <span class="per-title"><?= Yii::t('app', '予約率')?> </span><span class="per-number"><?= number_format(Prop::compratio($stockProp->propinfo, false), 1)?></span><span class="per-unit">%</span>
                                        </div>
                                        <div class="user-prop-info-amount">
                                            <span class="amount-title"><?= Yii::t('app', '残額')?> </span><span class="amount-number"><?= Prop::balance($stockProp->propinfo)?></span>
                                        </div>
                                        <div class="user-prop-info-penal-btn buymore">
                                            <a href="/propdeal/<?= $stockProp->prop_id?>/stocks"><span class="label label-warning"><i class="fa fa-cart-plus"></i> <?= Yii::t('app', '追加購入')?></span></a>
                                        </div>
                                    </div>
                                <?php else:?>
                                    <div class="user-prop-info-penal earning">
                                        <h6><?= Yii::t('app', '前月収益実績')?></h6>
                                        <div class="user-prop-info-per">
                                            <span class="per-title"><?= Yii::t('app', '利益率')?> </span><span class="per-number"><?= number_format($stockProp->getLastEarning()->per, 1)?></span><span class="per-unit">%</span>
                                        </div>
                                        <div class="user-prop-info-amount">
                                            <span class="amount-title"><?= Yii::t('app', '収益額')?> </span><span class="amount-number"><?= Yii::$app->formatter->asCurrency($stockProp->getLastEarning()->total_amount)?></span>
                                        </div>
                                        <div class="user-prop-info-penal-btn seemore">
                                            <span class="label label-success" onclick="loadProfitDetail(this, <?= $stockProp->prop_id?>);"><i class="fa fa-plus"></i> <?= Yii::t('app', '収益履歴を見る')?></span>
                                            <span class="label label-success hide" onclick="closeProfitDetail(this, <?= $stockProp->prop_id?>)"><i class="fa fa-minus"></i> <?= Yii::t('app', '収益履歴を閉じる')?></span>
                                        </div>
                                    </div>
                                <?php endif?>                                    
                                </div>
                            </div>
                            <div class="user-prop-detail" id="user-prop-detail-<?= $stockProp->prop_id?>">
                                
                            </div>
                        </div>
                    <?php endforeach?>
                    
                </div>
                
            </div>
            
            <div class="col-md-3 user-dashboard-side">
                
                <div class="recommend-prop">
                    <h4 class="h6 bold"><?= Yii::t('app', 'おすすめ')?></h4>
                    <?= $this->render('../prop/_normal_tile', [
                        'props' => [$recommendProp],
                        'add_link' => true,
                        'widthClass' => 'col-md-12'
                    ])?>
                </div>
                
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    
</div>