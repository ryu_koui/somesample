<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\user;

$this->title = Yii::t('app', 'ポイント換金');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ポイント詳細'), 'url' => '/user/point'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-dashboard">
    
    <h1 class="h2 title text-center"><?= Html::encode($this->title) ?></h1>
    
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-3">
                <li><?= Yii::t('app', '換金額の入力')?></li>
                <li class="active"><?= Yii::t('app', '換金情報確認')?></li>
                <li><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"> </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-8 user-dashboard-body">
                <div class="point-exchange-confirm">
                    <table class="table">
                        <caption><?= Yii::t('app', 'ポイント換金情報')?></caption>
                        <tbody>
                            <tr>
                                <th scope="row"><?= Yii::t('app', 'ポイント残額')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($point->points)?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '換金額')?></th>
                                <td><?= Yii::$app->formatter->asCurrency($model->amount)?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '送金方法')?></th>
                                <td><i class="fa fa-paypal"></i> PayPal</td>
                            </tr>
                            <tr>
                                <th scope="row"><?= Yii::t('app', '受取りアカウント')?></th>
                                <td><?= Html::encode($paypal->email)?></td>
                            </tr>
                            <tr><td></td><td></td></tr>
                        </tbody>
                    </table>
                    
                    <div class="buy-stock-confirm-btn">
                        <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($model, 'amount', [
                                'template' => "{input}",
                            ])->hiddenInput();?>
                            <?= Html::hiddenInput('exchange', 1)?>
                            <div class="form-group">
                                <?= Html::submitButton('<b><i class="fa fa-paypal"></i> '.Yii::t('app', '送金処理に進む').'</b>', ['class' => 'btn btn-success btn-lg']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    
                </div>
            </div>
            
            <div class="col-md-4 user-dashboard-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 株の売却は買い取りされるまで成立しません
                    </div>
                    <p>
                        売却手続きは該当の株の売却宣言のみで、<br/>
                        実際にすぐには売却されません。
                    </p>
                    <p>
                        買い取り手が出て始めて実際に売却され、売却総額相当のポイントがユーザに還元されます。<br/>
                        現金化するにはダッシュボードのポイント管理から行ってください。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>