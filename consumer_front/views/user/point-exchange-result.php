<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\user;

$this->title = Yii::t('app', 'ポイント換金');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ポイント詳細'), 'url' => '/user/point'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-dashboard">
    
    <h1 class="h2 title text-center"><?= Html::encode($this->title) ?></h1>
    
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-3">
                <li><?= Yii::t('app', '換金額の入力')?></li>
                <li><?= Yii::t('app', '換金情報確認')?></li>
                <li class="active"><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"> </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-2"></div>
            
            <div class="col-md-8 user-dashboard-body">
                <div class="point-exchange-confirm">
                   
                    <h3 class="text-center"><?= Yii::t('app', 'ポイント換金手続き完了しました！')?></h3>
                   
                    <div class="text-center">
                        <?= Html::a(Yii::t('app', 'ポイント詳細'), '/user/point', ['class' => 'btn btn-lg btn-success']) ?>
                    </div>
                </div>
            </div>
            
            <div class="col-md-2"></div>
        </div>
    </div>
    
</div>