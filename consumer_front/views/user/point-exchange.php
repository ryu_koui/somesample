<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\user;

$this->title = Yii::t('app', 'ポイント換金');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ポイント詳細'), 'url' => '/user/point'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-dashboard">
    
    <h1 class="h2 title text-center"><?= Html::encode($this->title) ?></h1>
    
    <div class="body-content container">
        
        <div class="flow-step-indicator">
            <ul id="progress" class="progress-3">
                <li class="active"><?= Yii::t('app', '換金額の入力')?></li>
                <li><?= Yii::t('app', '換金情報確認')?></li>
                <li><?= Yii::t('app', '完了')?></li>
            </ul>
            <div style="clear: both;text-align:center;"> </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-8 user-dashboard-body">
                
                <div class="point-exchanger">
                    <div class="point-exchanger-body">
                        <?php $form = ActiveForm::begin(['id' => 'point_exchange_form', 'action' => '/user/point-exchange-confirm']); ?>
                        <div class="point-exchanger-input">
                            <label for="value-input-num"><?= Yii::t('app', '換金可能金額')?> <?= Yii::$app->formatter->asCurrency($point->points)?></label>
                            <div style="clear: both;"></div>
                            <ul class="nav navbar-nav">
                                <li>
                                    <div class="input-group input-group-sm exchange-input">
                                        <!-- <span class="input-group-addon" id="exchange-amount-addon">￥</span> -->
                                        <?= $form->field($model, 'amount', [
                                            'template' => "{input}\n{hint}\n{error}",
                                        ])->textInput([
                                            'maxlength' => true,
                                            'id' => "value-input-amount",
                                            'class' => "form-control value-input",
                                            // 'aria-describedby' => "exchange-amount-addon",
                                            'placeholder' => Yii::t('app', '換金したい金額を入力'),
                                        ])?>
                                    </div>
                                </li>
                            </ul>
                            <div style="clear: both;"></div>
                            
                            <div class="help-block text-danger"></div>
                            
                        </div>
                        <div class="point-exchanger-info">
                            <ul>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '送金方法')?></p>
                                    <p class="contents-params"><i class="fa fa-paypal"></i> PayPal</p>
                                </li>
                                <li>
                                    <p class="contents-subtile"><?= Yii::t('app', '受取りアカウント')?></p>
                                    <p class="contents-params"><?= Html::encode($paypal->email)?></p>
                                </li>
                            </ul>
                            <?= Html::submitButton(Yii::t('app', '確定して次へ'), ['id' => 'next-step-btn', 'class' => 'btn btn-lg btn-success pull-right']) ?>
                            <div style="clear: both;"></div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                
            </div>
            
            <div class="col-md-4 user-dashboard-side">
                <div class="warning-panel">
                    <div class="bold">
                        <i class="fa fa-exclamation-triangle"></i> 株の売却は買い取りされるまで成立しません
                    </div>
                    <p>
                        売却手続きは該当の株の売却宣言のみで、<br/>
                        実際にすぐには売却されません。
                    </p>
                    <p>
                        買い取り手が出て始めて実際に売却され、売却総額相当のポイントがユーザに還元されます。<br/>
                        現金化するにはダッシュボードのポイント管理から行ってください。
                    </p>
                </div>
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>