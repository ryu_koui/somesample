<?php
use yii\helpers\Html;
use common\models\user;
use yii\grid\GridView;

$this->title = Yii::t('app', 'ポイント詳細');
?>

<div class="user-dashboard">

    <div class="herounit">
        
        <div class="hero-content">
            <ul class="dash-info">
                <li>
                    <p class="contents-subtile"><?= Yii::t('app', '所持ポイント')?></p>
                    <p class="contents-params total-earnings"><?= Yii::$app->formatter->asCurrency($point->points)?></p>
                </li>
                <li>
                    <p class="contents-params">
                    <?php if ($point->points > 0):?>
                        <?= Html::a(Yii::t('app', 'ポイント換金'), '/user/point-exchange', ['class' => 'btn btn-success point-exchange']) ?>
                    <?php else:?>
                        <button class="btn btn-success point-exchange" disabled="disabled "><?= Yii::t('app', 'ポイント換金')?></button>
                    <?php endif?>
                    </p>
                </li>
            </ul>
        </div>
        
        <div class="user-main-tab container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class=""><a href="/user/dashboard" role="tab"><?= Yii::t('app', 'ダッシュボード')?></a></li>
                <li role="presentation" class="active"><a href="#" role="tab" data-toggle="tab"><?= Yii::t('app', 'ポイント詳細')?></a></li>
                <li role="presentation" class=""><a href="/user/account" role="tab"><?= Yii::t('app', 'アカウント管理')?></a></li>
            </ul>
        </div>
        
    </div>
    
    <div class="body-content container">
        <div class="row">
            
            <div class="col-md-9 user-dashboard-body">
                <h3><?= Yii::t('app', 'ポイント履歴')?></h3>
                
                <?= GridView::widget([
                    'dataProvider' => $point_his,
                    'columns' => [
                        [
                            'label' => Yii::t('app', '日時'),
                            'attribute' => 'created_at',
                            'format' => ['relativeTime'],
                        ],
                        [
                            'label' => Yii::t('app', '目的'),
                            'value' => function($data) {
                                return user\PointHistory::getActionLabel($data->action);
                            }
                        ],
                        [
                            'label' => Yii::t('app', '入出金額'),
                            'attribute' => 'change',
                            'format' => ['currency'],
                        ],
                        [
                            'label' => Yii::t('app', '所持ポイント'),
                            'attribute' => 'points',
                            'format' => ['currency'],
                        ],
                    ]
                ]); ?>
            </div>
            
            <div class="col-md-3 user-dashboard-side">
                
                <div class="recommend-prop">
                    <h4 class="h6 bold"><?= Yii::t('app', 'おすすめ')?></h4>
                    <?= $this->render('../prop/_normal_tile', [
                        'props' => [$recommendProp],
                        'add_link' => true,
                        'widthClass' => 'col-md-12'
                    ])?>
                </div>
                
                <div class="help-panel">
                    <h4 class="h6 bold"><?= Yii::t('app', 'よくある質問')?></h4>
                    <ul class="">
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            質問１
                        </li>
                        <div class="collapse" id="collapse1">
                            回答１、文言も長い
                        </div>
                        <li class="collapsed" role="button" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            質問2
                        </li>
                        <div class="collapse" id="collapse2">
                            回答2、文言も長い
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>