/**
 * @copyright Copyright &copy; 2015 Andrew Blake
 * @package andrewblake1\yii2-credit-card
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://github.com/andrewblake1/yii2-credit-card
 * @version 1.0.2
 */
(function($) {
	$('#creditcard_form').submit(function(e){
		var expireObj = $.payment.cardExpiryVal($('#creditcard-expire_date').val());
		$('#creditcard_form').find(':hidden[name="Creditcard[expire_month]"]').attr('value', expireObj.month);
		$('#creditcard_form').find(':hidden[name="Creditcard[expire_year]"]').attr('value', expireObj.year);
		
	});
    /**
     * plugins
     */

    /**
     * Mask credit card number and change credit card icon when credit card type can be detected
     *
     * Using stripes https://github.com/stripe/jquery.payment and font-awesome
     *
     * @returns {$.fn}
     */
    $.fn.ccNumber = function () {
        var $this = $(this);

        // format and mask as the user types
        $this.payment("formatCardNumber")
            // set credit card icon as type
            // TODO: should be on change but working around issue https://github.com/stripe/jquery.payment/issues/159
            .on("keyup paste blur", function () {
                var ccNumber = $this.val();
                var type = $.payment.cardType(ccNumber);
                var icon;
                switch (type) {
                    case 'visa':
                        icon = 'cc-visa';
                        break;
                    case 'mastercard':
                        icon = 'cc-mastercard';
                        break;
                    case 'amex':
                        icon = 'cc-amex';
                        break;
                    case 'dinersclub':
                        icon = 'cc-diners-club';
                        break;
                    case 'discover':
                        icon = 'cc-discover';
                        break;
                    case 'jcb':
                        icon = 'cc-jcb';
                        break;
                    case 'unionpay':
                    case 'visaelectron':
                    case 'maestro':
                    case 'forbrugsforeningen':
                    case 'dankort':
                    default:
                        icon = 'credit-card';
                }
                $this.parent().find('.fa').removeClass().addClass('fa fa-lg fa-' + icon);
                
                type = type === null ? 'unknown' : type;
                $this.parents('form').find(':hidden[name="Creditcard[type]"]').attr('value', type);
            });

        return this;
    }
    
}(jQuery));
