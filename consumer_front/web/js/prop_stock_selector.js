(function($) {
	var StockSelector = {
		formName: '',
		rawList: [],
		currentList: [],
		maxNum: 0,
		maxPrice: 0,
		selectedNum: 0,
		selectedPrice: 0,
		selectors: {},
		
		setSelectors: function() {
			this.selectors = {
				numInput:		$('#value-input-num'),
				numInputBtn:	$('#select-by-num-btn'),
				numInputHelp:	$('#stock-selector-by-num .help-block'),
				priceInput:		$('#value-input-price'),
				priceInputBtn:	$('#select-by-price-btn'),
				priceInputHelp:	$('#stock-selector-by-price .help-block'),
				selectedNum:	$('#seleted_stock_num'),
				selectedPrice:	$('#seleted_stock_price'),
				selectForm:		$('#stock_select_form'),
			};
		},
		
		resetSelected: function(all) {
			this.selectedNum = 0;
			this.selectedPrice = 0;
			this.currentList = $.extend(true, [], this.rawList);
			$('#stock_select_list .stocks-select-panel').removeClass('selected').find('input.value-input').val('');
			
			this.selectors.selectForm.children('input.required_stock_info').remove();
			this.selectors.selectForm.children('button').prop('disabled', true);
			
			this.selectors.selectedNum.text(this.selectedNum);
			this.selectors.selectedPrice.text(this.formatCurrency(this.selectedPrice));
			
			this.selectors.numInputHelp.text('').parent().removeClass('has-error');
			this.selectors.priceInputHelp.text('').parent().removeClass('has-error');
			
			if (all) {
				this.selectors.numInput.val('');
				this.selectors.priceInput.val('');
			}
		},
		
		selectByNum: function(num) {
			var left = num;
			for (var n in this.rawList) {
				if (left <= 0) break;
				
				if (left >= this.rawList[n].onsale_cnt) {
					left = left - this.rawList[n].onsale_cnt;
					this.selectedPrice += this.rawList[n].onsale_cnt * this.rawList[n].unit_price;
					this.currentList[n].sold_cnt = this.rawList[n].onsale_cnt;
				} else {
					this.selectedPrice += left * this.rawList[n].unit_price;
					this.currentList[n].onsale_cnt = this.rawList[n].onsale_cnt - left;
					this.currentList[n].sold_cnt = left;
					break;
				}
			}
			this.selectedNum = num;
		},
		
		selectByPrice: function(price) {
			var left = price;
			var cnt = 0;
			var sumPrice = 0;
			for (var n in this.rawList) {
				if (left <= 0) break;
				
				var times = Math.floor(left / this.rawList[n].unit_price);
				if (times < 1) break;
				
				if (times >= this.rawList[n].onsale_cnt) {
					left = left - this.rawList[n].unit_price * this.rawList[n].onsale_cnt;
					this.currentList[n].sold_cnt = this.rawList[n].onsale_cnt;
					cnt += this.rawList[n].onsale_cnt;
					sumPrice += this.rawList[n].onsale_cnt * this.rawList[n].unit_price;
				} else {
					this.currentList[n].onsale_cnt = this.rawList[n].onsale_cnt - times;
					this.currentList[n].sold_cnt = times;
					cnt += times;
					sumPrice += times * this.rawList[n].unit_price;
					break;
				}
			}
			if (cnt < 1) {
				this.selectInputError(this.selectors.priceInputHelp);
				return;
			}
			this.selectedNum = cnt;
			this.selectedPrice = sumPrice;
		},
		
		renderCurrentState: function() {
			this.selectors.selectedNum.text(this.selectedNum);
			this.selectors.selectedPrice.text(this.formatCurrency(this.selectedPrice));
			for (var n in this.currentList) {
				if (!this.currentList[n].sold_cnt) continue;
				
				var elem = $('#stock-select-' + this.currentList[n].stock_id).addClass('selected');
				elem.find('input.value-input').val(this.currentList[n].sold_cnt);
				
				$('<input>').attr({
					type: 'hidden',
					class: 'required_stock_info',
					name: this.formName + '[required_stock_list][' + this.currentList[n].stock_id + '][num]',
					value: this.currentList[n].sold_cnt
				}).appendTo(this.selectors.selectForm);
				$('<input>').attr({
					type: 'hidden',
					class: 'required_stock_info',
					name: this.formName + '[required_stock_list][' + this.currentList[n].stock_id + '][unit_price]',
					value: this.currentList[n].unit_price
				}).appendTo(this.selectors.selectForm);
			}
			
			$('<input>').attr({
				type: 'hidden',
				class: 'required_stock_info',
				name: this.formName + '[required_price]',
				value: this.selectedPrice
			}).appendTo(this.selectors.selectForm);
			
			$('<input>').attr({
				type: 'hidden',
				class: 'required_stock_info',
				name: this.formName + '[required_num]',
				value: this.selectedNum
			}).appendTo(this.selectors.selectForm);
			
			this.selectors.selectForm.children('button').prop('disabled', false);
		},
		
		selectByNumEvent: function(e) {
			var self = this;
			this.selectors.numInputBtn.click(function(e) {
				self.resetSelected();
				var num = self.selectors.numInput.val();
				if (num > self.maxNum || num < 1) {
					self.selectInputError(self.selectors.numInputHelp);
					return;
				}
				
				self.selectByNum(num);
				self.renderCurrentState();
			});
		},
		
		selectByPriceEvent: function(e) {
			var self = this;
			this.selectors.priceInputBtn.click(function(e) {
				self.resetSelected();
				var price = self.selectors.priceInput.val();
				if (price > self.maxPrice || price < 1) {
					self.selectInputError(self.selectors.priceInputHelp);
					return;
				}
				
				self.selectByPrice(price);
				self.renderCurrentState();
			});
		},
		
		tabEvent: function(e) {
			var self = this;
			$('#stock-selector li.selector-tab').click(function(){
				if ($(this).hasClass('active')) return;
				self.resetSelected(true);
			});
		},
		
		init: function(form_name, stock_list, max_stock_num, max_stock_price) {
			this.formName = form_name;
			this.rawList = stock_list;
			this.maxNum = max_stock_num;
			this.maxPrice = max_stock_price;
			this.setSelectors();
			this.selectByNumEvent();
			this.selectByPriceEvent();
			this.tabEvent();
		},
		
		selectInputError: function(help) {
			help.html('Max: ' + this.maxNum);
			help.parent().addClass('has-error');
		},
		
		formatCurrency: function(value) {
			value = Math.floor(value);
			return '￥ ' + String(value).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );
		}
	};
	
	if (typeof stock_list != 'undefined') {
		StockSelector.init(form_name, stock_list, max_stock_num, max_stock_price);
	}
}(jQuery));