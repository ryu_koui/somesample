(function($) {
	var StockSelector = {
		maxNum: 0,
		selectors: {},
		
		setSelectors: function() {
			this.selectors = {
				numInput:		$('#value-input-num'),
				priceInput:		$('#value-input-price'),
				inputBtn:		$('#select-btn'),
				inputHelp:		$('#stock-selector .help-block'),
				selectedNum:	$('#seleted_stock_num'),
				selectedPrice:	$('#seleted_stock_price'),
				selectForm:		$('#stock_select_form'),
				formBtn:		$('#next-step-btn'),
			}
		},
		
		selectEvent: function() {
			var self = this;
			this.selectors.inputBtn.click(function(e) {
				self.reset();
				
				var num = self.selectors.numInput.val();
				var price = self.selectors.priceInput.val();
				
				if (num > self.maxNum) {
					self.inputError('Max stock: ' + self.maxNum);
					return;
				}
				if (price < 1) {
					self.inputError('Min price: 1');
					return;
				}
				
				self.selectors.selectedNum.text(num);
				self.selectors.selectedPrice.text(self.formatCurrency(price));
				self.selectors.formBtn.prop('disabled', false);
			});
		},
		
		inputError: function(message) {
			this.selectors.inputHelp.html(message);
			this.selectors.inputHelp.parent().addClass('has-error');
		},
		
		init: function(max_num) {
			this.maxNum = max_num;
			this.setSelectors();
			this.selectEvent();
		},
		
		reset: function() {
			this.selectors.formBtn.prop('disabled', 'disabled');
			this.selectors.selectedNum.text(0);
			this.selectors.selectedPrice.text(this.formatCurrency(0));
			this.selectors.inputHelp.html('');
			this.selectors.inputHelp.parent().removeClass('has-error');
		},
		
		formatCurrency: function(value) {
			value = Math.floor(value);
			return '￥ ' + String(value).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );
		}
	}
	
	StockSelector.init(max_num);
}(jQuery));