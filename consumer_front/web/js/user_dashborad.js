function loadProfitDetail(self, prop_id) {
	var detail_div = $('#user-prop-detail-' + prop_id);
	if (detail_div.find('.grid-view').size() > 0) {
		$(self).addClass('hide').siblings('span').removeClass('hide');
		detail_div.slideDown('fast');
		return;
	}
	
	$.post('/user/propprofit', {
		'prop_id': prop_id
	}, function(data) {
		$(self).addClass('hide').siblings('span').removeClass('hide');
		detail_div.html(data.html).slideDown('fast');
	}, 'json');
}
function closeProfitDetail(self, prop_id) {
	$(self).addClass('hide').siblings('span').removeClass('hide');
	$('#user-prop-detail-' + prop_id).slideUp('fast');
}

(function($) {
	
}(jQuery));