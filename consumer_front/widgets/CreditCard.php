<?php
namespace consumer_front\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\account;
use consumer_front\assets\FontAwesomeAsset;
use consumer_front\assets\PaymentAsset;
use consumer_front\assets\CreditCardAsset;

class CreditCard extends Widget
{
    public $form;
    public $model;
    
    public function init()
    {
        parent::init();
        FontAwesomeAsset::register($this->view);
        PaymentAsset::register($this->view);
        CreditCardAsset::register($this->view);
    }
    
    public function setForm($form)
    {
        $this->form = $form;
        $this->checkConfig();
    }
    
    public function number($fieldConfig = [])
    {
        $fieldConfig = ArrayHelper::merge([
            'inputTemplate' => '<div class="input-group"><span class="input-group-addon"><i class="fa fa-lg fa-credit-card"></i></span>{input}</div>',
            'inputOptions' => [
                'type' => 'tel',
                'autocomplete' => 'cc-number',
                'placeholder' => Yii::t('creditcard', 'Card number'),
            ],
        ], $fieldConfig);
        
        $fieldConfig['inputOptions']['id'] = Html::getInputId($this->model, 'number');
        $this->view->registerJs("jQuery('#{$fieldConfig['inputOptions']['id']}').ccNumber();");
        return $this->form->field($this->model, 'number', $fieldConfig)->textInput();
    }

    public function expiry($fieldConfig = [])
    {
        $fieldConfig = ArrayHelper::merge([
            'inputOptions' => [
                'autocomplete' => 'cc-exp',
                'placeholder' => Yii::t('creditcard', 'MM / YY'),
            ],
        ], $fieldConfig);
        
        $fieldConfig['inputOptions']['id'] = Html::getInputId($this->model, 'expire_date');
        $this->view->registerJs("jQuery('#{$fieldConfig['inputOptions']['id']}').payment('formatCardExpiry');");
        return $this->form->field($this->model, 'expire_date', $fieldConfig)->textInput();
    }

    public function cvc($fieldConfig = [])
    {
        $fieldConfig = ArrayHelper::merge([
            'inputOptions' => [
                'placeholder' => Yii::t('creditcard', 'CV code'),
                'autocomplete' => 'off',
            ],
        ], $fieldConfig);
        
        $fieldConfig['inputOptions']['id'] = Html::getInputId($this->model, 'cvc');
        $this->view->registerJs("jQuery('#{$fieldConfig['inputOptions']['id']}').payment('formatCardCVC');");

        // build the field
        return $this->form->field($this->model, 'cvc', $fieldConfig)->textInput();
    }

    public function name($fieldConfig = [])
    {
        $fieldConfig = ArrayHelper::merge([
            'inputOptions' => [
                'placeholder' => Yii::t('creditcard', 'Name'),
            ],
        ], $fieldConfig);
        
        $fieldConfig['inputOptions']['id'] = Html::getInputId($this->model, 'name');
        return $this->form->field($this->model, 'name', $fieldConfig)->textInput();
    }
    
    public function hidden($name, $fieldConfig = [])
    {
        return Html::activeHiddenInput($this->model, $name);
    }

    private function checkConfig()
    {
        if (!$this->form) {
            throw new InvalidConfigException("The 'form' property must be defined and must be an instance of
                yii\widgets\ActiveForm");
        } elseif (!$this->form instanceof \yii\widgets\ActiveForm) {
            throw new InvalidConfigException("The 'form' property must be an instance of yii\widgets\ActiveForm");
        }
    }
}
