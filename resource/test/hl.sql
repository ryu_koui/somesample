-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: localhost    Database: hl
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_creditcard`
--

DROP TABLE IF EXISTS `account_creditcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_creditcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` smallint(6) NOT NULL,
  `type` varchar(32) NOT NULL,
  `number` varchar(20) NOT NULL,
  `expire_month` smallint(6) NOT NULL,
  `expire_year` smallint(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `payout_default` smallint(6) NOT NULL DEFAULT '0',
  `payment_default` smallint(6) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_creditcard`
--

LOCK TABLES `account_creditcard` WRITE;
/*!40000 ALTER TABLE `account_creditcard` DISABLE KEYS */;
INSERT INTO `account_creditcard` VALUES (15,2,'visa','4980 0064 1111 1111',1,2021,'liu',0,0,'2015-09-27 15:18:08','2015-09-27 15:18:08'),(16,3,'visa','4986 0046 1111 1111',4,2019,'RYU KOUI',0,0,'2015-09-30 22:36:05','2015-09-30 22:36:05');
/*!40000 ALTER TABLE `account_creditcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_paypal`
--

DROP TABLE IF EXISTS `account_paypal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_paypal` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL DEFAULT '',
  `payer_id` varchar(255) NOT NULL DEFAULT '',
  `payout_default` smallint(6) NOT NULL DEFAULT '0',
  `payment_default` smallint(6) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_paypal`
--

LOCK TABLES `account_paypal` WRITE;
/*!40000 ALTER TABLE `account_paypal` DISABLE KEYS */;
INSERT INTO `account_paypal` VALUES (3,'customer1@vadek.net','8316850414','CPNVP4FTB4Z5Q',0,0,'2015-12-19 16:20:56','2015-12-19 16:20:56'),(4,'customer1@vadek.net','8316850414','CPNVP4FTB4Z5Q',0,0,'2015-12-21 12:19:57','2015-12-21 12:19:57');
/*!40000 ALTER TABLE `account_paypal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_info`
--

DROP TABLE IF EXISTS `campaign_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(32) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` text,
  `fee_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_info`
--

LOCK TABLES `campaign_info` WRITE;
/*!40000 ALTER TABLE `campaign_info` DISABLE KEYS */;
INSERT INTO `campaign_info` VALUES (1,'BuyFeeDiscount1','購入手数料半額','購入手数料半額',2,10,'2015-11-28 22:30:20','2015-11-28 22:30:20'),(2,'ProfitRatioGuarantee1','利益率保証','利益率保証',NULL,10,'2015-11-28 22:30:20','2015-11-28 22:30:20');
/*!40000 ALTER TABLE `campaign_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_info_lang_ja-JP`
--

DROP TABLE IF EXISTS `campaign_info_lang_ja-JP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_info_lang_ja-JP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_info_lang_ja-JP`
--

LOCK TABLES `campaign_info_lang_ja-JP` WRITE;
/*!40000 ALTER TABLE `campaign_info_lang_ja-JP` DISABLE KEYS */;
INSERT INTO `campaign_info_lang_ja-JP` VALUES (1,'購入手数料半額','購入手数料半額'),(2,'利益率5%保証','利益率5%保証');
/*!40000 ALTER TABLE `campaign_info_lang_ja-JP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_info_lang_zh-CN`
--

DROP TABLE IF EXISTS `campaign_info_lang_zh-CN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_info_lang_zh-CN` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_info_lang_zh-CN`
--

LOCK TABLES `campaign_info_lang_zh-CN` WRITE;
/*!40000 ALTER TABLE `campaign_info_lang_zh-CN` DISABLE KEYS */;
INSERT INTO `campaign_info_lang_zh-CN` VALUES (1,'购买手续费半价','购买手续费半价'),(2,'利润率5%保障','利润率5%保障');
/*!40000 ALTER TABLE `campaign_info_lang_zh-CN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_info`
--

DROP TABLE IF EXISTS `fee_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_deposit_per` decimal(4,2) DEFAULT NULL,
  `buy_stock_per` decimal(4,2) DEFAULT NULL,
  `sell_stock_per` decimal(4,2) DEFAULT NULL,
  `user_earnings_per` decimal(4,2) DEFAULT NULL,
  `point_exchange_per` decimal(4,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_info`
--

LOCK TABLES `fee_info` WRITE;
/*!40000 ALTER TABLE `fee_info` DISABLE KEYS */;
INSERT INTO `fee_info` VALUES (1,10.00,2.00,0.50,0.05,3.00,'2015-12-17 22:30:20','2015-12-17 22:30:20'),(2,NULL,1.00,NULL,NULL,NULL,'2015-12-17 22:30:20','2015-12-17 22:30:20');
/*!40000 ALTER TABLE `fee_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1425727988),('m130524_201442_init',1425727997),('m150913_141312_create_prop_base_info_table',1442158593),('m150917_222214_rename_prop_baseinfo_table',1442528725),('m150918_181604_create_prop_info_table',1442605113),('m150919_090439_rename_prop_info_table_pk',1442653698),('m150919_124005_create_user_info_table',1442667804),('m150921_040703_create_prop_stocks_table',1442990400),('m150923_044431_create_user_point_table',1443285406),('m150927_111126_recreate_prop_stocks_table',1443353258),('m151104_135449_change_prop_info_status_column',1446646282),('m151106_223947_create_prop_profit',1446853735),('m151106_234132_create_stock_detail',1446866296),('m151107_045502_create_prop_share_his',1446875931),('m151107_045538_create_user_earnings_his',1446875932),('m151216_093041_create_user_point_his',1450258988),('m151217_070512_create_account_paypal',1450337893),('m151217_200947_create_campaign',1450383815),('m151217_204833_create_prop_campaign',1450385981),('m151217_210347_create_prop_stock_campaign',1450386477),('m151218_000401_create_fee_info',1450398915),('m151220_085756_create_prop_info_lang_ja',1450602090),('m151220_125451_create_campaign_info_lang',1450616272);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_campaign`
--

DROP TABLE IF EXISTS `prop_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `camp_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prop_id` (`prop_id`),
  KEY `camp_id` (`camp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_campaign`
--

LOCK TABLES `prop_campaign` WRITE;
/*!40000 ALTER TABLE `prop_campaign` DISABLE KEYS */;
INSERT INTO `prop_campaign` VALUES (1,3,1,'2015-11-28 22:30:20','2015-11-28 22:30:20'),(2,3,2,'2015-11-28 22:30:20','2015-11-28 22:30:20');
/*!40000 ALTER TABLE `prop_campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_info`
--

DROP TABLE IF EXISTS `prop_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `earning_flg` tinyint(4) NOT NULL DEFAULT '0',
  `trading_flg` tinyint(4) NOT NULL DEFAULT '0',
  `price` decimal(15,3) NOT NULL,
  `stock_num` int(11) NOT NULL,
  `earning_at` datetime DEFAULT NULL,
  `profit_type` tinyint(4) NOT NULL DEFAULT '0',
  `profit_id` int(11) NOT NULL DEFAULT '0',
  `profit_rate` decimal(3,1) DEFAULT '0.0',
  `expire` datetime DEFAULT NULL,
  `region` smallint(6) NOT NULL,
  `city` varchar(32) DEFAULT NULL,
  `postal_code` smallint(6) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `layout` varchar(32) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_info`
--

LOCK TABLES `prop_info` WRITE;
/*!40000 ALTER TABLE `prop_info` DISABLE KEYS */;
INSERT INTO `prop_info` VALUES (1,'プラウドシティ大田六郷',10,0,0,20000000.000,100,NULL,0,0,6.0,NULL,1,'東京都',32767,'大田区西六郷３-2-1',1,'4LDK',NULL,NULL,'2015-09-19 06:59:29','2015-10-02 00:33:41'),(2,'クラッシィハウス芝浦',20,1,0,30000000.000,250,NULL,0,0,6.8,NULL,1,'東京都',32767,'港区芝浦４丁目2番28',1,'4LDK',NULL,NULL,'2015-10-02 00:41:21','2015-10-02 01:05:31'),(3,'レーベン柏Crystal I’ll ',20,1,0,42580000.000,400,NULL,0,0,11.0,NULL,1,'千葉',32767,'柏市千代田１-7-1',1,'3LDK  ',NULL,NULL,'2015-10-02 01:24:03','2015-10-02 01:24:03'),(4,'インプレスト コア 浅草橋',10,0,0,45680000.000,500,NULL,0,0,10.0,NULL,1,'東京都',32767,'東京都台東区浅草橋３-29-1他',1,'3LDK',NULL,NULL,'2015-11-12 00:31:01','2015-11-12 00:32:57'),(5,'シティテラス平井',20,1,0,69800000.000,700,NULL,0,0,11.2,NULL,1,'東京都',32767,'東京都江戸川区平井４-20番2',1,'4LD・K+N',NULL,NULL,'2015-11-12 00:41:06','2015-11-12 00:41:06'),(6,'ナイスブライトピア世田谷千歳台',10,0,0,42806000.000,400,NULL,0,0,10.0,NULL,1,'東京都',32767,'東京都世田谷区千歳台３-41-2他（',1,'2LDK+S',NULL,NULL,'2015-11-12 00:49:48','2015-11-12 00:49:48'),(7,'パークシティ柏の葉キャンパス ザ・ゲートタワー',10,0,0,58790000.000,600,NULL,0,0,9.5,NULL,1,'千葉県',32767,'柏市若柴大割227-15',1,'3LDK+WIC+SI',NULL,NULL,'2015-11-12 01:57:13','2015-11-12 01:57:13');
/*!40000 ALTER TABLE `prop_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_info_lang_ja-JP`
--

DROP TABLE IF EXISTS `prop_info_lang_ja-JP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_info_lang_ja-JP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_info_lang_ja-JP`
--

LOCK TABLES `prop_info_lang_ja-JP` WRITE;
/*!40000 ALTER TABLE `prop_info_lang_ja-JP` DISABLE KEYS */;
INSERT INTO `prop_info_lang_ja-JP` VALUES (1,'プラウドシティ大田六郷','品川駅8分・東京駅15分、総敷地面積2万3000m2超のスケールで描く、水と緑と人生のフラッグシップ＜プラウドシティ大田六郷＞全632邸誕生','Point1 都心と緑を身近にし、職住近接を叶えるゆとりあるライフスタイルを提案\r\n\r\n現地周辺の航空写真※2015年3月撮影。画像加工を施しており、実際とは多少異なります。\r\n『GREEN CITY』　東京を気持ちよく生きる、多摩川の潤いに満ちたみずみずしい街\r\n例えば、出勤前にリバーサイドをジョギングする気持ち良さを。\r\n例えば、子どもたちと水辺の生き物を観察する楽しみを。\r\n東京が忘れかけていた、そんな自然体の気持ちよさをもたらしてくれるのが多摩川を身近にする住環境です。\r\nリフレッシュした後には、区内最大の雑色商店街で馴染みの店に立ち寄るのも楽しみになりそうです。\r\n\r\n※雑色商店街は大田区で最大の店舗数の商店街（雑色商店街HPより）\r\n※京急本線「雑色」駅より、「品川」駅へ8分(各停利用、京急蒲田駅で特快乗換え)、「東京」駅へ15分(品川駅で東海道線乗換え)、「羽田空港国際線ターミナル」駅へ8分(京急蒲田駅で京急空港線特快乗換え)\r\n\r\nPoint2 発展著しい東京・城南エリア。その中心となる品川駅へ8分／東京駅へ15分\r\n\r\n『GREEN FUTURE』　品川駅まで8分。明日の快適を物語る、伸び盛りの東京\r\nこれからの東京の中心地は一体どこか。\r\nその鍵を握りそうなのが、城南エリアです。\r\nリニア中央新幹線の始発駅やアジアヘッドクォーター特区に指定された品川は、東京のサウスゲートとして大注目の地。\r\n品川駅へ直結8分、さらに国際化で存在感を増す羽田空港も身近にする…将来性も利便性もあきらめない、明日の東京がプロジェクトの舞台です。\r\n\r\n※1、2：京急本線「雑色」駅より各停利用、「京急蒲田」で京急本線(特快)乗換え、「品川」で東海道本線に乗換え。※3：「羽田空港国際線ターミナル」駅へ京急本線(各停)利用、「京急蒲田」駅で京急空港線(特快)乗換え。いずれも( )内は通勤時※4：東日本旅客鉄道(株)2014年6月3日プレスリリースより。※5：東京都政策企画局HP「アジアヘッドクオーター特区」より。※6：東海旅客鉄道HP「中央新幹線(品川・名古屋間)に係る事業説明の利用について」より。※7：東日本旅客鉄道(株)HP「グループ経営構想V「今後の重点取組み事項」の進捗及び更新について」より。※8：国土交通省「平成27年度鉄道局関係予算概算要求概要」より。※9：大田区HP「新空港線「蒲蒲線」整備促進事業」より。※10：大田区HP「京急蒲田駅西口地区」より。※11：大田区HP「駅舎について」より。※12：大田区HP「羽田空港跡地を活用した産業交流拠点の形成」より。※13：殿町国際戦略拠点 キング スカイフロントHPより\r\n\r\nPoint3 敷地内に医療施設や保育園など、子育てファミリーを中心にサポートする、様々な施設とサービス\r\n\r\n『GREEN LIFE』　真の永住を語り、安心な人生を叶える、ライフソリューション＆シェア\r\n大田区の方々と野村不動産が協議を重ね、たどり着いたのは、人生の様々な問題に立ち向かうライフソリューションレジデンス。\r\n保育所や子育て支援施設、医療支援施設といった人生を支える施設が敷地内に集結。\r\nさらに「時短」・「キレイ」・「安心」をキーワードに、ソフト・ハードの両面から暮らしをサポートします。\r\n\r\n【敷地内で行う施設とサービス（予定）】\r\n◎敷地内で送迎も安心「保育所」　　　　　　　　◎プレママ期から相談できる「子育て広場」(※1)\r\n◎放課後の子どもを預かる「子育て支援施設」　　◎地域の方々との交流の場「地域公開型集会室」\r\n◎敷地内にかかりつけ医がもてる「医療施設」　　◎暮らしをサポートする「行政の相談窓口」\r\n◎日々の買物をもっと便利に「ミニショップ」\r\n\r\n※1：実施予定。実施場所は未定です。\r\n※：上記施設は入居者専用の施設ではなく、入居者の通園等が、優先・保証されるものではありません。許可保育施設・子育て支援施設・医療施設は将来に渡って保証されるものではありません。\r\n『GREEN RESIDENCE』　総敷地面積2万3000m2超、全632邸の森に寛ぐフラッグシップ\r\n23区内で供給されるマンションのうち、600戸を超える規模のものはわずか1％足らず(※2)。\r\nビッグプロジェクトだからこそ、提示できる新たな価値があります。それは、東京にいながらリゾートのような潤いを与えてくれる、豊かな四季の庭。\r\n緑を感じるラウンジやカフェ、ライブラリーといった充実の空間が、新たな寛ぎをもたらします。\r\n\r\n【共用施設・サービス】\r\n◎水と水辺を見晴らす「カフェラウンジ」　　　　　　　　　　　　◎ホテルのように落ち着いた「ラウンジ」\r\n◎青山ブックセンターセレクトで7000冊超の「ライブラリー」　 　◎ボーネルンドがプロデュースする「キッズルーム」\r\n◎中庭を見晴らすキッチン付「パーティルーム」　　　　　　　　　◎和・洋2つの空間がもてなす「ゲストルーム」\r\n◎ちょっと遠出に気軽さを「レンタサイクル＆カーシェア」\r\n\r\n※2：1993年以降、東京23区内で発売された新規分譲マンション(1万500物件)の内、総戸数600戸以上は0.58％(61物件)(MRC調べ：2015年5月現在）\r\n※共用部・共用サービスは一部有料・予約制となります。また内容は変更となる場合があります'),(2,'クラッシィハウス芝浦','駅前大規模開発計画が進行中のJR山手線・京浜東北線「田町」駅徒歩9分、都営三田線・浅草線「三田」駅徒歩11分。開放感にあふれ、暮らしやすい環境が整った港区芝浦のキャナルフロントに誕生。','Point1 JR山手線・京浜東北線「田町」駅徒歩9分、都営三田線・浅草線「三田」駅徒歩11分。港区アドレス・全邸キャナルビュー\r\n\r\n都心「港区芝浦」に全邸キャナルビューの地上20階建・123邸誕生\r\n対岸の建物まで約100m～約140mの距離があり、全住戸で開放感を味わえます\r\nJR山手線・京浜東北線「田町」駅より徒歩9分。\r\n大通りから一区画入った静かな運河沿いに地上20階建て・全123邸の〈クラッシィハウス芝浦〉が誕生します。\r\n\r\n建設地のある「港区芝浦」は、ウォーターフロントの景観と、レインボーブリッジなどの都会的な夜景が印象的な街。\r\n水辺のテラスや公園などが広がる都心住街区として進化を続けています。\r\n「JR山手線最寄り」＆「港区アドレス」という都心立地で、水辺のやすらぎを享受できるロケーションが特徴です。\r\n\r\nさらに、運河を挟んで対岸まで約100m～約140mの距離があるので、キャナルフロントの開放感を楽しむことができます。\r\n全邸キャナルビュー、70m2台・3LDK中心（※1）で、ゆとりと開放感に包まれて暮らせます。\r\n\r\n\r\nPoint2 新駅をはじめ、さまざまな開発が進む「田町」「品川」駅周辺。更なる発展が期待できるエリア\r\n\r\n現地周辺再開発エリア概念図。※現地周辺再開発エリア概念図は2014年4月に現地周辺を撮影した航空写真にCG処理したもので、実際とは多少異なる場合があります\r\n「田町」駅～「品川」駅間にJR新駅が暫定開業予定、「田町」駅東口では大規模開発が進行中\r\n東京の中でも、さまざまな開発計画が進む田町・品川エリアは、次代の東京を担う新拠点として、\r\n今後更なる発展が期待できる街といえるでしょう。\r\n\r\nその計画の一部を紹介すると…。\r\n■【田町～品川駅間JR新駅】田町駅より約1.3km、品川駅より約0.9km付近に設置予定\r\n出典元／東日本旅客鉄道株式会社、2014年6月3日プレスリリースより（暫定開業予定：2020年、遅れる場合あり）\r\n■【田町駅東口北地区土地区画整理事業】出典元／田町駅東口北地区土地区画整理事業／「港区役所ホームページ」より\r\n●（仮称）TGMM芝浦プロジェクト／約540ｍ（供用開始予定：2019年、遅れる場合あり） \r\n●保育施設／約640ｍ（現況：工事中、工事完了予定：2016年12月、遅れる場合あり）\r\n●田町駅直結の歩行者デッキ／約600ｍ \r\n●愛育病院移転整備工事／約900ｍ（完成済・2015年2月移転） \r\n●芝浦公園／約790ｍ（現況：工事中、工事完了予定：2016年度、※遅れる場合あり） \r\n●みなとパーク芝浦／約850ｍ（2014年12月開設） \r\n■【品川シーズンテラス】約970ｍ（完成済・2015年5月開業）\r\n■【リニア中央新幹線計画】出典元／東海日本旅客鉄道株式会社、「中央新幹線（東京都・名古屋市間）環境影響評価準備書」より\r\n（開業予定：2027年、遅れる場合あり）\r\n■【アジアヘッドクォーター特区】出典元／東京都知事本局 総合特区推進部、「アジアヘッドクォーター特区ホームページ」より\r\n※計画内容は将来変更する場合があります。あらかじめご承知ください。\r\n\r\nさらなる進化を遂げる地に暮らすときめき、将来性も同物件の大きな特徴といえます。\r\n\r\n\r\nPoint3 5駅利用可。「品川」直通3分、「東京」直通6分、「羽田空港国内線ターミナル」17分\r\n\r\n都心各所へスピーディにつながるアクセス。オンもオフも充実した都心生活を楽しめるロケーション\r\n徒歩9分のJR山手線・京浜東北線「田町」駅、徒歩11分の都営三田線・浅草線「三田」駅をはじめ、\r\n5駅（※2）利用可能の軽快なアクセスが魅力。\r\n\r\nJR「田町」駅からは、\r\n■「品川」駅へ直通3分（2分）…JR京浜東北線利用\r\n■「東京」駅へ直通6分（8分）…JR京浜東北線快速利用。通勤時はJR京浜東北線利用。\r\n■「汐留」駅へ3分（3分）…JR京浜東北線快速利用、「浜松町」駅／「大門」駅で都営大江戸線に乗り換え。\r\n■「六本木」駅へ8分（8分）…JR京浜東北線快速利用、「浜松町」駅／「大門」駅で都営大江戸線に乗り換え。\r\n■「渋谷」駅へ10分（11分）…JR山手線利用、「大崎」駅でJR湘南新宿ライン特別快速に乗り換え。\r\n■「新宿」駅へ15分（16分）…JR山手線利用、「大崎」駅でJR湘南新宿ライン特別快速に乗り換え。\r\n■「羽田空港国内線ターミナル」駅へ17分（21分）…JR山手線利用、「品川」駅で京急本線快特に乗り換え。\r\n※表示は日中時（（　）内は通勤時）のものです。なお乗り換え、待ち時間は含まれておりません。\r\n※掲載の情報は、2015年4月現在のものです。\r\n\r\n新幹線停車駅の「品川」駅へ直通3分・「東京」駅へ直通6分、「羽田空港国内線ターミナル」駅へも17分なので、\r\n通勤はもちろん、出張や旅行にも便利です。\r\nオンもオフも充実した都心生活を楽しめるロケーションとなっています。\r\n'),(3,'レーベン柏Crystal I’ll ','同物件の魅力。それは、駅前の便利な商業施設を徒歩で使える距離でありながら、 喧噪を感じることのない、落ち着いた暮らしが手に入ることです。','Point1 東京29分の軽快アクセス。ショッピングも子育ても「この街で完結」！\r\n\r\n「柏駅の利便性」と「住宅街の暮らしやすさ」のバランスが魅力\r\n同物件の魅力。それは、駅前の便利な商業施設を徒歩で使える距離でありながら、\r\n喧噪を感じることのない、落ち着いた暮らしが手に入ることです。\r\n\r\n駅前には「高島屋」や「そごう」から、「イトーヨーカドー」、さらに「ビックカメラ」などの大型専門店まで集結しています。\r\n様々なお買い物が可能な環境について、「買い物は柏で完結するんです」と教えてくれた住民の方も。\r\nわざわざ電車や車に乗らずとも、欲しいものが徒歩圏内で手に入るのは便利ですね。\r\n\r\nまた一方で、「利便性は欲しいけど、日々の暮らしは落ち着いた環境で送りたい」という人も多いのでは？\r\nそんな人の希望も叶えてくれるのが、同物件です。\r\n現地は駅からほどよい距離にある、穏やかな住宅街。\r\n子どもたちが毎日遊べる公園や小学校なども徒歩10分以内にしっかりそろい、安心して子育てをすることができそうです。\r\n\r\n\r\nPoint2 充実の共用施設を誇る、大規模【太陽光発電】マンション。実物見学可！\r\n\r\n太陽光の全量売電、一括受電、見える化で節電。電気代が半減！\r\n同物件の屋上には、千葉県内最大の太陽光発電量※を捻出する753枚のソーラーパネルが設置されています。「固定価格買取制度」認定物件なので買取価格も確定しており、20年間の全量売電が保証されています。\r\nこれで、ご家庭の毎月の電気代が最大約51％も削減可能。\r\n自然にも、お財布にもやさしい暮らし方が始まります。\r\n\r\n\r\nPoint3 女性の視点で設備を選択、「家も私も、いつも綺麗に」。水まわりが充実！\r\n\r\nゆったり、リラックスタイムを提供するバスルーム\r\nバスルームには、美しくありたいママ達の気持ちに寄り添う設備が満載です。\r\n◇細かな霧がお肌をしっとりさせ、発汗を促す「ミストサウナ」。水ミスト機能もついており、リフレッシュが簡単です。低温・高湿度で息苦しくなりにくいミストサウナが自宅にあれば、スパに行かなくてもキレイを保つことができそうですね。\r\n◇微細な泡が体を包み込む「マイクロバブルバス」があれば毎日のバスタイムが心地よいマッサージタイムに早変わり。疲れもすっきり取れそうです。'),(4,'インプレスト コア 浅草橋','都営浅草線「浅草橋」駅徒歩5分、JR総武線「浅草橋」駅徒歩6分、6駅10路線利用可能。「秋葉原」を生活圏に多彩な魅力を持つ「浅草橋」に、「新築」×「DIY」の新発想マンション登場!「DIY」を体験できるモデルルーム準備中!来場予約受付中!','Point1 「新築」×「DIY」自分らしい住空間をかなえる新発想マンション\r\n\r\n時間をかけて自分らしくつくりあげられる愛着のわく住まい\r\n「双日新都市開発」と「OpenA」のコラボレーション\r\n洋服や食事を選ぶように、暮らしの空間にももっと自由に「個性」を反映したい。\r\n＜インプレスト コア 浅草橋＞は、そんな願いに応えて、「新築マンション」に「DIY」の要素を取り入れた新しい発想の住まいです。\r\n壁に好きな色のペンキを塗ったり、棚をつくったり、自由に絵を飾ったり、\r\n暮らしの中で自分の個性に合わせて空間をつくりかえることができる。\r\nおしきせの既製品でなく、自ら手を入れてつくりあげていく楽しみの幅、可能性がひろがっています。\r\n最初は新鮮な天然木や漆喰の色は、時の経過とともに味わいを深め、\r\n住み続けるうちにどんどん自分にフィットしていくことでしょう。\r\n\r\n現在、同物件でかなう「DIY」を体験できるモデルルームを準備中。来場予約受付中です。\r\nいち早く新しい住まいのカタチに触れてみましょう。\r\n\r\n※完成予想図は計画段階の図面を基に描き起こしたもので、実際とは多少異なります。また、今後変更になる場合があります。\r\n一部、設計変更・オプション(いずれも有償・申込期限有)が含まれます。家具等は価格に含まれません。\r\n\r\n\r\nPoint2 都営浅草線「浅草橋」駅徒歩5分、6駅10路線利用可能！\r\n\r\n多忙な方が便利に楽しく暮らせる買い物環境。\r\nハイセンスなカフェやインテリアショップも身近\r\n現地から徒歩10分圏内には、\r\nminiピアゴ（約420m）、スーパーカエデ（約350m）、ココス ナカムラ（約550m）、\r\n肉のハナマサ（約570m）、ライフ（約630m）など、\r\n5つのスーパーが揃っています。\r\n24時間営業や深夜営業の店が多く、帰宅が遅くなっても買い物に困らない環境です。\r\n\r\nおかず横丁（約360m・徒歩5分）、佐竹商店街（約780m・徒歩10分）といった\r\n昔ながらの人情味があふれる商店街で買い物ができるのも魅力。\r\n\r\nまた、江戸通り問屋街（徒歩2分・約160ｍ）には\r\n老舗店のほか、文具、玩具、衣料、ハンドメイドの材料を扱う問屋が立ち並び、\r\n一般の方が買い物できるお店も多くあります。\r\n\r\n多彩なカルチャーの発信地「秋葉原」は約1.1kmの生活圏。\r\nものづくりをテーマにしたお店がJR高架下に集積する「2k540 AKI-OKA ARTISAN」（約1230m・徒歩16分）など、\r\n近年は周辺にオフを充実させてくれる高感度なスポットが特に増えてきています。\r\n\r\n\r\n'),(5,'シティテラス平井',' 天空に向かって大きく翼を広げたような象徴的な外観。誰もがその規模感に息を飲み、天を仰ぎ見る圧倒的なスケール。住まう人の誇りとなる、壮観な光景となる象徴がここに誕生します','【建物・施設】 \r\n\r\n都心にほど近い開放の地。東京23区内にありながら、豊かな自然に恵まれ、都会の喧騒を忘れさせてくれます。\r\n\r\n【リビング・ダイニング】\r\n\r\nバルコニー側の大きな窓からたっぷりと光が射し込む、明るく開放的なリビング･ダイニング。\r\n\r\n【キッチン】\r\n\r\n奥行き最大約300mmの人工大理石対面カウンタートップを採用。シンクとカウンターの間に段差があるので水が飛び散りにくくなります。また、対面タイプなので、家事をしながらLDにいる家族や友人達とのだんらんも楽しめます。\r\n\r\n【バスルーム】\r\n\r\n浴槽へのまたぎ高を約450mmに抑え、出入口の段差も極力解消した、低床設計のユニットバスを採用。さらに、壁には手すりを設置。手すりにつかまりながら浴槽への出入りができ高齢化社会に対応した、人にやさしい仕様です。浴槽のふちが広いので入浴時に腰掛けて入れます。'),(6,'ナイスブライトピア世田谷千歳台','閑静な住宅街「世田谷区・千歳台」。広大な公園や、教育施設、便利な買物施設も多彩に揃い子育てファミリーに嬉しい環境。耐震強度1.25倍の強耐震構造マンション。床暖房を標準装備。','【システムキッチン】\r\nリビング・ダイニングと隣り合う開放的なキッチンは、住まいの顔といえる空間。心地よく過ごすためのクオリティを大切にしました。すっきりとした空間を実現する為に欠かせない収納スペースも豊富に用意しています。\r\n\r\n【省エネ給湯システム自動運転のオートバス】\r\n一日の疲れを癒すリラクゼーションスペースとしても重要な浴室。ゆったりとした時間を快適に過ごしていただけるよう、機能性と清潔感に配慮しました。清潔感溢れる浴槽には、便利な追焚機能付き（循環式）オートバスを採用。お湯はりから保温までをワンタッチで操作でき、お料理の合間にも簡単に入浴の準備ができます。\r\n\r\n【セキュリティシステム】\r\nセコムマンションセキュリティシステムを導入。セキュリティのプロが住戸内の各種センサーや非常ボタンを24時間監視し、万一異常が発生した場合には現場へ急行。必要に応じて警察、消防機関などにも通報し、迅速で適切な対応をいたします。\r\n\r\n【サムターン回し対策】\r\nサムターン中央にあるスイッチを摘みながら解錠するスイッチ式サムターンを採用。ドアに穴を開け、専用工具を差し込むサムターン回しの解錠が困難な仕様になっています。'),(7,'パークシティ柏の葉キャンパス ザ・ゲートタワー','「柏の葉キャンパス」駅前では、「公」「学」、そして「民」である三井不動産グループが主体となり、住む×学ぶ×働く×交わる×遊ぶ×憩うのすべてが一体となった新しい街づくりが進行中です。','Point1 三井不動産グループによる商業一体型駅前大規模複合開発\r\n\r\n公・民・学が連携して新しい街づくりを邁進する柏の葉キャンパス\r\n「柏の葉キャンパス」駅前では、「公」「学」、そして「民」である三井不動産グループが主体となり、住む×学ぶ×働く×交わる×遊ぶ×憩うのすべてが一体となった新しい街づくりが進行中です。三井不動産グループは長年培ってきた総合ディベロッパーとしての経験を最大限に活かし、分譲住宅、オフィス、ホテル、商業施設などの施設を結びつけることで新しい街を創造しました。\r\n\r\n\r\nPoint2 駅徒歩3分＆免震タワー。タワーならではの共用施設も魅力\r\n\r\n安らぎを大切にしたスタイリッシュなデザイン\r\n駅徒歩わずか3分、駅前の利便性を享受する贅沢な地に誕生する〈パークシティ柏の葉キャンパス ザ・ゲートタワー〉は超高層36階建て＆免震タワーレジデンス。緑に映える凛とした佇まいの中にも、住まいとしての温もりが感じられる同マンションのエントランス。駅から続く都市空間から、気持ちが安らぐデザインです。\r\n\r\n\r\nPoint3 柏の葉キャンパスは、街ぐるみで子育てをサポートする街\r\n\r\n子どもたちにお仕事体験の場を提供する「ピノキオプロジェクト」\r\n柏の葉キャンパスでは街づくりの一環として、地域連携型の子育てプログラムがあります。たとえば「ピノキオプロジェクト」は、ピノキオのコスチュームを着た子どもたちが地元の花屋やカフェ、銀行などの店舗でお仕事体験ができるもの。仕事の後には、仮想通貨「Pi」が給料として支給され、地域の協力店舗で使用することができます。働くことの楽しさや意義を伝える場を作っています。');
/*!40000 ALTER TABLE `prop_info_lang_ja-JP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_info_lang_zh-CN`
--

DROP TABLE IF EXISTS `prop_info_lang_zh-CN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_info_lang_zh-CN` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_info_lang_zh-CN`
--

LOCK TABLES `prop_info_lang_zh-CN` WRITE;
/*!40000 ALTER TABLE `prop_info_lang_zh-CN` DISABLE KEYS */;
INSERT INTO `prop_info_lang_zh-CN` VALUES (1,'プラウドシティ大田六郷','品川駅8分・東京駅15分、総敷地面積2万3000m2超のスケールで描く、水と緑と人生のフラッグシップ＜プラウドシティ大田六郷＞全632邸誕生','Point1 都心と緑を身近にし、職住近接を叶えるゆとりあるライフスタイルを提案\r\n\r\n現地周辺の航空写真※2015年3月撮影。画像加工を施しており、実際とは多少異なります。\r\n『GREEN CITY』　東京を気持ちよく生きる、多摩川の潤いに満ちたみずみずしい街\r\n例えば、出勤前にリバーサイドをジョギングする気持ち良さを。\r\n例えば、子どもたちと水辺の生き物を観察する楽しみを。\r\n東京が忘れかけていた、そんな自然体の気持ちよさをもたらしてくれるのが多摩川を身近にする住環境です。\r\nリフレッシュした後には、区内最大の雑色商店街で馴染みの店に立ち寄るのも楽しみになりそうです。\r\n\r\n※雑色商店街は大田区で最大の店舗数の商店街（雑色商店街HPより）\r\n※京急本線「雑色」駅より、「品川」駅へ8分(各停利用、京急蒲田駅で特快乗換え)、「東京」駅へ15分(品川駅で東海道線乗換え)、「羽田空港国際線ターミナル」駅へ8分(京急蒲田駅で京急空港線特快乗換え)\r\n\r\nPoint2 発展著しい東京・城南エリア。その中心となる品川駅へ8分／東京駅へ15分\r\n\r\n『GREEN FUTURE』　品川駅まで8分。明日の快適を物語る、伸び盛りの東京\r\nこれからの東京の中心地は一体どこか。\r\nその鍵を握りそうなのが、城南エリアです。\r\nリニア中央新幹線の始発駅やアジアヘッドクォーター特区に指定された品川は、東京のサウスゲートとして大注目の地。\r\n品川駅へ直結8分、さらに国際化で存在感を増す羽田空港も身近にする…将来性も利便性もあきらめない、明日の東京がプロジェクトの舞台です。\r\n\r\n※1、2：京急本線「雑色」駅より各停利用、「京急蒲田」で京急本線(特快)乗換え、「品川」で東海道本線に乗換え。※3：「羽田空港国際線ターミナル」駅へ京急本線(各停)利用、「京急蒲田」駅で京急空港線(特快)乗換え。いずれも( )内は通勤時※4：東日本旅客鉄道(株)2014年6月3日プレスリリースより。※5：東京都政策企画局HP「アジアヘッドクオーター特区」より。※6：東海旅客鉄道HP「中央新幹線(品川・名古屋間)に係る事業説明の利用について」より。※7：東日本旅客鉄道(株)HP「グループ経営構想V「今後の重点取組み事項」の進捗及び更新について」より。※8：国土交通省「平成27年度鉄道局関係予算概算要求概要」より。※9：大田区HP「新空港線「蒲蒲線」整備促進事業」より。※10：大田区HP「京急蒲田駅西口地区」より。※11：大田区HP「駅舎について」より。※12：大田区HP「羽田空港跡地を活用した産業交流拠点の形成」より。※13：殿町国際戦略拠点 キング スカイフロントHPより\r\n\r\nPoint3 敷地内に医療施設や保育園など、子育てファミリーを中心にサポートする、様々な施設とサービス\r\n\r\n『GREEN LIFE』　真の永住を語り、安心な人生を叶える、ライフソリューション＆シェア\r\n大田区の方々と野村不動産が協議を重ね、たどり着いたのは、人生の様々な問題に立ち向かうライフソリューションレジデンス。\r\n保育所や子育て支援施設、医療支援施設といった人生を支える施設が敷地内に集結。\r\nさらに「時短」・「キレイ」・「安心」をキーワードに、ソフト・ハードの両面から暮らしをサポートします。\r\n\r\n【敷地内で行う施設とサービス（予定）】\r\n◎敷地内で送迎も安心「保育所」　　　　　　　　◎プレママ期から相談できる「子育て広場」(※1)\r\n◎放課後の子どもを預かる「子育て支援施設」　　◎地域の方々との交流の場「地域公開型集会室」\r\n◎敷地内にかかりつけ医がもてる「医療施設」　　◎暮らしをサポートする「行政の相談窓口」\r\n◎日々の買物をもっと便利に「ミニショップ」\r\n\r\n※1：実施予定。実施場所は未定です。\r\n※：上記施設は入居者専用の施設ではなく、入居者の通園等が、優先・保証されるものではありません。許可保育施設・子育て支援施設・医療施設は将来に渡って保証されるものではありません。\r\n『GREEN RESIDENCE』　総敷地面積2万3000m2超、全632邸の森に寛ぐフラッグシップ\r\n23区内で供給されるマンションのうち、600戸を超える規模のものはわずか1％足らず(※2)。\r\nビッグプロジェクトだからこそ、提示できる新たな価値があります。それは、東京にいながらリゾートのような潤いを与えてくれる、豊かな四季の庭。\r\n緑を感じるラウンジやカフェ、ライブラリーといった充実の空間が、新たな寛ぎをもたらします。\r\n\r\n【共用施設・サービス】\r\n◎水と水辺を見晴らす「カフェラウンジ」　　　　　　　　　　　　◎ホテルのように落ち着いた「ラウンジ」\r\n◎青山ブックセンターセレクトで7000冊超の「ライブラリー」　 　◎ボーネルンドがプロデュースする「キッズルーム」\r\n◎中庭を見晴らすキッチン付「パーティルーム」　　　　　　　　　◎和・洋2つの空間がもてなす「ゲストルーム」\r\n◎ちょっと遠出に気軽さを「レンタサイクル＆カーシェア」\r\n\r\n※2：1993年以降、東京23区内で発売された新規分譲マンション(1万500物件)の内、総戸数600戸以上は0.58％(61物件)(MRC調べ：2015年5月現在）\r\n※共用部・共用サービスは一部有料・予約制となります。また内容は変更となる場合があります'),(2,'クラッシィハウス芝浦','駅前大規模開発計画が進行中のJR山手線・京浜東北線「田町」駅徒歩9分、都営三田線・浅草線「三田」駅徒歩11分。開放感にあふれ、暮らしやすい環境が整った港区芝浦のキャナルフロントに誕生。','Point1 JR山手線・京浜東北線「田町」駅徒歩9分、都営三田線・浅草線「三田」駅徒歩11分。港区アドレス・全邸キャナルビュー\r\n\r\n都心「港区芝浦」に全邸キャナルビューの地上20階建・123邸誕生\r\n対岸の建物まで約100m～約140mの距離があり、全住戸で開放感を味わえます\r\nJR山手線・京浜東北線「田町」駅より徒歩9分。\r\n大通りから一区画入った静かな運河沿いに地上20階建て・全123邸の〈クラッシィハウス芝浦〉が誕生します。\r\n\r\n建設地のある「港区芝浦」は、ウォーターフロントの景観と、レインボーブリッジなどの都会的な夜景が印象的な街。\r\n水辺のテラスや公園などが広がる都心住街区として進化を続けています。\r\n「JR山手線最寄り」＆「港区アドレス」という都心立地で、水辺のやすらぎを享受できるロケーションが特徴です。\r\n\r\nさらに、運河を挟んで対岸まで約100m～約140mの距離があるので、キャナルフロントの開放感を楽しむことができます。\r\n全邸キャナルビュー、70m2台・3LDK中心（※1）で、ゆとりと開放感に包まれて暮らせます。\r\n\r\n\r\nPoint2 新駅をはじめ、さまざまな開発が進む「田町」「品川」駅周辺。更なる発展が期待できるエリア\r\n\r\n現地周辺再開発エリア概念図。※現地周辺再開発エリア概念図は2014年4月に現地周辺を撮影した航空写真にCG処理したもので、実際とは多少異なる場合があります\r\n「田町」駅～「品川」駅間にJR新駅が暫定開業予定、「田町」駅東口では大規模開発が進行中\r\n東京の中でも、さまざまな開発計画が進む田町・品川エリアは、次代の東京を担う新拠点として、\r\n今後更なる発展が期待できる街といえるでしょう。\r\n\r\nその計画の一部を紹介すると…。\r\n■【田町～品川駅間JR新駅】田町駅より約1.3km、品川駅より約0.9km付近に設置予定\r\n出典元／東日本旅客鉄道株式会社、2014年6月3日プレスリリースより（暫定開業予定：2020年、遅れる場合あり）\r\n■【田町駅東口北地区土地区画整理事業】出典元／田町駅東口北地区土地区画整理事業／「港区役所ホームページ」より\r\n●（仮称）TGMM芝浦プロジェクト／約540ｍ（供用開始予定：2019年、遅れる場合あり） \r\n●保育施設／約640ｍ（現況：工事中、工事完了予定：2016年12月、遅れる場合あり）\r\n●田町駅直結の歩行者デッキ／約600ｍ \r\n●愛育病院移転整備工事／約900ｍ（完成済・2015年2月移転） \r\n●芝浦公園／約790ｍ（現況：工事中、工事完了予定：2016年度、※遅れる場合あり） \r\n●みなとパーク芝浦／約850ｍ（2014年12月開設） \r\n■【品川シーズンテラス】約970ｍ（完成済・2015年5月開業）\r\n■【リニア中央新幹線計画】出典元／東海日本旅客鉄道株式会社、「中央新幹線（東京都・名古屋市間）環境影響評価準備書」より\r\n（開業予定：2027年、遅れる場合あり）\r\n■【アジアヘッドクォーター特区】出典元／東京都知事本局 総合特区推進部、「アジアヘッドクォーター特区ホームページ」より\r\n※計画内容は将来変更する場合があります。あらかじめご承知ください。\r\n\r\nさらなる進化を遂げる地に暮らすときめき、将来性も同物件の大きな特徴といえます。\r\n\r\n\r\nPoint3 5駅利用可。「品川」直通3分、「東京」直通6分、「羽田空港国内線ターミナル」17分\r\n\r\n都心各所へスピーディにつながるアクセス。オンもオフも充実した都心生活を楽しめるロケーション\r\n徒歩9分のJR山手線・京浜東北線「田町」駅、徒歩11分の都営三田線・浅草線「三田」駅をはじめ、\r\n5駅（※2）利用可能の軽快なアクセスが魅力。\r\n\r\nJR「田町」駅からは、\r\n■「品川」駅へ直通3分（2分）…JR京浜東北線利用\r\n■「東京」駅へ直通6分（8分）…JR京浜東北線快速利用。通勤時はJR京浜東北線利用。\r\n■「汐留」駅へ3分（3分）…JR京浜東北線快速利用、「浜松町」駅／「大門」駅で都営大江戸線に乗り換え。\r\n■「六本木」駅へ8分（8分）…JR京浜東北線快速利用、「浜松町」駅／「大門」駅で都営大江戸線に乗り換え。\r\n■「渋谷」駅へ10分（11分）…JR山手線利用、「大崎」駅でJR湘南新宿ライン特別快速に乗り換え。\r\n■「新宿」駅へ15分（16分）…JR山手線利用、「大崎」駅でJR湘南新宿ライン特別快速に乗り換え。\r\n■「羽田空港国内線ターミナル」駅へ17分（21分）…JR山手線利用、「品川」駅で京急本線快特に乗り換え。\r\n※表示は日中時（（　）内は通勤時）のものです。なお乗り換え、待ち時間は含まれておりません。\r\n※掲載の情報は、2015年4月現在のものです。\r\n\r\n新幹線停車駅の「品川」駅へ直通3分・「東京」駅へ直通6分、「羽田空港国内線ターミナル」駅へも17分なので、\r\n通勤はもちろん、出張や旅行にも便利です。\r\nオンもオフも充実した都心生活を楽しめるロケーションとなっています。\r\n'),(3,'レーベン柏Crystal I’ll ','同物件の魅力。それは、駅前の便利な商業施設を徒歩で使える距離でありながら、 喧噪を感じることのない、落ち着いた暮らしが手に入ることです。','Point1 東京29分の軽快アクセス。ショッピングも子育ても「この街で完結」！\r\n\r\n「柏駅の利便性」と「住宅街の暮らしやすさ」のバランスが魅力\r\n同物件の魅力。それは、駅前の便利な商業施設を徒歩で使える距離でありながら、\r\n喧噪を感じることのない、落ち着いた暮らしが手に入ることです。\r\n\r\n駅前には「高島屋」や「そごう」から、「イトーヨーカドー」、さらに「ビックカメラ」などの大型専門店まで集結しています。\r\n様々なお買い物が可能な環境について、「買い物は柏で完結するんです」と教えてくれた住民の方も。\r\nわざわざ電車や車に乗らずとも、欲しいものが徒歩圏内で手に入るのは便利ですね。\r\n\r\nまた一方で、「利便性は欲しいけど、日々の暮らしは落ち着いた環境で送りたい」という人も多いのでは？\r\nそんな人の希望も叶えてくれるのが、同物件です。\r\n現地は駅からほどよい距離にある、穏やかな住宅街。\r\n子どもたちが毎日遊べる公園や小学校なども徒歩10分以内にしっかりそろい、安心して子育てをすることができそうです。\r\n\r\n\r\nPoint2 充実の共用施設を誇る、大規模【太陽光発電】マンション。実物見学可！\r\n\r\n太陽光の全量売電、一括受電、見える化で節電。電気代が半減！\r\n同物件の屋上には、千葉県内最大の太陽光発電量※を捻出する753枚のソーラーパネルが設置されています。「固定価格買取制度」認定物件なので買取価格も確定しており、20年間の全量売電が保証されています。\r\nこれで、ご家庭の毎月の電気代が最大約51％も削減可能。\r\n自然にも、お財布にもやさしい暮らし方が始まります。\r\n\r\n\r\nPoint3 女性の視点で設備を選択、「家も私も、いつも綺麗に」。水まわりが充実！\r\n\r\nゆったり、リラックスタイムを提供するバスルーム\r\nバスルームには、美しくありたいママ達の気持ちに寄り添う設備が満載です。\r\n◇細かな霧がお肌をしっとりさせ、発汗を促す「ミストサウナ」。水ミスト機能もついており、リフレッシュが簡単です。低温・高湿度で息苦しくなりにくいミストサウナが自宅にあれば、スパに行かなくてもキレイを保つことができそうですね。\r\n◇微細な泡が体を包み込む「マイクロバブルバス」があれば毎日のバスタイムが心地よいマッサージタイムに早変わり。疲れもすっきり取れそうです。'),(4,'インプレスト コア 浅草橋','都営浅草線「浅草橋」駅徒歩5分、JR総武線「浅草橋」駅徒歩6分、6駅10路線利用可能。「秋葉原」を生活圏に多彩な魅力を持つ「浅草橋」に、「新築」×「DIY」の新発想マンション登場!「DIY」を体験できるモデルルーム準備中!来場予約受付中!','Point1 「新築」×「DIY」自分らしい住空間をかなえる新発想マンション\r\n\r\n時間をかけて自分らしくつくりあげられる愛着のわく住まい\r\n「双日新都市開発」と「OpenA」のコラボレーション\r\n洋服や食事を選ぶように、暮らしの空間にももっと自由に「個性」を反映したい。\r\n＜インプレスト コア 浅草橋＞は、そんな願いに応えて、「新築マンション」に「DIY」の要素を取り入れた新しい発想の住まいです。\r\n壁に好きな色のペンキを塗ったり、棚をつくったり、自由に絵を飾ったり、\r\n暮らしの中で自分の個性に合わせて空間をつくりかえることができる。\r\nおしきせの既製品でなく、自ら手を入れてつくりあげていく楽しみの幅、可能性がひろがっています。\r\n最初は新鮮な天然木や漆喰の色は、時の経過とともに味わいを深め、\r\n住み続けるうちにどんどん自分にフィットしていくことでしょう。\r\n\r\n現在、同物件でかなう「DIY」を体験できるモデルルームを準備中。来場予約受付中です。\r\nいち早く新しい住まいのカタチに触れてみましょう。\r\n\r\n※完成予想図は計画段階の図面を基に描き起こしたもので、実際とは多少異なります。また、今後変更になる場合があります。\r\n一部、設計変更・オプション(いずれも有償・申込期限有)が含まれます。家具等は価格に含まれません。\r\n\r\n\r\nPoint2 都営浅草線「浅草橋」駅徒歩5分、6駅10路線利用可能！\r\n\r\n多忙な方が便利に楽しく暮らせる買い物環境。\r\nハイセンスなカフェやインテリアショップも身近\r\n現地から徒歩10分圏内には、\r\nminiピアゴ（約420m）、スーパーカエデ（約350m）、ココス ナカムラ（約550m）、\r\n肉のハナマサ（約570m）、ライフ（約630m）など、\r\n5つのスーパーが揃っています。\r\n24時間営業や深夜営業の店が多く、帰宅が遅くなっても買い物に困らない環境です。\r\n\r\nおかず横丁（約360m・徒歩5分）、佐竹商店街（約780m・徒歩10分）といった\r\n昔ながらの人情味があふれる商店街で買い物ができるのも魅力。\r\n\r\nまた、江戸通り問屋街（徒歩2分・約160ｍ）には\r\n老舗店のほか、文具、玩具、衣料、ハンドメイドの材料を扱う問屋が立ち並び、\r\n一般の方が買い物できるお店も多くあります。\r\n\r\n多彩なカルチャーの発信地「秋葉原」は約1.1kmの生活圏。\r\nものづくりをテーマにしたお店がJR高架下に集積する「2k540 AKI-OKA ARTISAN」（約1230m・徒歩16分）など、\r\n近年は周辺にオフを充実させてくれる高感度なスポットが特に増えてきています。\r\n\r\n\r\n'),(5,'シティテラス平井',' 天空に向かって大きく翼を広げたような象徴的な外観。誰もがその規模感に息を飲み、天を仰ぎ見る圧倒的なスケール。住まう人の誇りとなる、壮観な光景となる象徴がここに誕生します','【建物・施設】 \r\n\r\n都心にほど近い開放の地。東京23区内にありながら、豊かな自然に恵まれ、都会の喧騒を忘れさせてくれます。\r\n\r\n【リビング・ダイニング】\r\n\r\nバルコニー側の大きな窓からたっぷりと光が射し込む、明るく開放的なリビング･ダイニング。\r\n\r\n【キッチン】\r\n\r\n奥行き最大約300mmの人工大理石対面カウンタートップを採用。シンクとカウンターの間に段差があるので水が飛び散りにくくなります。また、対面タイプなので、家事をしながらLDにいる家族や友人達とのだんらんも楽しめます。\r\n\r\n【バスルーム】\r\n\r\n浴槽へのまたぎ高を約450mmに抑え、出入口の段差も極力解消した、低床設計のユニットバスを採用。さらに、壁には手すりを設置。手すりにつかまりながら浴槽への出入りができ高齢化社会に対応した、人にやさしい仕様です。浴槽のふちが広いので入浴時に腰掛けて入れます。'),(6,'ナイスブライトピア世田谷千歳台','閑静な住宅街「世田谷区・千歳台」。広大な公園や、教育施設、便利な買物施設も多彩に揃い子育てファミリーに嬉しい環境。耐震強度1.25倍の強耐震構造マンション。床暖房を標準装備。','【システムキッチン】\r\nリビング・ダイニングと隣り合う開放的なキッチンは、住まいの顔といえる空間。心地よく過ごすためのクオリティを大切にしました。すっきりとした空間を実現する為に欠かせない収納スペースも豊富に用意しています。\r\n\r\n【省エネ給湯システム自動運転のオートバス】\r\n一日の疲れを癒すリラクゼーションスペースとしても重要な浴室。ゆったりとした時間を快適に過ごしていただけるよう、機能性と清潔感に配慮しました。清潔感溢れる浴槽には、便利な追焚機能付き（循環式）オートバスを採用。お湯はりから保温までをワンタッチで操作でき、お料理の合間にも簡単に入浴の準備ができます。\r\n\r\n【セキュリティシステム】\r\nセコムマンションセキュリティシステムを導入。セキュリティのプロが住戸内の各種センサーや非常ボタンを24時間監視し、万一異常が発生した場合には現場へ急行。必要に応じて警察、消防機関などにも通報し、迅速で適切な対応をいたします。\r\n\r\n【サムターン回し対策】\r\nサムターン中央にあるスイッチを摘みながら解錠するスイッチ式サムターンを採用。ドアに穴を開け、専用工具を差し込むサムターン回しの解錠が困難な仕様になっています。'),(7,'パークシティ柏の葉キャンパス ザ・ゲートタワー','「柏の葉キャンパス」駅前では、「公」「学」、そして「民」である三井不動産グループが主体となり、住む×学ぶ×働く×交わる×遊ぶ×憩うのすべてが一体となった新しい街づくりが進行中です。','Point1 三井不動産グループによる商業一体型駅前大規模複合開発\r\n\r\n公・民・学が連携して新しい街づくりを邁進する柏の葉キャンパス\r\n「柏の葉キャンパス」駅前では、「公」「学」、そして「民」である三井不動産グループが主体となり、住む×学ぶ×働く×交わる×遊ぶ×憩うのすべてが一体となった新しい街づくりが進行中です。三井不動産グループは長年培ってきた総合ディベロッパーとしての経験を最大限に活かし、分譲住宅、オフィス、ホテル、商業施設などの施設を結びつけることで新しい街を創造しました。\r\n\r\n\r\nPoint2 駅徒歩3分＆免震タワー。タワーならではの共用施設も魅力\r\n\r\n安らぎを大切にしたスタイリッシュなデザイン\r\n駅徒歩わずか3分、駅前の利便性を享受する贅沢な地に誕生する〈パークシティ柏の葉キャンパス ザ・ゲートタワー〉は超高層36階建て＆免震タワーレジデンス。緑に映える凛とした佇まいの中にも、住まいとしての温もりが感じられる同マンションのエントランス。駅から続く都市空間から、気持ちが安らぐデザインです。\r\n\r\n\r\nPoint3 柏の葉キャンパスは、街ぐるみで子育てをサポートする街\r\n\r\n子どもたちにお仕事体験の場を提供する「ピノキオプロジェクト」\r\n柏の葉キャンパスでは街づくりの一環として、地域連携型の子育てプログラムがあります。たとえば「ピノキオプロジェクト」は、ピノキオのコスチュームを着た子どもたちが地元の花屋やカフェ、銀行などの店舗でお仕事体験ができるもの。仕事の後には、仮想通貨「Pi」が給料として支給され、地域の協力店舗で使用することができます。働くことの楽しさや意義を伝える場を作っています。');
/*!40000 ALTER TABLE `prop_info_lang_zh-CN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_profit_his`
--

DROP TABLE IF EXISTS `prop_profit_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_profit_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `profit_type` smallint(6) NOT NULL,
  `receipt_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(15,3) NOT NULL,
  `shared_at` datetime DEFAULT NULL,
  `user_cnt` int(11) NOT NULL DEFAULT '0',
  `shared_amount` decimal(15,3) DEFAULT '0.000',
  `shared_fee` decimal(15,3) DEFAULT '0.000',
  `shared_tax` decimal(15,3) DEFAULT '0.000',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prop_id` (`prop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_profit_his`
--

LOCK TABLES `prop_profit_his` WRITE;
/*!40000 ALTER TABLE `prop_profit_his` DISABLE KEYS */;
INSERT INTO `prop_profit_his` VALUES (1,3,2,0,50000.000,'2015-12-08 19:53:05',2,23275.000,475.000,0.000,'2015-09-10 00:00:00','2015-12-08 19:53:05'),(2,3,2,0,60000.000,'2015-12-08 19:53:05',2,27930.000,570.000,0.000,'2015-09-15 00:00:00','2015-12-08 19:53:05'),(3,3,2,0,50000.000,'2015-12-08 19:53:05',2,23275.000,475.000,0.000,'2015-09-20 00:00:00','2015-12-08 19:53:05'),(4,3,2,0,70000.000,'2015-12-08 19:53:05',2,32585.000,665.000,0.000,'2015-09-30 00:00:00','2015-12-08 19:53:05'),(5,3,2,0,56000.000,'2015-12-08 19:53:05',2,26068.000,532.000,0.000,'2015-10-04 00:00:00','2015-12-08 19:53:05'),(6,3,2,0,72000.000,'2015-12-08 19:53:05',2,33516.000,684.000,0.000,'2015-10-07 00:00:00','2015-12-08 19:53:05'),(7,2,2,0,80000.000,'2015-12-08 19:53:05',1,25088.000,512.000,0.000,'2015-10-09 00:00:00','2015-12-08 19:53:05'),(8,3,2,0,58000.000,'2015-12-08 19:53:05',2,26999.000,551.000,0.000,'2015-10-12 00:00:00','2015-12-08 19:53:05'),(9,3,2,0,63000.000,'2015-12-08 19:53:05',2,29326.500,598.500,0.000,'2015-10-18 00:00:00','2015-12-08 19:53:05'),(10,3,2,0,71000.000,'2015-12-08 19:53:05',2,33050.500,674.500,0.000,'2015-10-25 00:00:00','2015-12-08 19:53:05'),(11,3,2,0,68000.000,'2015-12-08 19:53:05',2,31654.000,646.000,0.000,'2015-10-30 00:00:00','2015-12-08 19:53:05'),(12,2,2,0,90000.000,'2015-12-08 19:53:05',1,28224.000,576.000,0.000,'2015-10-30 12:00:00','2015-12-08 19:53:05'),(13,2,2,0,45600.000,'2015-12-08 19:51:46',1,14300.160,291.840,0.000,'2015-08-10 00:00:00','2015-12-08 19:51:46'),(14,2,2,0,54000.000,'2015-12-08 19:51:46',1,16934.400,345.600,0.000,'2015-08-15 00:00:00','2015-12-08 19:51:46'),(15,2,2,0,62000.000,'2015-12-08 19:53:05',1,19443.200,396.800,0.000,'2015-08-25 00:00:00','2015-12-08 19:53:05'),(16,5,2,0,95000.000,'2015-12-08 19:51:46',0,0.000,0.000,0.000,'2015-08-09 00:00:00','2015-12-08 19:51:46'),(17,5,2,0,110000.000,'2015-12-08 19:51:46',0,0.000,0.000,0.000,'2015-08-17 00:00:00','2015-12-08 19:51:46'),(18,5,2,0,78600.000,'2015-12-08 19:51:46',0,0.000,0.000,0.000,'2015-08-21 00:00:00','2015-12-08 19:51:46'),(19,5,2,0,85000.000,'2015-12-08 19:53:05',0,0.000,0.000,0.000,'2015-08-26 00:00:00','2015-12-08 19:53:05'),(20,5,2,0,64000.000,'2015-12-08 19:53:05',0,0.000,0.000,0.000,'2015-08-29 00:00:00','2015-12-08 19:53:05'),(21,3,2,0,78000.000,'2015-12-16 20:11:07',2,38220.000,780.000,0.000,'2015-12-16 00:00:00','2015-12-16 20:11:07'),(22,2,2,0,82000.000,'2015-12-16 20:19:03',1,25715.200,524.800,0.000,'2015-12-17 00:00:00','2015-12-16 20:19:03'),(23,5,2,0,92345.000,'2015-12-21 21:36:24',1,5406.074,2.704,0.000,'2015-12-21 21:36:24','2015-12-21 21:36:24'),(24,1,2,0,120500.000,'2015-12-21 22:18:25',2,90329.813,45.188,0.000,'2015-12-21 22:18:25','2015-12-21 22:18:25'),(26,3,2,0,120500.000,'2015-12-21 22:43:34',2,60219.875,30.125,0.000,'2015-12-21 22:43:34','2015-12-21 22:43:34');
/*!40000 ALTER TABLE `prop_profit_his` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_share_his`
--

DROP TABLE IF EXISTS `prop_share_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_share_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `amount` decimal(15,3) NOT NULL,
  `profit_cnt` int(11) NOT NULL,
  `user_cnt` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prop_id` (`prop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_share_his`
--

LOCK TABLES `prop_share_his` WRITE;
/*!40000 ALTER TABLE `prop_share_his` DISABLE KEYS */;
/*!40000 ALTER TABLE `prop_share_his` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_stock_campaign`
--

DROP TABLE IF EXISTS `prop_stock_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_stock_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `detail_id` int(11) NOT NULL,
  `camp_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_id` (`stock_id`),
  KEY `detail_id` (`detail_id`),
  KEY `camp_id` (`camp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_stock_campaign`
--

LOCK TABLES `prop_stock_campaign` WRITE;
/*!40000 ALTER TABLE `prop_stock_campaign` DISABLE KEYS */;
INSERT INTO `prop_stock_campaign` VALUES (1,11,23,1,'2015-12-19 11:48:07','2015-12-19 11:48:07'),(2,11,23,2,'2015-12-19 11:48:07','2015-12-19 11:48:07'),(5,11,28,1,'2015-12-19 15:24:59','2015-12-19 15:24:59'),(6,11,28,2,'2015-12-19 15:24:59','2015-12-19 15:24:59');
/*!40000 ALTER TABLE `prop_stock_campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_stock_detail`
--

DROP TABLE IF EXISTS `prop_stock_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_stock_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `stock_cnt` int(11) NOT NULL,
  `buy_price` decimal(15,3) NOT NULL,
  `onsale_cnt` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_stock_detail`
--

LOCK TABLES `prop_stock_detail` WRITE;
/*!40000 ALTER TABLE `prop_stock_detail` DISABLE KEYS */;
INSERT INTO `prop_stock_detail` VALUES (1,1,25,0.000,25,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(2,4,149,0.000,149,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(3,6,200,0.000,200,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(4,12,500,0.000,500,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(5,13,659,0.000,659,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(8,2,55,200000.000,0,'2015-09-29 00:45:03','2015-09-29 00:45:03'),(9,3,20,200000.000,0,'2015-09-30 22:36:06','2015-09-30 22:36:06'),(10,5,72,120000.000,44,'2015-10-02 01:10:52','2015-12-19 15:35:21'),(11,11,40,106450.000,0,'2015-11-07 12:00:55','2015-11-07 12:00:55'),(12,9,58,106450.000,0,'2015-11-01 03:45:53','2015-12-19 15:09:07'),(14,11,20,106100.000,0,'2015-11-07 12:00:55','2015-11-07 12:00:55'),(15,9,80,106100.000,0,'2015-11-05 03:45:53','2015-12-19 15:09:07'),(16,18,41,99714.286,0,'2015-12-10 16:30:05','2015-12-10 16:30:05'),(17,14,390,0.000,390,'2015-11-12 01:00:55','2015-11-12 01:00:55'),(18,15,10,107015.000,0,'2015-11-12 01:37:19','2015-11-12 01:37:19'),(21,5,21,60000.000,0,'2015-12-19 09:16:04','2015-12-19 09:16:04'),(22,5,1,120000.000,0,'2015-12-19 11:45:38','2015-12-19 11:45:38'),(23,11,1,106200.000,0,'2015-12-19 11:48:07','2015-12-19 11:48:07'),(26,22,1,50000.000,0,'2015-12-19 14:57:26','2015-12-19 14:57:26'),(28,11,1,70000.000,0,'2015-12-19 15:24:59','2015-12-19 15:24:59'),(29,22,1,50000.000,0,'2015-12-19 15:36:49','2015-12-19 15:36:49'),(30,22,1,50000.000,0,'2015-12-19 16:20:51','2015-12-19 16:20:51'),(31,23,600,0.000,600,'2015-11-12 01:57:13','2015-11-12 01:57:13'),(33,22,1,49999.000,0,'2015-12-21 12:36:29','2015-12-21 12:36:29'),(35,22,1,50000.000,0,'2015-12-23 15:33:11','2015-12-23 15:33:11'),(36,22,1,50000.000,0,'2015-12-23 15:55:42','2015-12-23 15:55:42'),(37,22,1,50000.000,0,'2015-12-23 16:10:41','2015-12-23 16:10:41');
/*!40000 ALTER TABLE `prop_stock_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_stocks`
--

DROP TABLE IF EXISTS `prop_stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stock_cnt` int(11) NOT NULL,
  `onsale_cnt` int(11) NOT NULL DEFAULT '0',
  `unit_price` decimal(15,3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prop_id` (`prop_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_stocks`
--

LOCK TABLES `prop_stocks` WRITE;
/*!40000 ALTER TABLE `prop_stocks` DISABLE KEYS */;
INSERT INTO `prop_stocks` VALUES (1,1,1,25,25,200000.000,'2015-09-29 00:45:00','2015-09-29 00:45:00'),(2,1,2,55,0,0.000,'2015-09-29 00:45:03','2015-09-30 12:08:26'),(3,1,3,20,0,0.000,'2015-09-30 22:36:06','2015-09-30 22:36:06'),(4,2,1,149,149,120000.000,'2015-10-02 01:10:00','2015-10-02 01:10:00'),(5,2,2,94,44,50000.000,'2015-10-02 01:10:52','2015-12-19 15:35:21'),(6,3,1,200,200,106450.000,'2015-11-05 00:00:00','2015-11-05 00:00:00'),(9,3,3,138,0,70000.000,'2015-11-06 03:45:53','2015-12-19 15:09:07'),(11,3,2,62,0,106100.000,'2015-11-07 12:00:55','2015-11-07 12:00:55'),(12,4,1,500,500,91360.000,'2015-11-12 01:00:55','2015-11-12 01:00:55'),(13,5,1,659,659,99714.286,'2015-11-12 01:00:55','2015-11-12 01:00:55'),(14,6,1,390,390,107015.000,'2015-11-12 01:00:55','2015-11-12 01:00:55'),(15,6,2,10,0,0.000,'2015-11-12 01:37:19','2015-11-12 01:37:19'),(18,5,3,41,0,0.000,'2015-12-10 16:30:05','2015-12-10 16:30:05'),(22,2,3,7,0,0.000,'2015-12-19 14:57:26','2015-12-19 14:57:26'),(23,7,1,600,600,97983.334,'2015-11-12 01:57:13','2015-11-12 01:57:13');
/*!40000 ALTER TABLE `prop_stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_earnings_his`
--

DROP TABLE IF EXISTS `user_earnings_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_earnings_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `prop_id` int(11) NOT NULL,
  `amount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `fee` decimal(15,3) NOT NULL DEFAULT '0.000',
  `tax` decimal(15,3) DEFAULT '0.000',
  `stock_cnt` int(11) NOT NULL DEFAULT '0',
  `buy_price` decimal(15,3) NOT NULL DEFAULT '0.000',
  `profit_his_id` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_earnings_his`
--

LOCK TABLES `user_earnings_his` WRITE;
/*!40000 ALTER TABLE `user_earnings_his` DISABLE KEYS */;
INSERT INTO `user_earnings_his` VALUES (36,2,2,14300.160,291.840,0.000,80,9600000.000,13,'2015-12-08 19:51:46','2015-12-08 19:51:46'),(37,2,2,16934.400,345.600,0.000,80,9600000.000,14,'2015-12-08 19:51:46','2015-12-08 19:51:46'),(38,2,2,19443.200,396.800,0.000,80,9600000.000,15,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(39,2,3,14700.000,300.000,0.000,120,12743000.000,1,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(40,3,3,8575.000,175.000,0.000,70,7444500.000,1,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(41,2,3,17640.000,360.000,0.000,120,12743000.000,2,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(42,3,3,10290.000,210.000,0.000,70,7444500.000,2,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(43,2,3,14700.000,300.000,0.000,120,12743000.000,3,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(44,3,3,8575.000,175.000,0.000,70,7444500.000,3,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(45,2,3,20580.000,420.000,0.000,120,12743000.000,4,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(46,3,3,12005.000,245.000,0.000,70,7444500.000,4,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(47,2,3,16464.000,336.000,0.000,120,12743000.000,5,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(48,3,3,9604.000,196.000,0.000,70,7444500.000,5,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(49,2,3,21168.000,432.000,0.000,120,12743000.000,6,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(50,3,3,12348.000,252.000,0.000,70,7444500.000,6,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(51,2,2,25088.000,512.000,0.000,80,9600000.000,7,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(52,2,3,17052.000,348.000,0.000,120,12743000.000,8,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(53,3,3,9947.000,203.000,0.000,70,7444500.000,8,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(54,2,3,18522.000,378.000,0.000,120,12743000.000,9,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(55,3,3,10804.500,220.500,0.000,70,7444500.000,9,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(56,2,3,20874.000,426.000,0.000,120,12743000.000,10,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(57,3,3,12176.500,248.500,0.000,70,7444500.000,10,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(58,2,3,19992.000,408.000,0.000,120,12743000.000,11,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(59,3,3,11662.000,238.000,0.000,70,7444500.000,11,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(60,2,2,28224.000,576.000,0.000,80,9600000.000,12,'2015-12-08 19:53:05','2015-12-08 19:53:05'),(65,2,3,11466.000,234.000,0.000,60,6380000.000,21,'2015-12-16 20:11:07','2015-12-16 20:11:07'),(66,3,3,26754.000,546.000,0.000,140,14875000.000,21,'2015-12-16 20:11:07','2015-12-16 20:11:07'),(67,2,2,25715.200,524.800,0.000,80,9600000.000,22,'2015-12-16 20:19:03','2015-12-16 20:19:03'),(68,3,5,5406.074,2.704,0.000,41,4088285.726,23,'2015-12-21 21:36:24','2015-12-21 21:36:24'),(69,2,1,66241.863,33.138,0.000,55,11000000.000,24,'2015-12-21 22:18:25','2015-12-21 22:18:25'),(70,3,1,24087.950,12.050,0.000,20,4000000.000,24,'2015-12-21 22:18:25','2015-12-21 22:18:25'),(71,2,3,18668.161,9.339,0.000,62,6556200.000,26,'2015-12-21 22:43:34','2015-12-21 22:43:34'),(72,3,3,41551.714,20.786,0.000,138,14662100.000,26,'2015-12-21 22:43:34','2015-12-21 22:43:34');
/*!40000 ALTER TABLE `user_earnings_his` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `auth_type` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (1,'offical','','',NULL,'',10,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'ryukoui','axxV0O23c9u89uKdaGHA5Asu0UOSZiZ7','$2y$13$8.ShxfOrQg7mnY6hPKUWB.a3LdV/ivF0ziEb/DdFCK/OuQceR.BES',NULL,'ryukoui@gmail.com',10,10,'2015-09-27 02:27:21','2015-09-27 02:27:21'),(3,'ryu_koui','q8Jo6bZiDLD99-HSMQ4FM8brQroFvJYy','$2y$13$H6sVnadiKOaD9/3WItS1beV8T/BLqIFK7qmwdg1lQnzZCUdz7QRx6',NULL,'ryu_koui@yahoo.co.jp',10,10,'2015-09-28 22:30:20','2015-09-28 22:30:20'),(4,'customer1','MBLl6zbuU1uZjS2_0FCezmoVh8iGvOFq','$2y$13$SM.VA063yGE5SCxxC1wnh.CVqeQlwEIsEslB/Up9/BYK2deMzltLS',NULL,'customer1@vadek.net',10,10,'2015-12-21 12:18:10','2015-12-21 12:18:10'),(5,'customer2','TlEWZR9z1dj0Ya8o_VusVxCroUi7IF2K','$2y$13$o2W5GM9/McQCDaXNSmqe6.mG6bch6b9K.gXHAyKbHryEAzFwjxwq6',NULL,'customer2@vadek.net',10,10,'2015-12-21 23:12:26','2015-12-21 23:12:26');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_point`
--

DROP TABLE IF EXISTS `user_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_point` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `points` decimal(15,3) NOT NULL DEFAULT '0.000',
  `total_earnings` decimal(15,3) DEFAULT '0.000',
  `total_expenses` decimal(15,3) DEFAULT '0.000',
  `payout_account` varchar(32) DEFAULT NULL,
  `payment_account` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_point`
--

LOCK TABLES `user_point` WRITE;
/*!40000 ALTER TABLE `user_point` DISABLE KEYS */;
INSERT INTO `user_point` VALUES (1,0.000,0.000,0.000,NULL,NULL,'2015-09-25 22:30:20','2015-09-25 22:30:20'),(2,420591.024,706272.984,22414971.200,'','creditcard','2015-09-27 02:27:21','2015-12-23 16:10:41'),(3,197199.738,323186.738,7804355.000,NULL,'creditcard','2015-09-28 22:30:20','2015-12-24 04:34:12'),(4,49749.005,49749.005,51408.000,NULL,NULL,'2015-12-21 12:18:10','2015-12-21 12:36:29'),(5,0.000,0.000,0.000,NULL,NULL,'2015-12-21 23:12:26','2015-12-21 23:12:26');
/*!40000 ALTER TABLE `user_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_point_his`
--

DROP TABLE IF EXISTS `user_point_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_point_his` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` smallint(6) NOT NULL,
  `change` decimal(15,3) NOT NULL,
  `points` decimal(15,3) NOT NULL,
  `total_earnings` decimal(15,3) NOT NULL,
  `total_expenses` decimal(15,3) NOT NULL,
  `account_type` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_point_his`
--

LOCK TABLES `user_point_his` WRITE;
/*!40000 ALTER TABLE `user_point_his` DISABLE KEYS */;
INSERT INTO `user_point_his` VALUES (1,2,102,25715.000,37181.000,322862.960,22343000.000,NULL,'2015-12-16 20:19:03','2015-12-16 20:19:03'),(6,2,102,49750.000,76504.000,182491.000,7444500.000,NULL,'2015-12-19 14:57:26','2015-12-19 14:57:26'),(7,3,101,51408.000,127912.000,182491.000,7444500.000,'paypal','2015-12-19 14:57:26','2015-12-19 14:57:26'),(8,3,2,-51408.000,25096.000,182491.000,7495908.000,'paypal','2015-12-19 14:57:26','2015-12-19 14:57:26'),(12,3,102,69650.000,146154.000,252141.000,7495908.000,NULL,'2015-12-19 15:24:59','2015-12-19 15:24:59'),(13,2,101,71971.000,109152.200,322862.960,22343000.000,'paypal','2015-12-19 15:24:59','2015-12-19 15:24:59'),(14,2,2,-71971.000,-34790.200,322862.960,22414971.200,'paypal','2015-12-19 15:24:59','2015-12-19 15:24:59'),(15,2,102,49750.000,86931.000,372612.960,22414971.200,NULL,'2015-12-19 15:36:49','2015-12-19 15:36:49'),(16,3,101,51408.000,197562.000,252141.000,7495908.000,'paypal','2015-12-19 15:36:49','2015-12-19 15:36:49'),(17,3,2,-51408.000,94746.000,252141.000,7547316.000,'paypal','2015-12-19 15:36:49','2015-12-19 15:36:49'),(18,2,102,49750.000,136681.000,422362.960,22414971.200,NULL,'2015-12-19 16:20:51','2015-12-19 16:20:51'),(19,3,101,51408.000,197562.000,252141.000,7547316.000,'paypal','2015-12-19 16:20:51','2015-12-19 16:20:51'),(20,3,2,-51408.000,94746.000,252141.000,7598724.000,'paypal','2015-12-19 16:20:51','2015-12-19 16:20:51'),(21,2,102,49750.000,186431.000,472112.960,22414971.200,NULL,'2015-12-21 12:19:51','2015-12-21 12:19:51'),(22,4,101,51408.000,51408.000,0.000,0.000,'paypal','2015-12-21 12:19:51','2015-12-21 12:19:51'),(23,4,2,-51408.000,-51408.000,0.000,51408.000,'paypal','2015-12-21 12:19:51','2015-12-21 12:19:51'),(24,4,102,49749.005,49749.005,49749.005,51408.000,NULL,'2015-12-21 12:36:29','2015-12-21 12:36:29'),(25,3,101,51407.000,197561.000,252141.000,7598724.000,'paypal','2015-12-21 12:36:29','2015-12-21 12:36:29'),(26,3,2,-51407.000,94747.000,252141.000,7650131.000,'paypal','2015-12-21 12:36:29','2015-12-21 12:36:29'),(27,3,103,5406.074,151560.074,257547.074,7650131.000,NULL,'2015-12-21 21:36:24','2015-12-21 21:36:24'),(28,2,103,66241.863,252672.863,538354.823,22414971.200,NULL,'2015-12-21 22:18:25','2015-12-21 22:18:25'),(29,3,103,24087.950,175648.024,281635.024,7650131.000,NULL,'2015-12-21 22:18:25','2015-12-21 22:18:25'),(30,2,103,18668.161,271341.024,557022.984,22414971.200,NULL,'2015-12-21 22:43:34','2015-12-21 22:43:34'),(31,3,103,41551.714,217199.738,323186.738,7650131.000,NULL,'2015-12-21 22:43:34','2015-12-21 22:43:34'),(35,2,102,49750.000,321091.024,606772.984,22414971.200,NULL,'2015-12-23 15:33:11','2015-12-23 15:33:11'),(36,3,101,51408.000,268607.738,323186.738,7650131.000,'paypal','2015-12-23 15:33:11','2015-12-23 15:33:11'),(37,3,2,-51408.000,165791.738,323186.738,7701539.000,'paypal','2015-12-23 15:33:11','2015-12-23 15:33:11'),(38,2,102,49750.000,370841.024,656522.984,22414971.200,NULL,'2015-12-23 15:55:42','2015-12-23 15:55:42'),(39,3,101,51408.000,268607.738,323186.738,7701539.000,'paypal','2015-12-23 15:55:42','2015-12-23 15:55:42'),(40,3,2,-51408.000,165791.738,323186.738,7752947.000,'paypal','2015-12-23 15:55:42','2015-12-23 15:55:42'),(41,2,102,49750.000,420591.024,706272.984,22414971.200,NULL,'2015-12-23 16:10:41','2015-12-23 16:10:41'),(42,3,101,51408.000,268607.738,323186.738,7752947.000,'paypal','2015-12-23 16:10:41','2015-12-23 16:10:41'),(43,3,2,-51408.000,165791.738,323186.738,7804355.000,'paypal','2015-12-23 16:10:41','2015-12-23 16:10:41'),(44,3,1,-10000.000,207199.738,323186.738,7804355.000,NULL,'2015-12-24 04:30:13','2015-12-24 04:30:13'),(45,3,1,-10000.000,197199.738,323186.738,7804355.000,NULL,'2015-12-24 04:34:12','2015-12-24 04:34:12');
/*!40000 ALTER TABLE `user_point_his` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-25  1:58:31
