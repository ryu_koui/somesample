<?php
namespace rest\versions\v1\controllers;

use yii\rest\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        return 'test index';
    }
    
    public function actionInfo()
    {
        return array('test');
    }
}
