<?php
namespace rest\versions\v1\controllers\user;

use yii\rest\ActiveController;

class InfoController extends ActiveController
{
    public $modelClass = 'common\models\user\Info';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        return $actions;
    }
    
}
