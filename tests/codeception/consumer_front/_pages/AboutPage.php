<?php

namespace tests\codeception\consumer_front\_pages;

use yii\codeception\BasePage;

/**
 * Represents about page
 * @property \codeception_consumer_front\AcceptanceTester|\codeception_consumer_front\FunctionalTester $actor
 */
class AboutPage extends BasePage
{
    public $route = 'site/about';
}
