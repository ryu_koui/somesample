<?php

namespace tests\codeception\consumer_front\unit;

/**
 * @inheritdoc
 */
class DbTestCase extends \yii\codeception\DbTestCase
{
    public $appConfig = '@tests/codeception/config/consumer_front/unit.php';
}
