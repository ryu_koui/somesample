<?php

namespace tests\codeception\consumer_front\unit;

/**
 * @inheritdoc
 */
class TestCase extends \yii\codeception\TestCase
{
    public $appConfig = '@tests/codeception/config/consumer_front/unit.php';
}
