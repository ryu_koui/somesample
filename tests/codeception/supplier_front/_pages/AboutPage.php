<?php

namespace tests\codeception\supplier_front\_pages;

use yii\codeception\BasePage;

/**
 * Represents about page
 * @property \codeception_supplier_front\AcceptanceTester|\codeception_supplier_front\FunctionalTester $actor
 */
class AboutPage extends BasePage
{
    public $route = 'site/about';
}
