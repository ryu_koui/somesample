<?php
use tests\codeception\supplier_front\FunctionalTester;
use tests\codeception\supplier_front\_pages\AboutPage;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that about works');
AboutPage::openBy($I);
$I->see('About', 'h1');
