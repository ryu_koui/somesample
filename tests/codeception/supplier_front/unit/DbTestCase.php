<?php

namespace tests\codeception\supplier_front\unit;

/**
 * @inheritdoc
 */
class DbTestCase extends \yii\codeception\DbTestCase
{
    public $appConfig = '@tests/codeception/config/supplier_front/unit.php';
}
