<?php

namespace tests\codeception\supplier_front\unit;

/**
 * @inheritdoc
 */
class TestCase extends \yii\codeception\TestCase
{
    public $appConfig = '@tests/codeception/config/supplier_front/unit.php';
}
